<?php

function htmlDisplayAlamat($obj) {
    $string = "";

    if (!empty($obj->alamat_jalan)) {
        $string .= $obj->alamat_jalan . "<br>\n";
    }

    if (!empty($obj->alamat_rt)) {
        $string .= "RT: " . $obj->alamat_rt . "\n";
    }
    if (!empty($obj->alamat_rw)) {
        $string .= "RW: " . $obj->alamat_rw . "\n";
    }
    if (!empty($obj->alamat_kode_pos)) {
        $string .= "Kode Pos: " . $obj->alamat_kode_pos . "\n";
    }

    if (!empty($obj->alamat_rt) || !empty($obj->alamat_rw) || !empty($obj->alamat_kode_pos)) {
        $string .= "<br>\n";
    }

    if (!empty($obj->alamat_provinsi)) {
        $string .= "Provinsi: " . $obj->alamat_provinsi . "\n";
    }
    if (!empty($obj->alamat_kota)) {
        $string .= "Kabupaten/Kota: " . $obj->alamat_kota . "\n";
    }

    if (!empty($obj->alamat_provinsi) || !empty($obj->alamat_kota)) {
        $string .= "<br>\n";
    }


    if (!empty($obj->alamat_kecamatan)) {
        $string .= "Kecamatan: " . $obj->alamat_kecamatan . "\n";
    }
    if (!empty($obj->alamat_kelurahan)) {
        $string .= "Kelurahan: " . $obj->alamat_kelurahan . "\n";
    }

    if (trim($string) == "") {
        $string = "-";
    }

    return $string;
}
