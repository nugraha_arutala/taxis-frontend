<?php

namespace App;

use App\Contracts\ApiModelContract;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException as HttpClientException;
use GuzzleHttp\Exception\ConnectException as HttpConnectException;
use GuzzleHttp\Exception\ServerException as HttpServerException;
use Illuminate\Support\Facades\Log;

class WajibPajak extends ApiModel implements ApiModelContract
{
    /**
     * Returns the endpoint URL for this object model. It's a function so that it doesn't show up on the object's property list.
     * @return string The endpoint URL.
     */
    protected static function endpointUrl()
    {
        return env('API_URL', 'http://localhost:3000') . "/pajak/subjek";
    }

    /**
     * Returns required fields. It's a function so that it doesn't show up on the object's property list.
     * @return array Array of string listing required field names.
     */
    protected function required()
    {
        return ['nama', 'jenis'];
    }

    protected function combineAlamat()
    {
        if(empty($this->alamat)) {
            $this->alamat = (object) [];
        } else if(gettype($this->alamat) === "array") {
            $this->alamat = (object) $this->alamat;
        }

        if(isset($this->jalan)) {
            $this->alamat->jalan = $this->jalan;
            unset($this->jalan);
        }
        if(isset($this->rt)) {
            $this->alamat->rt = $this->rt;
            unset($this->rt);
        }
        if(isset($this->rw)) {
            $this->alamat->rw = $this->rw;
            unset($this->rw);
        }
        if(isset($this->kelurahan)) {
            $this->alamat->kelurahan = $this->kelurahan;
            unset($this->kelurahan);
        }
        if(isset($this->kecamatan)) {
            $this->alamat->kecamatan = $this->kecamatan;
            unset($this->kecamatan);
        }
        if(isset($this->kota)) {
            $this->alamat->kota = $this->kota;
            unset($this->kota);
        }
        if(isset($this->provinsi)) {
            $this->alamat->provinsi = $this->provinsi;
            unset($this->provinsi);
        }
        if(isset($this->kode_pos)) {
            $this->alamat->kode_pos = $this->kode_pos;
            unset($this->kode_pos);
        }

        return true;
    }

    public static function find($user, $id)
    {
        try {
            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . "/{$id}");

            $response = json_decode($response->getBody());

            // return null if deleted
            if(!empty($response->deleted_at)) {
                return null;
            }
            $obj = new self();
            foreach($response as $key => $value) {
                $obj->{$key} = $value;
            }
            return $obj;
        } catch(HttpClientException $e) {
            if($e->getCode() === 404) {
                return null;
            }
            throw $e;
        }
    }

    public static function findByNpwpd($user, $npwpd)
    {
        try {
            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . '?' . http_build_query(['npwpd' => $npwpd]));

            $response = json_decode($response->getBody());
            $data = $response;
            if(count($data->items) > 0) {
                return $data->items[0];
            } else {
                return null;
            }
        } catch(HttpClientException $e) {
            if($e->getCode() === 404) {
                return null;
            }
            throw $e;
        }
    }

    /**
     * Fetch items
     * @param  object  $user      The active user
     * @param  integer $pageSize  Items per page
     * @param  integer $start     Item start from index number
     * @param  array   $ordering  Ordering specifications
     * @param  string  $search    String in the form of "column|query". If column doesn't exist, then it won't search
     * @return object             Object with "count" (total number of items regardless of page) and "items" properties
     */
    public static function index($user, $pageSize = 10, $start = 0, $ordering = null, $search = null, $filter = null)
    {
        // calculate page index
        $pageIndex = floor($start / $pageSize);

        // create ordering query
        $order = 'created_at DESC'; // default
        if($ordering) {
            // TODO
        }

        // search
        $search = explode('|', $search, 2);
        if(count($search) !== 2) {
            // cancel
            $search = null;
        }

        try {
            $query = [
                'page_size' => $pageSize,
                'page_num' => $pageIndex + 1, // pageIndex starts from 1 and not 0
                'orders' => $order
            ];
            if($search) {
                $query[$search[0]] = $search[1];
            }
            if($filter) {
                if(!empty($filter['sdate'])) {
                    $query['sdate'] = $filter['sdate'];
                }
                if(!empty($filter['edate'])) {
                    $query['edate'] = $filter['edate'];
                }
            }

            $query = http_build_query($query);

            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . '?' . $query);

            $response = json_decode($response->getBody());
            return $response;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }
    }

    public function save($user)
    {
        // Check required values
        $this->checkRequired();
        // Unset null values
        $this->cleanupNulls();
        // Cut stamps
        $this->cutStamps();
        // Put alamat in one object
        $this->combineAlamat();

        try {
            $fields = get_object_vars($this);
            unset($fields['_id']);
            unset($fields['__v']);

            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);

            // Create new
            if(empty($this->_id)) {
                $response = $client->request('POST', $this->endpointUrl(), [
                    'json' => $fields
                ]);
                $responseBody = json_decode($response->getBody());
                $this->_id = $responseBody->data->_id;
                Log::info("New wajib pajak");
                Log::debug(print_r($responseBody, true));
            }
            // Update existing
            else if(!empty($this->_id)) {
                $response = $client->request('PUT', $this->endpointUrl() . '/' . $this->_id, [
                    'json' => $fields
                ]);
                $responseBody = json_decode($response->getBody());
                Log::info("Updated wajib pajak.");
                Log::debug(print_r($responseBody, true));
            }

            return true;
        } catch(HttpServerException $e) {
            $responseBody = json_decode($e->getResponse()->getBody());
            if(isset($responseBody->meta->message)) {
                throw new \Exception($responseBody->meta->message);
            }
            throw $e;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }
    }
}
