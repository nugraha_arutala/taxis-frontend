<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\WajibPajak;
use App\ObjekPajak;
use App\KlasifikasiPajak;
use App\SPT;

class ObjekPajakController extends Controller
{
    /**
     * View data table
     */
    public function viewTable()
    {
        $jenisPajak = KlasifikasiPajak::parents(Auth::user());
        $jenisPajak = KlasifikasiPajak::structurize($jenisPajak->items);

        return view('objek-pajak/datatable', [
            'jenisPajak' => $jenisPajak
        ]);
    }

    public function viewDetail($id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $id);
        if(!$objekPajak) { abort(404); }
        return view('objek-pajak/detail', ['objekPajak' => $objekPajak]);
    }

    /**
     * Generate datatable JSON
     */
    public function datatableJson(Request $request, $subjekPajakId = null)
    {
        if(Auth::user()->checkRole('wajibPajak')) {
            // Wajib Pajak: restrict to own OP
            $user = User::find(Auth::user(), Auth::user()->_id);
            $subjekPajakId = $user->subjek_pajak ?? '-';
        }

        $filter = $request->input('filter', null);
        if($subjekPajakId) {
            if($filter === null) { $filter = []; }
            $filter['subjek'] = $subjekPajakId;
        }

        // These are technically filters
        $columns = $request->input('columns');
        if(!empty($columns[5]['search']['value'])) {
            $filter['status'] = $columns[5]['search']['value'];
        }
        if(!empty($columns[4]['search']['value'])) {
            $filter['jenis_pajak'] = $columns[4]['search']['value'];
        }

        $params = [
            Auth::user(),
            $request->input('length', 10),
            $request->input('start', 0),
            $request->input('order', null),
            $request->input('search', null),
            $filter
        ];
        $data = ObjekPajak::index(...$params);
        return json_encode($this->makeDatatableJson($data, $request->input('draw', 0)));
    }

    /**
     * New data form
     */
    public function formNew($idWajibPajak = null)
    {
        $klasifikasiPajak = KlasifikasiPajak::all(Auth::user());
        $klasifikasiPajak = KlasifikasiPajak::structurize($klasifikasiPajak->items);

        $vars = [
            'klasifikasiPajak' => $klasifikasiPajak
        ];

        if(Auth::user()->checkRole('wajibPajak')) {
            // Wajib Pajak: restrict to self
            $user = User::find(Auth::user(), Auth::user()->_id);
            $vars['fixedWajibPajak'] = true;
            $vars['wajibPajak'] = $user->subjek_pajak;
        } else {
            if($idWajibPajak) {
                $wajibPajak = WajibPajak::find(Auth::user(), $idWajibPajak);
                if($wajibPajak) {
                    $vars['wajibPajak'] = $wajibPajak;
                }
            }
        }

        $vars['sidebarUrl'] = url('pendaftaran/objek-pajak');
        return view('objek-pajak/form', $vars);
    }

    /**
     * Edit existing data form
     */
    public function formEdit($id)
    {
        $klasifikasiPajak = KlasifikasiPajak::all(Auth::user());
        $klasifikasiPajak = KlasifikasiPajak::structurize($klasifikasiPajak->items);

        $vars = [
            'klasifikasiPajak' => $klasifikasiPajak
        ];

        $objekPajak = ObjekPajak::find(Auth::user(), $id);
        if(!$objekPajak) { abort(404); }
        $vars['objekPajak'] = $objekPajak;
        $vars['wajibPajak'] = $objekPajak->subjek_pajak;

        if(Auth::user()->checkRole('wajibPajak')) {
            // Wajib Pajak: restrict to self
            $user = User::find(Auth::user(), Auth::user()->_id);
            if(empty($user->subjek_pajak) || empty($objekPajak->subjek_pajak)) { abort(404); }
            if($user->subjek_pajak->_id != $objekPajak->subjek_pajak->_id) {
                abort(404);
            }
            $vars['fixedWajibPajak'] = true;
        }

        return view('objek-pajak/form', $vars);
    }

    /**
     * Process data
     */
    public function post(Request $request)
    {
        $request->validate([
            'npwpd' => 'required',
            'jenis_pajak' => 'required',
            'klasifikasi_pajak' => 'required',
            'nama' => 'required',
            'kota' => 'required'
        ]);

        // confirm NPWPD is correct
        $wajibPajak = WajibPajak::findByNpwpd(Auth::user(), $request->input('npwpd'));
        if(!$wajibPajak) {
            return redirect()->back()->withInput()->withErrors([
                'message' => "Wajib Pajak dengan NPWPD tersebut tidak ditemukan. Mohon periksa lagi."
            ]);
        }
        // confirm WP is the same as User's
        if(Auth::user()->checkRole('wajibPajak')) {
            $user = User::find(Auth::user(), Auth::user()->_id);
            if(empty($user->subjek_pajak) || $user->subjek_pajak->_id !== $wajibPajak->_id) {
                return redirect()->back()->withInput()->withErrors([
                    'message' => "Isian Wajib Pajak tidak benar. Mohon periksa lagi."
                ]);
            }
        }
        $request->merge(['subjek_pajak' => $wajibPajak->_id]);
        $request->offsetUnset('npwpd');

        if(!$request->input('_id')) {
            // Create new model object
            $objekPajak = new ObjekPajak();
            $createSpt = true;
        } else {
            // Find object
            $objekPajak = ObjekPajak::find(Auth::user(), $request->input('_id'));
            if(!$objekPajak) { abort(404); }
            $createSpt = false;
        }

        // Assign input values
        $possibleInputs = [
            'jenis_pajak', 'klasifikasi_pajak', 'nama',
            'jalan', 'rt', 'rw', 'kode_pos', 'provinsi', 'kota', 'kecamatan', 'kelurahan', 'nomor_telepon', 'nomor_hp',
            'luas_tanah', 'luas_bangunan', 'luas_tempat_usaha', 'jam_operasional', 'subjek_pajak', 'status_kepemilikan',
            'status_usaha', 'jenis_masakan_utama'
        ];
        foreach($request->input() as $input => $value) {
            if(!in_array($input, $possibleInputs)) { continue; }
            $objekPajak->{$input} = $value;
        }

        try {
            // Save
            $objekPajak->save(Auth::user());
            if($createSpt) {
                // Fetch necessary data
                if(gettype($objekPajak->jenis_pajak) === 'string') {
                    $objekPajak->jenis_pajak = KlasifikasiPajak::find(Auth::user(), $objekPajak->jenis_pajak);
                }
                foreach($objekPajak->klasifikasi_pajak as &$i) {
                    if(gettype($i) === 'string') {
                        $i = KlasifikasiPajak::find(Auth::user(), $i);
                    }
                }
                if(gettype($objekPajak->subjek_pajak) === 'string') {
                    $objekPajak->subjek_pajak = WajibPajak::find(Auth::user(), $objekPajak->subjek_pajak);
                }
                $spt = SPT::create(Auth::user(), $objekPajak, date("m"), date("Y"));
            }
        } catch(\Exception $e) {
            $request->merge(['npwpd' => $wajibPajak->npwpd ?? '']);
            return redirect()->back()->withInput()->withErrors([
                'message' => $e->getMessage()
            ]);
        }

        $request->session()->flash('message', 'save-successful');
        return redirect('/lihat-data/objek-pajak');
    }

    /**
     * (AJAX) List OP
     */
    public function listing(Request $request)
    {
        $filter = [];
        if($request->input('subjek')) {
            $filter['subjek'] = $request->input('subjek');
        }

        // disallow non-filtering
        if(empty($filter)) {
            abort('404');
        }

        $params = [
            Auth::user(),
            999,
            0,
            null,
            null,
            $filter
        ];
        $data = ObjekPajak::index(...$params);

        $list = []; // stripped down list
        foreach($data->items as $i) {
            $list[] = (object) [
                '_id' => $i->_id,
                'nopd' => $i->nop,
                'nama' => ($i->detail_objek ? ($i->detail_objek->nama ?? null) : null)
            ];
        }

        return response()->json($list);
    }

    /**
     * Verifikasi OP
     */
    public function verify(Request $request)
    {
        // Find object
        $objekPajak = ObjekPajak::find(Auth::user(), $request->input('_id'));
        if(!$objekPajak) { abort(404); }

        try {
            $verify = $objekPajak->verify(Auth::user(), true);
        } catch(\Exception $e) {
            abort(500);
        }

        return redirect('/lihat-data/objek-pajak/'.$objekPajak->_id);
    }
}
