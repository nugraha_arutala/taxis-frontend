<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Make JSON data for server-side Datatable.
     * @param  object $data  Object with "count" (total number of items regardless of page) and "items" properties
     * @param  integer $draw Draw count. Defaults to 0.
     * @return object        JSON object
     */
    protected function makeDatatableJson($data, $draw = 0)
    {
        $data = (object) $data; // if array, turn into object
        return (object) [
            'draw' => $draw,
            'recordsTotal' => $data->count,
            'recordsFiltered' => $data->count,
            'data' => $data->items
        ];
    }
}
