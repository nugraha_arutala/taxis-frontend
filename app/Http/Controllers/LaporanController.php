<?php

namespace App\Http\Controllers;

use App\Exports\UsersExport;
use App\Exports\WajibPajakExport;
use App\Exports\ObjekPajakExport;
use App\Exports\SPTExport;
use App\KlasifikasiPajak;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class LaporanController extends Controller
{
    /**
     * View form
     */
    public function formView()
    {
        $klasifikasiPajak = KlasifikasiPajak::all(Auth::user());
        $klasifikasiPajak = KlasifikasiPajak::structurize($klasifikasiPajak->items);

        return view('laporan/view', [
            'klasifikasiPajak' => $klasifikasiPajak
        ]);
    }

    /**
     * Export to Excel
     */
    public function export(Request $request)
    {
        switch($request->input('time_frame')) {
            case "daily":
                $filter['sdate'] = date("Y-m-d");
                $filter['edate'] = date("Y-m-d");
                break;
            case "monthly":
                $filter['sdate'] = $request->input('tahun') . "-" . $request->input('bulan') . "-01";
                $filter['edate'] = date("Y-m-t", strtotime($filter['sdate']));
                break;
            case "costum":
                $filter['sdate'] = $request->input('start_date');
                $filter['edate'] = $request->input('end_date');
                break;
            default:
                $filter['sdate'] = "";
                $filter['edate'] = "";
                break;
        }

        if($request->input('stp_status')) 
            $filter['status'] = $request->input('stp_status');

        $params = [
            Auth::user(),
            $request->input('length', -1),
            $request->input('start', 1),
            $request->input('order', null),
            $request->input('search', null),
            $filter,
        ];
        
        if ("user" === $request->input('module')) {
            $filename = "Report " . ucfirst($request->input('time_frame')) . "-User " . implode(" until ", [$filter['sdate'], $filter['edate']]);
            return Excel::download(new UsersExport($params), $filename . ".xlsx");
        } 
        else if ("wajib_pajak" === $request->input('module')) {
            $filename = "Report " . ucfirst($request->input('time_frame')) . "-Wajib Pajak " . implode(" until ", [$filter['sdate'], $filter['edate']]);
            return Excel::download(new WajibPajakExport($params), $filename . ".xlsx");
        }
        else if ("objek_pajak" === $request->input('module')) {
            $filename = "Report " . ucfirst($request->input('time_frame')) . "-Objek Pajak " . implode(" until ", [$filter['sdate'], $filter['edate']]);
            return Excel::download(new ObjekPajakExport($params), $filename . ".xlsx");
        }
        else if ("pembayaran" === $request->input('module')) {
            $filename = "Report " . ucfirst($request->input('time_frame')) . "-SPT_Pembayaran Pajak " . implode(" until ", [$filter['sdate'], $filter['edate']]);
            return Excel::download(new SPTExport($params), $filename . ".xlsx");
        }
        
    }
}