<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\WajibPajak;
use App\ObjekPajak;
use App\KlasifikasiPajak;
use App\SPT;

class PelaporanController extends Controller
{
    /**
     * View data table
     */
    public function viewTable()
    {
        $jenisPajak = KlasifikasiPajak::parents(Auth::user());
        $jenisPajak = KlasifikasiPajak::structurize($jenisPajak->items);

        // get subjek pajak details
        if(gettype(Auth::user()->subjek_pajak) === "string") {
            Auth::user()->subjek_pajak = WajibPajak::find(Auth::user(), Auth::user()->subjek_pajak);
        }

        return view('pelaporan/datatable', [
            'jenisPajak' => $jenisPajak
        ]);
    }

    /**
     * Generate datatable JSON
     */
    public function datatableJson(Request $request)
    {
        $filter = $request->input('filter', null);
        // If not super admin, restrict objek pajak to self
        if(Auth::user()->checkRole('wajibPajak')) {
            if($filter === null) { $filter = []; }
            $filter['subjek'] = Auth::user()->subjek_pajak;
        }

        // These are technically filters
        $columns = $request->input('columns');
        if(!empty($columns[5]['search']['value'])) {
            $filter['status'] = $columns[5]['search']['value'];
        }
        if(!empty($columns[4]['search']['value'])) {
            $filter['jenis_pajak'] = $columns[4]['search']['value'];
        }

        $params = [
            Auth::user(),
            $request->input('length', 10),
            $request->input('start', 0),
            $request->input('order', null),
            $request->input('search', null),
            $filter
        ];
        $data = ObjekPajak::index(...$params);
        return json_encode($this->makeDatatableJson($data, $request->input('draw', 0)));
    }

    /**
     * View pelaporan per objek
     */
    public function viewObjek($id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $id);
        if(!$objekPajak) { abort(404); }

        // Check ownership if not admin/fiskus
        if(Auth::user()->checkRole('wajibPajak')) {
            if($objekPajak->subjek_pajak->_id !== Auth::user()->subjek_pajak->_id) {
                abort(404);
            }
        }

        return view('pelaporan/objek', [
            'objekPajak' => $objekPajak,
            'sidebarUrl' => url('pelaporan')
        ]);
    }

    /**
     * Generate datatable JSON
     */
    public function objekDatatableJson(Request $request, $objekPajakId = null)
    {
        $filter = $request->input('filter', null);
        if($objekPajakId) {
            if($filter === null) { $filter = []; }
            $filter['objek'] = $objekPajakId;
        }
        $filter['is_skpd'] = "false";

        $params = [
            Auth::user(),
            $request->input('length', 10),
            $request->input('start', 0),
            $request->input('order', null),
            $request->input('search', null),
            $filter
        ];
        $data = SPT::index(...$params);
        return json_encode($this->makeDatatableJson($data, $request->input('draw', 0)));
    }

    /**
     * Form new SPT
     */
    public function createForm($objekPajakId)
    {
        // Get OP
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        // Check ownership if not admin/fiskus
        if(Auth::user()->checkRole('wajibPajak')) {
            if($objekPajak->subjek_pajak->_id !== Auth::user()->subjek_pajak->_id) {
                abort(404);
            }
        }

        return view('pelaporan/create', [
            'objekPajak' => $objekPajak,
            'sidebarUrl' => url('pelaporan')
        ]);
    }

    public function createProcess(Request $request, $objekPajakId)
    {
        $request->validate([
            'bulan' => 'required|in:01,02,03,04,05,06,07,08,09,10,11,12',
            'tahun' => 'required|gte:2018|lte:'.date('Y')
        ]);

        // Get OP
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        $params = [
            Auth::user(),
            $request->input('length', 10),
            $request->input('start', 0),
            $request->input('order', null),
            $request->input('search', null),
            $filter = array(
                'objek' => $objekPajakId,
                'periode' => array(
                    'bulan' => $request->input('bulan'),
                    'tahun' => $request->input('tahun'),
                )
            )
        ];

        $data = SPT::index(...$params);
        if($data->count){
            return redirect()->back()->withInput()->withErrors([
                'message' => "Duplicate SPTPD"
            ]);
        } else {
            // Check ownership if not admin/fiskus
            if(Auth::user()->checkRole('wajibPajak')) {
                if($objekPajak->subjek_pajak->_id !== Auth::user()->subjek_pajak->_id) {
                    abort(404);
                }
            }

            $spt = SPT::create(Auth::user(), $objekPajak, $request->input('bulan'), $request->input('tahun'));

            $request->session()->flash('message', 'create-successful');
            $request->session()->flash('spt', $spt->_id);
            return redirect('/pelaporan/'.$objekPajakId);
        }
    }

    /**
     * Form pelaporan
     */
    public function form($objekPajakId, $id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        // Check ownership if not admin/fiskus
        if(Auth::user()->checkRole('wajibPajak')) {
            if($objekPajak->subjek_pajak->_id !== Auth::user()->subjek_pajak->_id) {
                abort(404);
            }
        }

        $spt = SPT::find(Auth::user(), $id);
        if(!$id) { abort(404); }

        // no editing after payment
        if($spt->status === 1) {
            abort(404);
        }

        // if mismatch
        if($spt->objek_pajak->_id !== $objekPajak->_id) {
            abort(404);
        }

        return view('pelaporan/form', [
            'objekPajak' => $objekPajak,
            'spt' => $spt,
            'sidebarUrl' => url('pelaporan')
        ]);
    }

    public function save(Request $request, $objekPajakId, $id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        // Check ownership if not admin/fiskus
        if(Auth::user()->checkRole('wajibPajak')) {
            if($objekPajak->subjek_pajak->_id !== Auth::user()->subjek_pajak->_id) {
                abort(404);
            }
        }

        $spt = SPT::find(Auth::user(), $id);
        if(!$id) { abort(404); }

        // no editing after payment
        if($spt->status === 1) {
            abort(404);
        }

        // if mismatch
        if($spt->objek_pajak->_id !== $objekPajak->_id) {
            abort(404);
        }

        // Assign input values, calculate DPP
        $klasifikasiMap = $request->input('klasifikasi');
        $dataPendapatan = $request->input('data_pendapatan');
        $spt->data_pendapatan = (object) [];
        $jumlahDpp = 0;
        $jumlahPajak = 0;
        $dendaPajak = 0;
        $potonganPajak = 0;

        if($objekPajak->jenis_pajak->kode == '4.1.1.02') {
            foreach($dataPendapatan as $klasifikasiId => $i) {
                $klasifikasi = KlasifikasiPajak::find(Auth::user(), $klasifikasiMap[$klasifikasiId]);
                if(!$klasifikasi) { continue; }

                // Calc tarif (persenan)
                $tarif = $klasifikasi->tarif ?? '10%';
                $mathTarif = $tarif;
                if(substr($mathTarif, -1) === '%') {
                    $mathTarif = rtrim($mathTarif, '%');
                }
                if(!is_numeric($mathTarif)) {
                    throw new \Exception("Invalid tarif.");
                } else {
                    $mathTarif = $mathTarif / 100;
                }

                // Perbulan
                if(isset($i['total'])) {
                    $spt->data_pendapatan->{$klasifikasi->_id} = (object) [
                        'makanan' => $i['total']['makanan'],
                        'service' => $i['total']['service'],
                        'lainnya' => $i['total']['lainnya']
                    ];
                    // decleave
                    foreach($spt->data_pendapatan->{$klasifikasi->_id} as $key => &$v) {
                        $v = str_replace('.', '', $v);
                        $v = str_replace(',', '.', $v);
                        if(is_numeric($v)) {
                            $v = $v+0;
                        } else {
                            $v = 0;
                        }
                    }
                }
                // Perhari
                else {
                    $spt->data_pendapatan->{$klasifikasi->_id} = 0;
                    // Setup
                    $spt->data_pendapatan->{$klasifikasi->_id} = (object) [
                        'data_per_hari' => []
                    ];

                    $totalMakanan = 0;
                    $totalService = 0;
                    $totalLainnya = 0;
                    foreach($i as $hari => $ii) {
                        $hariIni = (object) [
                            'tanggal' => "{$spt->periode->tahun}-{$spt->periode->bulan}-".str_pad($hari, 2, 0, STR_PAD_LEFT),
                            'makanan' => $ii['makanan'],
                            'service' => $ii['service'],
                            'lainnya' => $ii['lainnya']
                        ];
                        // decleave
                        foreach($hariIni as $key => &$v) {
                            if($key === 'tanggal') { continue; }
                            $v = str_replace('.', '', $v);
                            $v = str_replace(',', '.', $v);
                            if(is_numeric($v)) {
                                $v = $v+0;
                            } else {
                                $v = 0;
                            }
                        }
                        $totalMakanan += $hariIni->makanan;
                        $totalService += $hariIni->service;
                        $totalLainnya += $hariIni->lainnya;

                        $spt->data_pendapatan->{$klasifikasi->_id}->data_per_hari[] = $hariIni;
                    }

                    $spt->data_pendapatan->{$klasifikasi->_id}->makanan = $totalMakanan;
                    $spt->data_pendapatan->{$klasifikasi->_id}->service = $totalService;
                    $spt->data_pendapatan->{$klasifikasi->_id}->lainnya = $totalLainnya;
                }

                // Total
                $spt->data_pendapatan->{$klasifikasi->_id}->dpp =
                    $spt->data_pendapatan->{$klasifikasi->_id}->makanan +
                    $spt->data_pendapatan->{$klasifikasi->_id}->service +
                    $spt->data_pendapatan->{$klasifikasi->_id}->lainnya
                ;
                $spt->data_pendapatan->{$klasifikasi->_id}->tarif = $tarif;
                $spt->data_pendapatan->{$klasifikasi->_id}->pajak =
                    ceil(
                        $spt->data_pendapatan->{$klasifikasi->_id}->dpp
                        *
                        $mathTarif
                    )
                ;

                // Sum Jumlah_DPP + Jumlah_Pajak
                $jumlahDpp += $spt->data_pendapatan->{$klasifikasi->_id}->dpp;
                $jumlahPajak += $spt->data_pendapatan->{$klasifikasi->_id}->pajak;
            }
        } elseif ($objekPajak->jenis_pajak->kode == '4.1.1.11'){
            foreach($dataPendapatan as $klasifikasiId => $i) {
                $klasifikasi = KlasifikasiPajak::find(Auth::user(), $klasifikasiMap[$klasifikasiId]);
                if(!$klasifikasi) { continue; }

                $tarif = $klasifikasi->tarif ?? '5%';
                $mathTarif = $tarif;
                if(substr($mathTarif, -1) === '%') {
                    $mathTarif = rtrim($mathTarif, '%');
                }
                if(!is_numeric($mathTarif)) {
                    throw new \Exception("Invalid tarif.");
                } else {
                    $mathTarif = $mathTarif / 100;
                }

                if(isset($i['total'])) {
                    $spt->data_pendapatan->{$klasifikasi->_id} = (object) [
                        'harga' => $i['total']['harga'],
                        'penjualan' => $i['total']['penjualan'],
                        'total' => $i['total']['jumlah']
                    ];
                    // decleave
                    foreach($spt->data_pendapatan->{$klasifikasi->_id} as $key => &$v) {
                        $v = str_replace('.', '', $v);
                        $v = str_replace(',', '.', $v);
                        if(is_numeric($v)) {
                            $v = $v+0;
                        } else {
                            $v = 0;
                        }
                    }
                }

                // Total
                $spt->data_pendapatan->{$klasifikasi->_id}->dpp =
                    $spt->data_pendapatan->{$klasifikasi->_id}->total;
                $spt->data_pendapatan->{$klasifikasi->_id}->tarif = $tarif;
                $spt->data_pendapatan->{$klasifikasi->_id}->harga =
                    $spt->data_pendapatan->{$klasifikasi->_id}->harga;
                $spt->data_pendapatan->{$klasifikasi->_id}->pajak =
                    ceil(
                        $spt->data_pendapatan->{$klasifikasi->_id}->dpp
                        *
                        $mathTarif
                    )
                ;

                // Sum Jumlah_DPP + Jumlah_Pajak
                $jumlahDpp += $spt->data_pendapatan->{$klasifikasi->_id}->dpp;
                $jumlahPajak += $spt->data_pendapatan->{$klasifikasi->_id}->pajak;
            }
        }

        // Assign
        $spt->jumlah_dpp = $jumlahDpp;
        $spt->jumlah_pajak = $jumlahPajak;
        $spt->denda_pajak = $dendaPajak;
        $spt->potongan_pajak = $potonganPajak;

        try {
            // Save
            $spt->save(Auth::user());
        } catch(\Exception $e) {
            throw $e;
            return redirect()->back()->withInput()->withErrors([
                'message' => $e->getMessage()
            ]);
        }

        $request->session()->flash('message', 'save-successful');
        return redirect('/pelaporan/'.$objekPajakId.'/form/'.$id.'/print');
    }

    /**
     * Print SPT
     */
    public function viewDetail($objekPajakId, $id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        $wajibPajak = $objekPajak->subjek_pajak;

        // Check ownership if not admin/fiskus
        if(Auth::user()->checkRole('wajibPajak')) {
            if($objekPajak->subjek_pajak->_id !== Auth::user()->subjek_pajak->_id) {
                abort(404);
            }
        }

        $spt = SPT::find(Auth::user(), $id);
        if(!$id) { abort(404); }
        // If form is empty
        if(empty($spt->data_pendapatan)) {
            abort(404);
        }

        // if mismatch
        if($spt->objek_pajak->_id !== $objekPajak->_id) {
            abort(404);
        }

        if(gettype($spt->pembayar_pajak) === 'string') {
            $spt->pembayar_pajak = WajibPajak::find(Auth::user(), $spt->pembayar_pajak);
        }

        return view('pelaporan/detail', [
            'objekPajak' => $objekPajak,
            'wajibPajak' => $wajibPajak,
            'spt' => $spt,
            'sidebarUrl' => url('pelaporan')
        ]);
    }

    /**
     * Print SPT
     */
    public function printFormDetail($objekPajakId, $id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        $wajibPajak = $objekPajak->subjek_pajak;

        // Check ownership if not admin/fiskus
        if(Auth::user()->checkRole('wajibPajak')) {
            if($objekPajak->subjek_pajak->_id !== Auth::user()->subjek_pajak->_id) {
                abort(404);
            }
        }

        $spt = SPT::find(Auth::user(), $id);
        if(!$id) { abort(404); }
        // If form is empty
        if(empty($spt->data_pendapatan)) {
            abort(404);
        }

        // if mismatch
        if($spt->objek_pajak->_id !== $objekPajak->_id) {
            abort(404);
        }

        if(gettype($spt->pembayar_pajak) === 'string') {
            $spt->pembayar_pajak = WajibPajak::find(Auth::user(), $spt->pembayar_pajak);
        }

        return view('pelaporan/print', [
            'objekPajak' => $objekPajak,
            'wajibPajak' => $wajibPajak,
            'spt' => $spt,
            'sidebarUrl' => url('pelaporan')
        ]);
    }
}
