<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Show form
     */
    public function form()
    {
        return view('login');
    }

    /**
     * Attempt login
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials, true)) {
            // Authentication passed
            return redirect()->intended('/dashboard');
        } else {
            // Authentication failure
            return redirect()->back()->withInput($request->only('username'))->withErrors([
                'wrong-password' => true
            ]);
        }
    }

    /**
     * Logout
     */
    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
