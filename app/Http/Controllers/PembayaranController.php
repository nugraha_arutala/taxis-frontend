<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\WajibPajak;
use App\ObjekPajak;
use App\KlasifikasiPajak;
use App\SPT;

class PembayaranController extends Controller
{
    /**
     * View data table
     */
    public function viewTable()
    {
        $jenisPajak = KlasifikasiPajak::parents(Auth::user());
        $jenisPajak = KlasifikasiPajak::structurize($jenisPajak->items);

        return view('objek-pajak/datatable', [
            'page' => "pembayaran",
            'jenisPajak' => $jenisPajak
        ]);
    }

    /**
     * View pembayaran per objek
     */
    public function viewObjek($id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $id);
        if(!$objekPajak) { abort(404); }
        return view('pembayaran/objek', [
            'objekPajak' => $objekPajak,
            'pembayar' => Auth::user()->checkRole('wajibPajak'),
            'sidebarUrl' => url('pembayaran')
        ]);
    }

    /**
     * Generate datatable JSON
     */
    public function objekDatatableJson(Request $request, $objekPajakId = null)
    {
        $filter = $request->input('filter', null);
        if($objekPajakId) {
            if($filter === null) { $filter = []; }
            $filter['objek'] = $objekPajakId;
        }

        $params = [
            Auth::user(),
            $request->input('length', 10),
            $request->input('start', 0),
            $request->input('order', null),
            $request->input('search', null),
            $filter
        ];
        $data = SPT::index(...$params);
        return json_encode($this->makeDatatableJson($data, $request->input('draw', 0)));
    }

    public function paymentPage($objekPajakId, $id)
    {
        if(Auth::user()->checkRole('wajibPajak')) {
            return $this->askPaymentMethod($objekPajakId, $id);
        } else {
            return $this->askPaymentConfirmation($objekPajakId, $id);
        }
    }

    /**
     * Ask payment method page (WP/SP)
     */
    private function askPaymentMethod($objekPajakId, $id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        $user = User::find(Auth::user(), Auth::user()->_id);
        $wajibPajak = $user->subjek_pajak; // FIXME
        if(!$wajibPajak) {
            abort(404);
        }

        $spt = SPT::find(Auth::user(), $id);
        if(!$id) { abort(404); }

        // if mismatch
        if($spt->objek_pajak->_id !== $objekPajak->_id) {
            abort(404);
        }

        return view('pembayaran/paymentMethod', [
            'objekPajak' => $objekPajak,
            'wajibPajak' => $wajibPajak,
            'spt' => $spt,
            'sidebarUrl' => url('pembayaran')
        ]);
    }

    /**
     * Ask payment confirmation page (Fiskus, Admin)
     */
    private function askPaymentConfirmation($objekPajakId, $id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        $spt = SPT::find(Auth::user(), $id);
        if(!$id) { abort(404); }

        // if mismatch
        if($spt->objek_pajak->_id !== $objekPajak->_id) {
            abort(404);
        }

        return view('pembayaran/confirmPayment', ['objekPajak' => $objekPajak, 'spt' => $spt, 'sidebarUrl' => url('pembayaran')]);
    }

    public function pay(Request $request, $objekPajakId, $id)
    {
        if(Auth::user()->checkRole('wajibPajak')) {
            return $this->setPaymentMethod($request, $objekPajakId, $id);
        } else {
            return $this->doConfirmPayment($request, $objekPajakId, $id);
        }
    }

    /**
     * Set payment method
     */
    private function setPaymentMethod(Request $request, $objekPajakId, $id)
    {
        $request->validate([
            'data_pembayaran.cara_pembayaran' => 'required|in:atm,teller,instantpay,emoney'
        ]);

        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        $spt = SPT::find(Auth::user(), $id);
        if(!$id) { abort(404); }

        // if mismatch
        if($spt->objek_pajak->_id !== $objekPajak->_id) {
            abort(404);
        }

        $user = User::find(Auth::user(), Auth::user()->_id);

        $set = $spt->setPaymentData(
            Auth::user(), $request->input('data_pembayaran.cara_pembayaran'),
            $user->subjek_pajak, date('Y-m-d')
        ); // TODO FIXME

        if($set) {
            return redirect('/pembayaran/'.$objekPajak->_id.'/detail-pembayaran/'.$spt->_id);
        } else {
            $request->session()->flash('message', 'set-failed');
            return redirect('/pembayaran/'.$objekPajak->_id);
        }
    }

    /**
     * Actually confirm the payment (process)
     */
    private function doConfirmPayment(Request $request, $objekPajakId, $id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        $spt = SPT::find(Auth::user(), $id);
        if(!$id) { abort(404); }

        // if mismatch
        if($spt->objek_pajak->_id !== $objekPajak->_id) {
            abort(404);
        }

        $setStatus = $spt->setStatus(Auth::user(), 1);

        if($setStatus) {
            return redirect('/pembayaran/'.$objekPajak->_id.'/detail/'.$spt->_id);
        } else {
            $request->session()->flash('message', 'confirm-failed');
            return redirect('/pembayaran/'.$objekPajak->_id);
        }
    }

    /**
     * View detail pembayaran
     */
    public function viewPaymentDetail(Request $request, $objekPajakId, $id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        $spt = SPT::find(Auth::user(), $id);
        if(!$id) { abort(404); }

        // if mismatch
        if($spt->objek_pajak->_id !== $objekPajak->_id) {
            abort(404);
        }

        $user = User::find(Auth::user(), Auth::user()->_id);
        $wajibPajak = $user->subjek_pajak; // FIXME
        if(!$wajibPajak) {
            abort(404);
        }

        return view('pembayaran/detailPembayaran', ['objekPajak' => $objekPajak, 'spt' => $spt, 'wajibPajak' => $wajibPajak, 'sidebarUrl' => url('pembayaran')]);
    }

    /**
     * View detail SPD
     */
    public function viewSPDDetail($objekPajakId, $id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        $wajibPajak = $objekPajak->subjek_pajak;

        $spt = SPT::find(Auth::user(), $id);
        if(!$id) { abort(404); }

        // if mismatch
        if($spt->objek_pajak->_id !== $objekPajak->_id) {
            abort(404);
        }

        // calc pendapatan
        $dataPendapatan = null;
        if (isset($spt->data_pendapatan)) {
            if($objekPajak->jenis_pajak->kode == '4.1.1.02') {
                // compile
                $dataPendapatan = (object) ['makanan' => 0, 'service' => 0, 'lainnya' => 0];
                foreach($spt->data_pendapatan as $i) {
                    $dataPendapatan->makanan += ($i->makanan ?? 0);
                    $dataPendapatan->service += ($i->service ?? 0);
                    $dataPendapatan->lainnya += ($i->lainnya ?? 0);
                }
            } else {
                // compile
                $dataPendapatan = (object) ['total' => 0];
                foreach($spt->data_pendapatan as $i) {
                    $dataPendapatan->total += ($i->total ?? 0);
                }
            }
        } elseif (isset($spt->data_tagihan)) {
            // compile
            $dataPendapatan = (object) ['makanan' => 0, 'service' => 0, 'lainnya' => 0, 'total' => 0];
            foreach($spt->data_tagihan as $i) {
                $dataPendapatan->total += ($i->total ?? 0);
            }
        }

        return view('pembayaran/detail', [
            'objekPajak' => $objekPajak,
            'wajibPajak' => $wajibPajak,
            'spt' => $spt,
            'dataPendapatan' => $dataPendapatan,
            'sidebarUrl' => url('pembayaran')
        ]);
    }

    /**
     * Print detail SPD
     */
    public function printSspd($objekPajakId, $id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        $wajibPajak = $objekPajak->subjek_pajak;

        $spt = SPT::find(Auth::user(), $id);
        if(!$id) { abort(404); }

        // if mismatch
        if($spt->objek_pajak->_id !== $objekPajak->_id) {
            abort(404);
        }

        return view('pembayaran/printSPD', [
            'objekPajak' => $objekPajak,
            'wajibPajak' => $wajibPajak,
            'spt' => $spt,
            'sidebarUrl' => url('pembayaran')
        ]);
    }

    /**
     * Print detail pembayaran
     */
    public function printInvoice($objekPajakId, $id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        $spt = SPT::find(Auth::user(), $id);
        if(!$id) { abort(404); }

        $wajibPajak = $spt->pembayar_pajak;
        if(gettype($wajibPajak) === 'string') {
            $wajibPajak = WajibPajak::find(Auth::user(), $wajibPajak);
            if(!$wajibPajak) {
                throw new \Error('404 Pembayar Pajak (print Invoice page)');
                abort(500);
            }
        }

        // if mismatch
        if($spt->objek_pajak->_id !== $objekPajak->_id) {
            abort(404);
        }

        return view('pembayaran/printInvoice', [
            'objekPajak' => $objekPajak,
            'wajibPajak' => $wajibPajak,
            'spt' => $spt,
            'sidebarUrl' => url('pembayaran')
        ]);
    }
}
