<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\WajibPajak;
use App\ObjekPajak;
use App\KlasifikasiPajak;
use App\SPT;
use App\SuratTeguran;

class SKPDController extends Controller
{
    /**
     * View data table
     */
    public function viewTable()
    {
        $jenisPajak = KlasifikasiPajak::parents(Auth::user());
        $jenisPajak = KlasifikasiPajak::structurize($jenisPajak->items);

        // get subjek pajak details
        if(gettype(Auth::user()->subjek_pajak) === "string") {
            Auth::user()->subjek_pajak = WajibPajak::find(Auth::user(), Auth::user()->subjek_pajak);
        }

        return view('skpd/datatable', [
            'jenisPajak' => $jenisPajak
        ]);
    }

    /**
     * Generate datatable JSON
     */
    public function datatableJson(Request $request)
    {
        // Restrict to fiskus & admin
        if(!Auth::user()->checkRole('fiskus') && !Auth::user()->checkRole('admin')) {
            abort(404);
        }

        //get data Surat Teguran prev month
        $params_teguran = [
            Auth::user(),
            100,
            0,
            null,
            null,
            [
                'sdate' => date("Y-n-j", strtotime("first day of previous month")),
                'edate' => date("Y-n-j", strtotime("last day of previous month")),
            ]
        ];

        $teguran = SuratTeguran::index(...$params_teguran);
        $objek_ids = [];
        foreach ($teguran->items as $key => $value) {
            if(!empty($value->objek_pajak)){
                $objek_ids[] = $value->objek_pajak;
            }
        }

        $filter = $request->input('filter', null);
        // If not super admin, restrict objek pajak to self
        if(Auth::user()->checkRole('wajibPajak')) {
            if($filter === null) { $filter = []; }
            $filter['subjek'] = Auth::user()->subjek_pajak;
        }

        // These are technically filters
        $columns = $request->input('columns');
        if(!empty($columns[5]['search']['value'])) {
            $filter['status'] = $columns[5]['search']['value'];
        }
        if(!empty($columns[4]['search']['value'])) {
            $filter['jenis_pajak'] = $columns[4]['search']['value'];
        }
        if(!empty($objek_ids)) {
            $filter['ids'] = $objek_ids;
        } else {
            $filter['ids'] = [0];
        }

        $params = [
            Auth::user(),
            $request->input('length', 10),
            $request->input('start', 0),
            $request->input('order', null),
            $request->input('search', null),
            $filter
        ];
        $data = ObjekPajak::index(...$params);
        return json_encode($this->makeDatatableJson($data, $request->input('draw', 0)));
    }

    /**
     * View skpd per objek
     */
    public function viewObjek($id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $id);
        if(!$objekPajak) { abort(404); }

        // Check ownership if not admin/fiskus
        if(Auth::user()->checkRole('wajibPajak')) {
            if($objekPajak->subjek_pajak->_id !== Auth::user()->subjek_pajak->_id) {
                abort(404);
            }
        }

        return view('skpd/objek', [
            'objekPajak' => $objekPajak,
            'sidebarUrl' => url('skpd')
        ]);
    }

    /**
     * Generate datatable JSON
     */
    public function objekDatatableJson(Request $request, $objekPajakId = null)
    {
        $filter = $request->input('filter', null);
        if($objekPajakId) {
            if($filter === null) { $filter = []; }
            $filter['objek'] = $objekPajakId;
        }
        $filter['is_skpd'] = "true";

        $params = [
            Auth::user(),
            $request->input('length', 10),
            $request->input('start', 0),
            $request->input('order', null),
            $request->input('search', null),
            $filter
        ];
        $data = SPT::index(...$params);
        return json_encode($this->makeDatatableJson($data, $request->input('draw', 0)));
    }

    /**
     * Form new SPT
     */
    public function createForm($objekPajakId)
    {
        // Get OP
        //get data Surat Teguran prev month
        $filter['objek'] = $objekPajakId;
        $params_teguran = [
            Auth::user(),
            -1,
            0,
            null,
            null,
            $filter
        ];

        $teguran = SuratTeguran::index(...$params_teguran);
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        // Check ownership if not admin/fiskus
        if(Auth::user()->checkRole('wajibPajak')) {
            if($objekPajak->subjek_pajak->_id !== Auth::user()->subjek_pajak->_id) {
                abort(404);
            }
        }

        return view('skpd/create', [
            'objekPajak' => $objekPajak,
            'suratTeguran' => $teguran->items,
            'sidebarUrl' => url('skpd')
        ]);
    }

    public function createProcess(Request $request, $objekPajakId)
    {
        $request->validate([
            'bulan' => 'required|in:01,02,03,04,05,06,07,08,09,10,11,12',
            'tahun' => 'required|gte:2018|lte:'.date('Y')
        ]);

        // Get OP
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        $params = [
            Auth::user(),
            $request->input('length', 10),
            $request->input('start', 0),
            $request->input('order', null),
            $request->input('search', null),
            $filter = array(
                'objek' => $objekPajakId,
                'periode' => array(
                    'bulan' => $request->input('bulan'),
                    'tahun' => $request->input('tahun'),
                )
            )
        ];

        $data = SPT::index(...$params);
        if($data->count){
            return redirect()->back()->withInput()->withErrors([
                'message' => "Duplicate SKPD atau user telah lapor Pajak"
            ]);
        } else {
            // Check ownership if not admin/fiskus
            if(Auth::user()->checkRole('wajibPajak')) {
                if($objekPajak->subjek_pajak->_id !== Auth::user()->subjek_pajak->_id) {
                    abort(404);
                }
            }

            $spt = SPT::create(Auth::user(), $objekPajak, $request->input('bulan'), $request->input('tahun'), $request->input('no_surat_teguran'));

            $request->session()->flash('message', 'create-successful');
            $request->session()->flash('spt', $spt->_id);
            return redirect('/skpd/'.$objekPajakId);
        }
    }

    /**
     * Form pelaporan
     */
    public function form($objekPajakId, $id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        // Check ownership if not admin/fiskus
        if(Auth::user()->checkRole('wajibPajak')) {
            if($objekPajak->subjek_pajak->_id !== Auth::user()->subjek_pajak->_id) {
                abort(404);
            }
        }

        $spt = SPT::find(Auth::user(), $id);
        if(!$id) { abort(404); }

        // no editing after payment
        if($spt->status === 1) {
            abort(404);
        }

        // if mismatch
        if($spt->objek_pajak->_id !== $objekPajak->_id) {
            abort(404);
        }

        return view('skpd/form', [
            'objekPajak' => $objekPajak,
            'spt' => $spt,
            'sidebarUrl' => url('skpd')
        ]);
    }

    public function save(Request $request, $objekPajakId, $id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        // Check ownership if not admin/fiskus
        if(Auth::user()->checkRole('wajibPajak')) {
            if($objekPajak->subjek_pajak->_id !== Auth::user()->subjek_pajak->_id) {
                abort(404);
            }
        }

        $spt = SPT::find(Auth::user(), $id);
        if(!$id) { abort(404); }

        // no editing after payment
        if($spt->status === 1) {
            abort(404);
        }

        // if mismatch
        if($spt->objek_pajak->_id !== $objekPajak->_id) {
            abort(404);
        }

        // Assign input values, calculate DPP
        $klasifikasiMap = $request->input('klasifikasi');
        $dataPendapatan = $request->input('data_tagihan');
        $spt->data_tagihan = (object) [];
        $jumlahDpp = 0;
        $jumlahPajak = 0;
        $dendaPajak = 0;
        $potonganPajak = 0;

        foreach($dataPendapatan as $klasifikasiId => $i) {
            $klasifikasi = KlasifikasiPajak::find(Auth::user(), $klasifikasiMap[$klasifikasiId]);
            if(!$klasifikasi) { continue; }

            if(isset($i['total'])) {
                $spt->data_tagihan->{$klasifikasi->_id} = (object) [
                    'tagihan' => $i['total']['tagihan'],
                    'denda' => $i['total']['denda'],
                ];
                // decleave
                foreach($spt->data_tagihan->{$klasifikasi->_id} as $key => &$v) {
                    $v = str_replace('.', '', $v);
                    $v = str_replace(',', '.', $v);
                    if(is_numeric($v)) {
                        $v = $v+0;
                    } else {
                        $v = 0;
                    }
                }
            }

            // Total
            $spt->data_tagihan->{$klasifikasi->_id}->pajak =
                $spt->data_tagihan->{$klasifikasi->_id}->tagihan +
                $spt->data_tagihan->{$klasifikasi->_id}->denda;
            $spt->data_tagihan->{$klasifikasi->_id}->total =
                $spt->data_tagihan->{$klasifikasi->_id}->tagihan +
                $spt->data_tagihan->{$klasifikasi->_id}->denda;

            // Sum Jumlah_DPP + Jumlah_Pajak
            $dendaPajak += $spt->data_tagihan->{$klasifikasi->_id}->denda;
            $jumlahPajak += $spt->data_tagihan->{$klasifikasi->_id}->pajak;

        }

        // Assign
        $spt->jumlah_pajak = $jumlahPajak;
        $spt->denda_pajak = $dendaPajak;
        $spt->potongan_pajak = $potonganPajak;

        try {
            // Save
            $spt->save(Auth::user());
        } catch(\Exception $e) {
            throw $e;
            return redirect()->back()->withInput()->withErrors([
                'message' => $e->getMessage()
            ]);
        }

        $request->session()->flash('message', 'save-successful');
        return redirect('/skpd/'.$objekPajakId.'/form/'.$id.'/print');
    }

    /**
     * Print SPT
     */
    public function printFormDetail($objekPajakId, $id)
    {
        $objekPajak = ObjekPajak::find(Auth::user(), $objekPajakId);
        if(!$objekPajak) { abort(404); }

        $wajibPajak = $objekPajak->subjek_pajak;

        // Check ownership if not admin/fiskus
        if(Auth::user()->checkRole('wajibPajak')) {
            if($objekPajak->subjek_pajak->_id !== Auth::user()->subjek_pajak->_id) {
                abort(404);
            }
        }

        $spt = SPT::find(Auth::user(), $id);
        if(!$id) { abort(404); }
        // If form is empty
        if(empty($spt->data_tagihan)) {
            abort(404);
        }

        // if mismatch
        if($spt->objek_pajak->_id !== $objekPajak->_id) {
            abort(404);
        }

        if(gettype($spt->pembayar_pajak) === 'string') {
            $spt->pembayar_pajak = WajibPajak::find(Auth::user(), $spt->pembayar_pajak);
        }

        return view('skpd/print', [
            'objekPajak' => $objekPajak,
            'wajibPajak' => $wajibPajak,
            'spt' => $spt,
            'sidebarUrl' => url('skpd')
        ]);
    }
}
