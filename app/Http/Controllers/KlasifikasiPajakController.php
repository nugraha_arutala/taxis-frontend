<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\KlasifikasiPajak;

class KlasifikasiPajakController extends Controller
{
    /**
     * View data table
     */
    public function viewTable()
    {
        return view('master/jenis-klasifikasi/datatable');
    }

    public function viewDetail($id)
    {
        $klasifikasiPajak = KlasifikasiPajak::find(Auth::user(), $id);
        if(!$klasifikasiPajak) { abort(404); }
        if($klasifikasiPajak->parent_id) {
            $klasifikasiPajak->parent = KlasifikasiPajak::find(Auth::user(), $klasifikasiPajak->parent_id) ?? null;
        }
        return view('master/jenis-klasifikasi/detail', ['klasifikasiPajak' => $klasifikasiPajak]);
    }

    public function findJenisPajak(Request $request)
    {
        if($request->input('id') || $request->input('_id')) {
            $id = $request->input('id') ?? $request->input('_id');
            $klasifikasiPajak = KlasifikasiPajak::find(Auth::user(), $id);
            if(!$klasifikasiPajak) { abort(404); }
            return response()->json($klasifikasiPajak);
        } else
        if($request->input('kode')) {
            $klasifikasiPajak = KlasifikasiPajak::findJenisPajakByKode(Auth::user(), $request->input('kode'));
            if(!$klasifikasiPajak) { abort(404); }
            return response()->json($klasifikasiPajak);
        }

        // if no query to base search on
        abort(404);
    }

    /**
     * Generate datatable JSON
     */
    public function datatableJson(Request $request)
    {
        $params = [
            Auth::user(),
            $request->input('length', 10),
            $request->input('start', 0),
            $request->input('order', null),
            $request->input('search.value', null)
        ];
        $data = KlasifikasiPajak::index(...$params);
        foreach($data->items as &$i) {
            if($i->parent_id) {
                $i->parent = KlasifikasiPajak::find(Auth::user(), $i->parent_id);
            }
        }
        return json_encode($this->makeDatatableJson($data, $request->input('draw', 0)));
    }

    /**
     * New data form
     */
    public function form($id = null)
    {
        $vars = [];

        $listParent = KlasifikasiPajak::parents(Auth::user());
        $listParent = KlasifikasiPajak::structurize($listParent->items);
        $vars['listParent'] = $listParent;

        if($id) {
            $klasifikasiPajak = KlasifikasiPajak::find(Auth::user(), $id);
            if(!$klasifikasiPajak) { abort(404); }
            if($klasifikasiPajak->parent_id) {
                $klasifikasiPajak->parent = KlasifikasiPajak::find(Auth::user(), $klasifikasiPajak->parent_id) ?? null;
            }
            $vars['klasifikasiPajak'] = $klasifikasiPajak;
        }
        $vars['sidebarUrl'] = url('master-data/jenis-klasifikasi');
        return view('master/jenis-klasifikasi/form', $vars);
    }

    /**
     * Process form
     */
    public function post(Request $request, $id = null)
    {
        $request->validate([
            'kode' => 'required',
            'nama' => 'required',
            'parent' => 'required_if:tipe,klasifikasi_usaha',
            'tarif' => 'required_if:tipe,klasifikasi_usaha'
        ]);

        $jenisParent = null;
        if($request->input('tipe') === 'klasifikasi_usaha') {
            // confirm parent is correct
            $jenisParent = KlasifikasiPajak::find(Auth::user(), $request->input('parent'));
            if(!$jenisParent) {
                return redirect()->back()->withInput()->withErrors([
                    'message' => "Jenis Pajak dengan kode tersebut tidak ditemukan. Mohon periksa lagi."
                ]);
            }
        }

        if(!$request->input('_id') ?? $id ?? false) {
            // Create new model object
            $klasifikasiPajak = new KlasifikasiPajak();
        } else {
            // Find object
            $klasifikasiPajak = KlasifikasiPajak::find(Auth::user(), $request->input('_id') ?? $id);
            if(!$klasifikasiPajak) { abort(404); }
        }

        // Assign input values
        $klasifikasiPajak->kode = $request->input('kode');
        $klasifikasiPajak->nama = $request->input('nama');
        $klasifikasiPajak->deskripsi = $request->input('deskripsi');
        if($jenisParent) {
            $klasifikasiPajak->parent_id = $jenisParent->_id;
            $klasifikasiPajak->tarif = $request->input('tarif');
            if ($request->input('harga')){
                $klasifikasiPajak->harga = $request->input('harga');
            }
        } else {
            $klasifikasiPajak->parent_id = null;
        }

        try {
            // Save
            $klasifikasiPajak->save(Auth::user());
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withErrors([
                'message' => $e->getMessage()
            ]);
        }

        $request->session()->flash('message', 'save-successful');
        return redirect('/master-data/jenis-klasifikasi');
    }
}
