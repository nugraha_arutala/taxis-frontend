<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\WajibPajak;
use App\User;

class UserController extends Controller
{
    /**
     * View data table
     */
    public function viewTable()
    {
        if(!Auth::user()->checkRole('admin')) {
            abort('404');
        }

        return view('user/datatable');
    }

    public function find(Request $request)
    {
        // TODO
        abort(404);
    }

    public function viewDetail($id)
    {
        if(!Auth::user()->checkRole('admin')) {
            abort('404');
        }

        $user = User::find(Auth::user(), $id);
        if(!$user) { abort(404); }

        if(!empty($user->subjek_pajak)) {
            if(gettype($user->subjek_pajak) === 'string') {
                $user->subjek_pajak = WajibPajak::find(Auth::user(), $i->subjek_pajak);
                if(!$user->subjek_pajak) {
                    $user->subjek_pajak = null;
                }
            }
        }

        return view('user/detail', ['user' => $user, 'sidebarUrl' => url('lihat-data/user')]);
    }

    /**
     * Generate datatable JSON
     */
    public function datatableJson(Request $request)
    {
        if(!Auth::user()->checkRole('admin')) {
            abort('404');
        }

        $filter = $request->input('filter', null);
        // Technically a filter
        $columns = $request->input('columns');
        if (isset($columns[3]['search']['value'])) {
            if (strlen((string) $columns[3]['search']['value']) > 0) {
                $filter['roles'] = $columns[3]['search']['value'];
            }
        }

        $params = [
            Auth::user(),
            $request->input('length', 10),
            $request->input('start', 0),
            $request->input('order', null),
            $request->input('search.value', null),
            $filter
        ];
        $data = User::index(...$params);

        foreach($data->items as &$i) {
            // get subjek_pajak
            if(!empty($i->subjek_pajak)) {
                if(gettype($i->subjek_pajak) === "string") {
                    $subjekPajak = WajibPajak::find(Auth::user(), $i->subjek_pajak);
                    if($subjekPajak) {
                        $i->subjek_pajak = $subjekPajak;
                    } else {
                        $i->subjek_pajak = null;
                    }
                }
            }

            if(!empty($i->roles)) {
                $dummy = new User;
                $dummy->roles = $i->roles;
                $i->roles = $dummy->getRoleNames();
            }
        }

        return json_encode($this->makeDatatableJson($data, $request->input('draw', 0)));
    }

    /**
     * New data form
     */
    public function formNew()
    {
        if(!Auth::user()->checkRole('admin')) {
            abort('404');
        }

        return view('user/form');
    }

    /**
     * Edit existing data form
     */
    public function formEdit($id)
    {
        if(!Auth::user()->checkRole('admin')) {
            abort('404');
        }

        $user = User::find(Auth::user(), $id);
        if(!$user) { abort(404); }
        return view('user/form', ['user' => $user, 'sidebarUrl' => url('lihat-data/user')]);
    }

    /**
     * Process data
     */
    public function post(Request $request)
    {
        if(!Auth::user()->checkRole('admin')) {
            abort('404');
        }

        if(!$request->input('_id')) {
            // Create new model object
            $user = new User();
        } else {
            // Find object
            $user = User::find(Auth::user(), $request->input('_id'));
            if(!$user) { abort(404); }
        }

        // Assign input values
        $possibleInputs = [
            'nama', 'username', 'password', 'email', 'roles', 'npwpd'
        ];
        foreach($request->input() as $input => $value) {
            if(!in_array($input, $possibleInputs)) { continue; }
            if($input === 'password' && $request->input('_id') && !$value) { unset($user->password); continue; }
            if($input === 'npwpd' && $value) {
                $subjekPajak = WajibPajak::findByNpwpd(Auth::user(), $value);
                if(!$subjekPajak) {
                    return redirect()->back()->withInput()->withErrors([
                        'message' => "Subjek pajak dengan NPWPD tersebut tidak ditemukan."
                    ]);
                }
                $user->subjek_pajak = $subjekPajak->_id;
                continue;
            }
            $user->{$input} = $value;
        }

        try {
            // Save
            $user->save(Auth::user());
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withErrors([
                'message' => $e->getMessage()
            ]);
        }

        $request->session()->flash('message', 'save-successful');
        return redirect('/lihat-data/user');
    }

    public function viewProfile()
    {
        $user = User::find(Auth::user(), Auth::user()->_id);
        if(!$user) { abort(404); }

        if(!empty($user->subjek_pajak)) {
            if(gettype($user->subjek_pajak) === 'string') {
                $user->subjek_pajak = WajibPajak::find(Auth::user(), $i->subjek_pajak);
                if(!$user->subjek_pajak) {
                    $user->subjek_pajak = null;
                }
            }
        }

        return view('user/profil', ['user' => $user]);
    }

    public function editProfileWp()
    {
        $user = Auth::user();
        if(!$user->subjek_pajak) {
            $user = User::find(Auth::user(), Auth::user()->_id);
            if(!$user->subjek_pajak) {
                abort(404);
            }
            if(gettype($user->subjek_pajak) === 'string') {
                $user->subjek_pajak = WajibPajak::find(Auth::user(), $i->subjek_pajak);
                if(!$user->subjek_pajak) {
                    abort(404);
                }
            }
        }
        return view('wajib-pajak/form', ['wajibPajak' => $user->subjek_pajak, 'profile' => true]);
    }

    public function editProfileUser()
    {
        $user = Auth::user();
        if(!$user->subjek_pajak) {
            $user = User::find(Auth::user(), Auth::user()->_id);
        }
        return view('user/form', ['user' => $user, 'profile' => true]);
    }

    public function saveProfileWp(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'jenis' => 'required|in:perorangan,perusahaan,bendaharawan'
        ]);

        // Find object
        if(!Auth::user()->subjek_pajak) {
            abort(404);
        }
        $wajibPajak = WajibPajak::find(Auth::user(), Auth::user()->subjek_pajak->_id ?? Auth::user()->subjek_pajak);
        if(!$wajibPajak) { abort(404); }

        // Assign input values
        $possibleInputs = [
            'nama', 'kewarganegaraan', 'tempat_lahir', 'tanggal_lahir', 'nik', 'paspor', 'npwp', 'jenis',
            'jalan', 'rt', 'rw', 'kode_pos', 'provinsi', 'kota', 'kecamatan', 'kelurahan', 'nomor_telepon', 'nomor_hp', 'email'
        ];
        foreach($request->input() as $input => $value) {
            if(!in_array($input, $possibleInputs)) { continue; }
            $wajibPajak->{$input} = $value;
        }

        try {
            // Save
            $wajibPajak->save(Auth::user());
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withErrors([
                'message' => $e->getMessage()
            ]);
        }

        $request->session()->flash('message', 'save-successful');
        return redirect('/profil');
    }

    public function saveProfileUser(Request $request)
    {
        // Find object
        $user = User::find(Auth::user(), Auth::user()->_id);

        // Assign input values
        $possibleInputs = [
            'nama', 'password', 'email'
        ];
        foreach($request->input() as $input => $value) {
            if(!in_array($input, $possibleInputs)) { continue; }
            if($input === 'password' && !$value) { unset($user->password); continue; }
            $user->{$input} = $value;
        }

        try {
            // Save
            $user->save(Auth::user());
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withErrors([
                'message' => $e->getMessage()
            ]);
        }

        $request->session()->flash('message', 'save-successful');
        return redirect('/profil');
    }
}
