<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\WajibPajak;

class WajibPajakController extends Controller
{
    /**
     * View data table
     */
    public function viewTable()
    {
        return view('wajib-pajak/datatable');
    }

    public function find(Request $request)
    {
        if($request->input('id') || $request->input('_id')) {
            $id = $request->input('id') ?? $request->input('_id');
            $wajibPajak = WajibPajak::find(Auth::user(), $id);
            if(!$wajibPajak) { abort(404); }
            return response()->json($wajibPajak);
        } else
        if($request->input('npwpd')) {
            $wajibPajak = WajibPajak::findByNpwpd(Auth::user(), $request->input('npwpd'));
            if(!$wajibPajak) { abort(404); }
            return response()->json($wajibPajak);
        }

        // if no query to base search on
        abort(404);
    }

    public function viewDetail($id)
    {
        $wajibPajak = WajibPajak::find(Auth::user(), $id);
        if(!$wajibPajak) { abort(404); }
        return view('wajib-pajak/detail', ['wajibPajak' => $wajibPajak]);
    }

    /**
     * Generate datatable JSON
     */
    public function datatableJson(Request $request)
    {
        $params = [
            Auth::user(),
            $request->input('length', 10),
            $request->input('start', 0),
            $request->input('order', null),
            $request->input('search.value', null)
        ];
        $data = WajibPajak::index(...$params);
        return json_encode($this->makeDatatableJson($data, $request->input('draw', 0)));
    }

    /**
     * New data form
     */
    public function formNew()
    {
        return view('wajib-pajak/form');
    }

    /**
     * Edit existing data form
     */
    public function formEdit($id)
    {
        $wajibPajak = WajibPajak::find(Auth::user(), $id);
        if(!$wajibPajak) { abort(404); }
        return view('wajib-pajak/form', ['wajibPajak' => $wajibPajak]);
    }

    /**
     * Process data
     */
    public function post(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'jenis' => 'required|in:perorangan,perusahaan,bendaharawan'
        ]);

        if(!$request->input('_id')) {
            // Create new model object
            $wajibPajak = new WajibPajak();
        } else {
            // Find object
            $wajibPajak = WajibPajak::find(Auth::user(), $request->input('_id'));
            if(!$wajibPajak) { abort(404); }
        }

        // Assign input values
        $possibleInputs = [
            'nama', 'kewarganegaraan', 'tempat_lahir', 'tanggal_lahir', 'nik', 'paspor', 'npwp', 'jenis',
            'jalan', 'rt', 'rw', 'kode_pos', 'provinsi', 'kota', 'kecamatan', 'kelurahan', 'nomor_telepon', 'nomor_hp', 'email',
            'npwpd',
        ];
        foreach($request->input() as $input => $value) {
            if(!in_array($input, $possibleInputs)) { continue; }
            $wajibPajak->{$input} = $value;
        }

        try {
            // Save
            $wajibPajak->save(Auth::user());
        } catch(\Exception $e) {
            return redirect()->back()->withInput()->withErrors([
                'message' => $e->getMessage()
            ]);
        }

        $request->session()->flash('message', 'save-successful');
        return redirect('/lihat-data/wajib-pajak');
    }
}
