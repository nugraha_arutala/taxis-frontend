<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\SPT;

class DashboardController extends Controller
{
    /**
     * Show dashboard view
     */
    public function view()
    {
        $filterSPT['is_skpd'] = "false"; 
        $paramsSPT = [Auth::user(), 1, 0, null, null, $filterSPT];

        $filterSKPD['is_skpd'] = "true"; 
        $paramsSKPD = [Auth::user(), 1, 0, null, null, $filterSKPD];

        $filterPembayaran['status'] = 1; 
        $paramsPembayaran = [Auth::user(), 1, 0, null, null, $filterPembayaran];


        $spt = SPT::index(...$paramsSPT);
        $skpd = SPT::index(...$paramsSKPD);
        $pembayaran = SPT::index(...$paramsPembayaran);

        return view('dashboard', [
            'spt' => $spt,
            'skpd' => $skpd,
            'pembayaran' => $pembayaran
        ]);
    }
}
