<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use App\User;
use App\ObjekPajak;
use App\WajibPajak;
use App\SuratTeguran;

class SuratTeguranController extends Controller
{
    /**
     * View data table
     */
    public function viewTable()
    {
        // Restrict to fiskus & admin
        if(!Auth::user()->checkRole('fiskus') && !Auth::user()->checkRole('admin')) {
            abort(404);
        }

        return view('surat-teguran/datatable');
    }

    /**
     * Generate datatable JSON
     */
    public function datatableJson(Request $request, $subjekPajakId = null)
    {
        // Restrict to fiskus & admin
        if(!Auth::user()->checkRole('fiskus') && !Auth::user()->checkRole('admin')) {
            abort(404);
        }

        $filter = $request->input('filter', null);

        $params = [
            Auth::user(),
            $request->input('length', 10),
            $request->input('start', 0),
            $request->input('order', null),
            $request->input('search', null),
            $filter
        ];
        $data = SuratTeguran::index(...$params);

        // Fetch details
        $cache = [
            'op' => [],
            'wp' => []
        ];
        foreach($data->items as &$i) {
            if(gettype($i->objek_pajak) === 'string') {
                // check cache
                if(isset($cache['op'][$i->objek_pajak])) {
                    $i->objek_pajak = $cache['op'][$i->objek_pajak]; continue;
                }

                $op = ObjekPajak::find(Auth::user(), $i->objek_pajak);
                if(!$op) {
                    $i->objek_pajak = null; continue;
                }
                if(gettype($op->subjek_pajak) === 'string') {
                    // check cache
                    if(isset($cache['wp'][$op->subjek_pajak])) {
                        $op->subjek_pajak = $cache['wp'][$op->subjek_pajak];
                    } else {
                        $wp = SubjekPajak::find(Auth::user(), $op->subjek_pajak);
                        if(!$wp) {
                            $i->subjek_pajak = null;
                        } else {
                            $cache['wp'][$wp->_id] = $wp;
                            $op->subjek_pajak = $wp;
                        }
                    }
                }
                $cache['op'][$op->_id] = $op;
                $i->objek_pajak = $op;
            }
        }

        return json_encode($this->makeDatatableJson($data, $request->input('draw', 0)));
    }

    public function formNew()
    {
        if(!Auth::user()->checkRole('admin') && !Auth::user()->checkRole('fiskus')) {
            abort('404');
        }

        return view('surat-teguran/form', [
            'sidebarUrl' => url('surat-teguran')
        ]);
    }

    public function formEdit($id)
    {
        if(!Auth::user()->checkRole('admin') && !Auth::user()->checkRole('fiskus')) {
            abort('404');
        }

        // Find object
        $suratTeguran = SuratTeguran::find(Auth::user(), $id);
        if(!$suratTeguran) { abort(404); }
        if(gettype($suratTeguran->objek_pajak) === 'string') {
            $suratTeguran->objek_pajak = ObjekPajak::find(Auth::user(), $suratTeguran->objek_pajak);
        }

        return view('surat-teguran/form', [
            'suratTeguran' => $suratTeguran,
            'sidebarUrl' => url('surat-teguran')
        ]);
    }

    public function save(Request $request)
    {
        $validation = [
            'objek_pajak' => 'required',
            'no_surat_teguran' => 'required',
            'bulan' => 'required|in:01,02,03,04,05,06,07,08,09,10,11,12',
            'tahun' => 'required|gte:2018|lte:'.date('Y')
        ];
        if(!$request->input('_id')) {
            $validation['berkas_surat_teguran'] = 'required';
        }
        $request->validate($validation);

        // check OP
        $objekPajak = ObjekPajak::find(Auth::user(), $request->input('objek_pajak'));
        if(!$objekPajak) {
            return redirect()->back()->withInput()->withErrors([
                'Objek Pajak tidak ditemukan.'
            ]);
        }

        if(!$request->input('_id')) {
            // Create new model object
            $suratTeguran = new SuratTeguran();
        } else {
            // Find object
            $suratTeguran = SuratTeguran::find(Auth::user(), $request->input('_id'));
            if(!$suratTeguran) { abort(404); }
        }

        if($request->hasFile('berkas_surat_teguran')) {
            // store file
            $filepath = $request->file('berkas_surat_teguran')->store('surat-teguran');
            if(!$filepath) {
                return redirect()->back()->withInput()->withErrors([
                    'Gagal mengupload berkas. Cek kembali berkas dan coba lagi.'
                ]);
            }
            $suratTeguran->berkas = $filepath;
            $suratTeguran->gambar = $filepath;
        }
        $suratTeguran->objekpajak_id = $objekPajak->_id;
        $suratTeguran->no_surat_teguran = $request->input('no_surat_teguran');
        $suratTeguran->periode = (object) [
            'tahun' => $request->input('tahun'),
            'bulan' => $request->input('bulan')
        ];

        try {
            // Save
            $suratTeguran->save(Auth::user());
        } catch(\Exception $e) {
            throw $e;
            return redirect()->back()->withInput()->withErrors([
                'message' => $e->getMessage()
            ]);
        }

        $request->session()->flash('message', 'save-successful');
        return redirect('/surat-teguran');
    }

    public function viewFile($id)
    {
        $suratTeguran = SuratTeguran::find(Auth::user(), $id);
        if(!$suratTeguran) { abort(404); }

        try {
            $contents = Storage::get($suratTeguran->gambar);
            $type = Storage::mimeType($suratTeguran->gambar);
        } catch(\Exception $e) {
            exit('File tidak ditemukan. Mohon unggah ulang.');
        }

        $response = Response::make($contents, 200);
        $response->header("Content-Type", $type);

        return $response;
    }
}
