<?php

namespace App;

use App\Contracts\ApiModelContract;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException as HttpClientException;
use GuzzleHttp\Exception\ConnectException as HttpConnectException;
use GuzzleHttp\Exception\ServerException as HttpServerException;
use Illuminate\Support\Facades\Log;

class ObjekPajak extends ApiModel implements ApiModelContract
{
    /**
     * Returns the endpoint URL for this object model. It's a function so that it doesn't show up on the object's property list.
     * @return string The endpoint URL.
     */
    protected static function endpointUrl()
    {
        return env('API_URL', 'http://localhost:3000') . "/pajak/objek";
    }

    /**
     * Returns required fields. It's a function so that it doesn't show up on the object's property list
     * @return array Array of string listing required field names.
     */
    public function required()
    {
        return ['subjek_pajak', 'jenis_pajak', 'klasifikasi_pajak'];
    }

    protected function combineAlamat()
    {
        if(empty($this->alamat)) {
            $this->alamat = (object) [];
        } else if(gettype($this->alamat) === "array") {
            $this->alamat = (object) $this->alamat;
        }

        if(isset($this->jalan)) {
            $this->alamat->jalan = $this->jalan;
            unset($this->jalan);
        }
        if(isset($this->rt)) {
            $this->alamat->rt = $this->rt;
            unset($this->rt);
        }
        if(isset($this->rw)) {
            $this->alamat->rw = $this->rw;
            unset($this->rw);
        }
        if(isset($this->kelurahan)) {
            $this->alamat->kelurahan = $this->kelurahan;
            unset($this->kelurahan);
        }
        if(isset($this->kecamatan)) {
            $this->alamat->kecamatan = $this->kecamatan;
            unset($this->kecamatan);
        }
        if(isset($this->kota)) {
            $this->alamat->kota = $this->kota;
            unset($this->kota);
        }
        if(isset($this->provinsi)) {
            $this->alamat->provinsi = $this->provinsi;
            unset($this->provinsi);
        }
        if(isset($this->kode_pos)) {
            $this->alamat->kode_pos = $this->kode_pos;
            unset($this->kode_pos);
        }

        return true;
    }

    protected function combineDetails()
    {
        if(empty($this->detail_objek)) {
            $this->detail_objek = (object) [];
        } else if(gettype($this->detail_objek) === "array") {
            $this->detail_objek = (object) $this->detail_objek;
        }

        $this->combineAlamat();
        $this->detail_objek->alamat = $this->alamat;
        unset($this->alamat);

        if(isset($this->nama)) {
            $this->detail_objek->nama = $this->nama;
            unset($this->nama);
        }

        if(isset($this->nomor_telepon)) {
            $this->detail_objek->nomor_telepon = $this->nomor_telepon;
            unset($this->nomor_telepon);
        }

        if(isset($this->nomor_hp)) {
            $this->detail_objek->nomor_hp = $this->nomor_hp;
            unset($this->nomor_hp);
        }

        if(isset($this->status_usaha)) {
            $this->detail_objek->status_usaha = $this->status_usaha;
            unset($this->status_usaha);
        }

        if(isset($this->jenis_masakan_utama)) {
            $this->detail_objek->jenis_masakan_utama = $this->jenis_masakan_utama;
            unset($this->jenis_masakan_utama);
        }

        if(isset($this->luas_tanah)) {
            $this->detail_objek->luas_tanah = $this->luas_tanah;
            unset($this->luas_tanah);
        }

        if(isset($this->luas_bangunan)) {
            $this->detail_objek->luas_bangunan = $this->luas_bangunan;
            unset($this->luas_bangunan);
        }

        if(isset($this->luas_tempat_usaha)) {
            $this->detail_objek->luas_tempat_usaha = $this->luas_tempat_usaha;
            unset($this->luas_tempat_usaha);
        }

        if(isset($this->status_kepemilikan)) {
            $this->detail_objek->status_kepemilikan = $this->status_kepemilikan;
            unset($this->status_kepemilikan);
        }

        if(isset($this->jam_operasional)) {
            $this->detail_objek->jam_operasional = $this->jam_operasional;
            foreach($this->detail_objek->jam_operasional as &$i) {
                $i = (object) $i;
            }
            unset($this->jam_operasional);
        }

        return true;
    }

    public static function find($user, $id)
    {
        try {
            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . "/{$id}");

            $response = json_decode($response->getBody());

            // return null if deleted
            if(!empty($response->deleted_at)) {
                return null;
            }
            $obj = new self();
            foreach($response as $key => $value) {
                $obj->{$key} = $value;
            }
            return $obj;
        } catch(HttpClientException $e) {
            if($e->getCode() === 404) {
                return null;
            }
            throw $e;
        }
    }

    public static function findByNopd($user, $nopd)
    {
        try {
            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . '?' . http_build_query(['nop' => $nopd]));

            $response = json_decode($response->getBody());

            // return null if deleted
            if(!empty($response->deleted_at)) {
                return null;
            }
            $obj = new self();
            if($response->count >= 1) {
                $response = $response->items[0];
            } else {
                return null;
            }
            foreach($response as $key => $value) {
                $obj->{$key} = $value;
            }
            return $obj;
        } catch(HttpClientException $e) {
            if($e->getCode() === 404) {
                return null;
            }
            throw $e;
        }
    }

    /**
     * Fetch items
     * @param  object  $user      The active user
     * @param  integer $pageSize  Items per page
     * @param  integer $start     Item start from index number
     * @param  array   $ordering  Ordering specifications
     * @param  array   $search    Search array with [regex] (boolean) and [value] (string)
     * @param  array   $filter    Filter data
     * @return object             Object with "count" (total number of items regardless of page) and "items" properties
     */
    public static function index($user, $pageSize = 10, $start = 0, $ordering = null, $search = null, $filter = null)
    {
        // calculate page index
        $pageIndex = floor($start / $pageSize);

        // create ordering query
        $order = 'created_at DESC'; // default
        if($ordering) {
            // TODO
        }

        // TODO Search: API doesn't support it yet

        try {
            $query = [
                'page_size' => $pageSize,
                'page_num' => $pageIndex + 1, // pageIndex starts from 1 and not 0
                'orders' => $order
            ];
            if($filter) {
                if(!empty($filter['subjek'])) {
                    $query['subjek'] = $filter['subjek'];
                }
                if(!empty($filter['status'])) {
                    $query['status'] = $filter['status'];
                }
                if(!empty($filter['jenis_pajak'])) {
                    $query['jenis'] = $filter['jenis_pajak'];
                }
                if(!empty($filter['sdate'])) {
                    $query['sdate'] = $filter['sdate'];
                }
                if(!empty($filter['edate'])) {
                    $query['edate'] = $filter['edate'];
                }
                if(!empty($filter['n_ids'])) {
                    $query['n_ids'] = $filter['n_ids'];
                }
                if(!empty($filter['ids'])) {
                    $query['ids'] = $filter['ids'];
                }
            }
            $query = http_build_query($query);

            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . '?' . $query);

            $response = json_decode($response->getBody());
            return $response;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }
    }

    public function save($user)
    {
        if(!isset($this->status)) {
            // Set default status: Unverified
            $this->status = 0;
        }

        // Check required values
        $this->checkRequired();
        // Unset null values
        $this->cleanupNulls();
        // Cut stamps
        $this->cutStamps();
        // Combine details
        $this->combineDetails();

        try {
            $fields = get_object_vars($this);
            unset($fields['_id']);
            unset($fields['__v']);

            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);

            // Create new
            if(empty($this->_id)) {
                $response = $client->request('POST', $this->endpointUrl(), [
                    'json' => $fields
                ]);
                $responseBody = json_decode($response->getBody());
                $this->_id = $responseBody->data->_id;
                Log::info("New objek pajak");
                Log::debug(print_r($responseBody, true));
            }
            // Update existing
            else if(!empty($this->_id)) {
                $response = $client->request('PUT', $this->endpointUrl() . '/' . $this->_id, [
                    'json' => $fields
                ]);
                $responseBody = json_decode($response->getBody());
                Log::info("Updated objek pajak.");
                Log::debug(print_r($responseBody, true));
            }

            return true;
        } catch(HttpServerException $e) {
            $responseBody = json_decode($e->getResponse()->getBody());
            if(isset($responseBody->meta->message)) {
                throw new \Exception($responseBody->meta->message);
            }
            throw $e;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }
    }

    /**
     * Set verif status
     * @param object  $user active user object
     * @param integer $to   new status
     */
    public function verify($user, $status = true)
    {
        if(empty($this->_id)) {
            throw new \Error("verify called on unsaved OP object.");
        }

        $status = $status ? 1 : 0;
        $this->status = $status;

        try {
            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('PUT', $this->endpointUrl() . '/' . $this->_id, [
                'json' => [
                    'status' => $this->status
                ]
            ]);
            $responseBody = json_decode($response->getBody());
            Log::info("Updated status OP.");
            Log::debug(print_r($responseBody, true));

            return true;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }

        return false;
    }
}
