<?php

namespace App;

use App\Contracts\ApiModelContract;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException as HttpClientException;
use GuzzleHttp\Exception\ConnectException as HttpConnectException;
use GuzzleHttp\Exception\ServerException as HttpServerException;
use Illuminate\Support\Facades\Log;

class KlasifikasiPajak extends ApiModel implements ApiModelContract
{
    /**
     * Returns the endpoint URL for this object model. It's a function so that it doesn't show up on the object's property list.
     * @return string The endpoint URL.
     */
    protected static function endpointUrl()
    {
        return env('API_URL', 'http://localhost:3000') . "/pajak/klasifikasi";
    }

    /**
     * Returns required fields. It's a function so that it doesn't show up on the object's property list.
     * @return array Array of string listing required field names.
     */
    protected function required()
    {
        return ['kode', 'nama'];
    }

    public static function all($user)
    {
        try {
            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . '?page_size=999999&orders=kode ASC');

            $response = json_decode($response->getBody());
            return $response;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }
    }

    /**
     * Fetch Jenis Pajak
     * @param  object  $user  The active user
     * @return array
     */
    public static function parents($user)
    {
        try {
            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . '?orders=parent_id ASC');

            $response = json_decode($response->getBody());

            foreach($response->items as $key => $i) {
                if($i->parent_id) {
                    unset($response->items[$key]);
                }
            }
            return $response;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }
    }

    /**
     * Helper function: put child classifications inside parents.
     * @param  array  $objects array of objects in some cluttered order
     * @return object          the structurized object
     */
    public static function structurize($objects)
    {
        $parents = [];
        $childs = [];
        foreach($objects as &$i) {
            if(!$i->parent_id) {
                $i->children = [];
                $parents[$i->_id] = $i;
            } else {
                $childs[] = $i;
            }
        }
        unset($objects);

        foreach($childs as &$i) {
            if(!empty($parents[$i->parent_id])) {
                $parents[$i->parent_id]->children[] = $i;
            }
        }
        unset($childs);

        usort($parents, function($a, $b) {
            return $a->kode > $b->kode;
        });

        foreach($parents as &$i) {
            usort($i->children, function($a, $b) {
                return $a->kode > $b->kode;
            });
        }
        return $parents;
    }

    public static function find($user, $id)
    {
        try {
            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . "/{$id}");

            $response = json_decode($response->getBody());

            // return null if deleted
            if(!empty($response->deleted_at)) {
                return null;
            }
            $obj = new self();
            foreach($response as $key => $value) {
                $obj->{$key} = $value;
            }
            return $obj;
        } catch(HttpClientException $e) {
            if($e->getCode() === 404) {
                return null;
            }
            throw $e;
        }
    }

    public static function findJenisPajakByKode($user, $kode)
    {
        try {
            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . '?' . http_build_query(['kode' => $kode]));

            $response = json_decode($response->getBody());
            $data = $response;
            if(count($data->items) > 0) {
                $item = $data->items[0];
                if($item->parent_id === null) {
                    return $item;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch(HttpClientException $e) {
            if($e->getCode() === 404) {
                return null;
            }
            throw $e;
        }
    }

    /**
     * Fetch items
     * @param  object  $user      The active user
     * @param  integer $pageSize  Items per page
     * @param  integer $start     Item start from index number
     * @param  array   $ordering  Ordering specifications
     * @param  array   $search    Search array with [regex] (boolean) and [value] (string)
     * @param  array   $filter    Filter data
     * @return object             Object with "count" (total number of items regardless of page) and "items" properties
     */
    public static function index($user, $pageSize = 10, $start = 0, $ordering = null, $search = null, $filter = null)
    {
        // calculate page index
        $pageIndex = floor($start / $pageSize);

        // create ordering query
        $order = 'kode ASC'; // default
        if($ordering) {
            // TODO ordering
        }

        // TODO search

        try {
            $query = [
                'page_size' => $pageSize,
                'page_num' => $pageIndex + 1, // pageIndex starts from 1 and not 0
                'orders' => $order
            ];
            // TODO filter

            $query = http_build_query($query);

            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . '?' . $query);

            $response = json_decode($response->getBody());
            return $response;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }
    }

    public function save($user)
    {
        // Check required values
        $this->checkRequired();
        // Cut stamps
        $this->cutStamps();

        try {
            $fields = get_object_vars($this);
            unset($fields['_id']);
            unset($fields['__v']);

            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);

            // Create new
            if(empty($this->_id)) {
                $response = $client->request('POST', $this->endpointUrl(), [
                    'json' => $fields
                ]);
                $responseBody = json_decode($response->getBody());
                $this->_id = $responseBody->data->_id;
                Log::info("New jenis/klasifikasi");
                Log::debug(print_r($responseBody, true));
            }
            // Update existing
            else if(!empty($this->_id)) {
                $response = $client->request('PUT', $this->endpointUrl() . '/' . $this->_id, [
                    'json' => $fields
                ]);
                $responseBody = json_decode($response->getBody());
                Log::info("Updated jenis/klasifikasi.");
                Log::debug(print_r($responseBody, true));
            }

            return true;
        } catch(HttpServerException $e) {
            $responseBody = json_decode($e->getResponse()->getBody());
            if(isset($responseBody->meta->message)) {
                throw new \Exception($responseBody->meta->message);
            }
            throw $e;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }
    }
}
