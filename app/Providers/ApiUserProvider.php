<?php

namespace App\Providers;

use Illuminate\Support\Str;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException as HttpClientException;
use GuzzleHttp\Exception\ConnectException as HttpConnectException;
use GuzzleHttp\Exception\ServerException as HttpServerException;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class ApiUserProvider implements \Illuminate\Contracts\Auth\UserProvider
{
    private function dummyUserInfo()
    {
        return (object) [
            '_id' => 'ID',
            'token' => 'token',
            'username' => 'admin',
            'password' => 'admin',
            'roles' => [ '365' ],
            'email' => 'admin@ad.min',
            'nama' => 'DUMMY ADMIN'
        ];
    }

    public function retrieveById($identifier)
    {
        if(env('DEBUG_FAKE_USER')) {
            $user = new User($this->dummyUserInfo());
            return $user;
        }

        try {
            $client = new HttpClient();
            $response = $client->request('GET', env('API_URL', 'http://localhost:3000') . '/auth/me', [
                'headers' => [
                    'Authorization' => "Bearer {$identifier}"
                ]
            ]);
            $body = json_decode($response->getBody());
            $userInfo = $body->data;
            $userInfo->token = $identifier;

            $user = new User($userInfo);
            return $user;
        } catch(HttpClientException $e) {
            return null;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        } catch(HttpServerException $e) {
            return null;
        }
    }

    public function retrieveByToken($identifier, $token)
    {
        return $this->retrieveById($identifier);
    }

    public function updateRememberToken(UserContract $user, $token)
    {
        // TODO
        return null;
    }

    /**
     * Login procedure
     */
    public function retrieveByCredentials(array $credentials)
    {
        if(env('DEBUG_FAKE_USER')) {
            $user = new User($this->dummyUserInfo());
            return $user;
        }

        if (empty($credentials) ||
           (count($credentials) === 1 &&
            array_key_exists('password', $credentials))) {
            return;
        }

        // Trim values
        foreach ($credentials as $key => $value) {
            if (Str::contains($key, 'password')) {
                continue;
            }
            $credentials[$key] = trim($value);
        }

        // Attempt login
        try {
            $client = new HttpClient();
            // Do login
            $response = $client->request('POST', env('API_URL', 'http://localhost:3000') . '/auth/login', [
                'json' => [
                    'username' => $credentials['username'],
                    'password' => $credentials['password']
                ]
            ]);
            $body = json_decode($response->getBody());
            // If successful, get details
            if($body->auth === true) {
                $token = $body->token;
                try {
                    $response = $client->request('GET', env('API_URL', 'http://localhost:3000') . '/auth/me', [
                        'headers' => [
                            'Authorization' => "Bearer {$token}"
                        ]
                    ]);
                } catch(\Exception $e) {
                    Log::error("Login pada pengambilan kredensial.");
                    Log::error($e);
                    return null;
                }
                $body = json_decode($response->getBody());

                $userInfo = $body->data;
                $userInfo->token = $token;

                $user = new User($userInfo);
                return $user;
            } else {
                return null;
            }
        } catch(HttpClientException $e) {
            return null;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }
    }

    public function validateCredentials(UserContract $user, array $credentials)
    {
        // Validate password
        try {
            $client = new HttpClient();
            // Do login
            $response = $client->request('POST', env('API_URL', 'http://localhost:3000') . '/auth/verify-password', [
                'json' => [
                    '_id' => $user->_id,
                    'password' => $credentials['password']
                ]
            ]);

            return true;
        } catch(HttpClientException $e) {
            return false;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }
    }
}
