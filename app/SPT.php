<?php

namespace App;

use App\Contracts\ApiModelContract;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException as HttpClientException;
use GuzzleHttp\Exception\ConnectException as HttpConnectException;
use GuzzleHttp\Exception\ServerException as HttpServerException;
use Illuminate\Support\Facades\Log;

class SPT extends ApiModel implements ApiModelContract
{
    /**
     * Returns the endpoint URL for this object model. It's a function so that it doesn't show up on the object's property list.
     * @return string The endpoint URL.
     */
    protected static function endpointUrl()
    {
        return env('API_URL', 'http://localhost:3000') . "/pajak/spt";
    }

    /**
     * Returns required fields. It's a function so that it doesn't show up on the object's property list
     * @return array Array of string listing required field names.
     */
    public function required()
    {
        return ['objekpajak_id', 'periode', 'status'];
    }

    public static function find($user, $id)
    {
        try {
            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . "/{$id}");

            $response = json_decode($response->getBody());

            // return null if deleted
            if(!empty($response->deleted_at)) {
                return null;
            }
            $obj = new self();
            foreach($response as $key => $value) {
                $obj->{$key} = $value;
            }
            return $obj;
        } catch(HttpClientException $e) {
            if($e->getCode() === 404) {
                return null;
            }
            throw $e;
        }
    }

    /**
     * Fetch items
     * @param  object  $user      The active user
     * @param  integer $pageSize  Items per page
     * @param  integer $start     Item start from index number
     * @param  array   $ordering  Ordering specifications
     * @param  array   $search    Search array with [regex] (boolean) and [value] (string)
     * @param  array   $filter    Filter data
     * @return object             Object with "count" (total number of items regardless of page) and "items" properties
     */
    public static function index($user, $pageSize = 10, $start = 0, $ordering = null, $search = null, $filter = null)
    {
        // calculate page index
        $pageIndex = floor($start / $pageSize);

        // create ordering query
        $order = 'periode DESC'; // default

        try {
            $query = [
                'page_size' => $pageSize,
                'page_num' => $pageIndex + 1, // pageIndex starts from 1 and not 0
                'orders' => $order
            ];
            if($filter) {
                if(!empty($filter['objek'])) {
                    $query['objek'] = $filter['objek'];
                }
                if(!empty($filter['status'])) {
                    $query['status'] = $filter['status'];
                }
                if(!empty($filter['periode'])) {
                    $query['periode'] = $filter['periode'];
                }
                if(!empty($filter['sdate'])) {
                    $query['sdate'] = $filter['sdate'];
                }
                if(!empty($filter['edate'])) {
                    $query['edate'] = $filter['edate'];
                }
                if(!empty($filter['is_skpd'])) {
                    $query['is_skpd'] = $filter['is_skpd'];
                }
            }
            $query = http_build_query($query);

            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . '?' . $query);

            $response = json_decode($response->getBody());
            return $response;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }
    }

    /**
     * Set payment status
     * @param object  $user active user object
     * @param integer $to   new status
     */
    public function setStatus($user, $to)
    {
        if(empty($this->_id)) {
            throw new \Error("setStatus called on unsaved SPT object.");
        }

        $to = $to ? 1 : 0;
        $this->status = $to;

        try {
            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('PUT', $this->endpointUrl() . '/' . $this->_id, [
                'json' => [
                    'status' => $this->status
                ]
            ]);
            $responseBody = json_decode($response->getBody());
            Log::info("Updated status SPT.");
            Log::debug(print_r($responseBody, true));

            return true;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }

        return false;
    }

    /**
     * Set payment method & user. TODO & FIXME
     * @param object  $user active user object
     * @param integer $pm   payment method
     * @param object  $wp   wajib pajak
     * @param object  $tgl  tanggal (YYYY-MM-DD format)
     */
    public function setPaymentData($user, $pm, $wp, $tanggal)
    {
        if(empty($this->_id)) {
            throw new \Error("setPaymentMethod called on unsaved SPT object.");
        }

        if(!$this->data_pembayaran) {
            $this->data_pembayaran = (object) [];
        }
        $this->data_pembayaran->cara_pembayaran = $pm;
        $this->data_pembayaran->tanggal_pembayaran = $tanggal;

        if(gettype($wp) === 'object') {
            $this->pembayar_pajak = $wp->_id;
        } elseif(gettype($wp) === 'string') {
            $this->pembayar_pajak = $wp;
        }

        // TODO generate kode bayar

        try {
            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('PUT', $this->endpointUrl() . '/' . $this->_id, [
                'json' => [
                    'data_pembayaran' => $this->data_pembayaran,
                    'pembayar_pajak' => $this->pembayar_pajak
                ]
            ]);
            $responseBody = json_decode($response->getBody());
            Log::info("Updated data pembayaran SPT.");
            Log::debug(print_r($responseBody, true));

            return true;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }

        return false;
    }

    public static function create($user, $objekPajak, $bulan, $tahun, $surat_teguran = null)
    {
        $spt = new self();
        $spt->objekpajak_id = $objekPajak->_id;
        $spt->jenispajak_id = $objekPajak->jenis_pajak->_id;
        $spt->klasifikasipajak_id = [];
        foreach($objekPajak->klasifikasi_pajak as $i) {
            $spt->klasifikasi_pajak[] = $i->_id;
        }
        $spt->subjekpajak_id = $objekPajak->subjek_pajak->_id;
        $spt->periode = (object) [
            'bulan' => $bulan,
            'tahun' => $tahun
        ];
        $spt->status = 0;

        if ($surat_teguran) {
            $spt->no_surat_teguran = $surat_teguran;
        }

        if($spt->save($user)) {
            return $spt;
        } else {
            return null;
        }
    }

    public function save($user)
    {
        if(!isset($this->status)) {
            // Set default status: Unverified
            $this->status = 0;
        }

        // Check required values
        $this->checkRequired();
        // Unset null values
        $this->cleanupNulls();
        // Cut stamps
        $this->cutStamps();

        try {
            $fields = get_object_vars($this);
            unset($fields['_id']);
            unset($fields['__v']);

            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);

            // Create new
            if(empty($this->_id)) {
                $response = $client->request('POST', $this->endpointUrl(), [
                    'json' => $fields
                ]);
                $responseBody = json_decode($response->getBody());
                $this->_id = $responseBody->data->_id;
                Log::info("New SPT");
                Log::debug(print_r($responseBody, true));
            }
            // Update existing
            else if(!empty($this->_id)) {
                $response = $client->request('PUT', $this->endpointUrl() . '/' . $this->_id, [
                    'json' => $fields
                ]);
                $responseBody = json_decode($response->getBody());
                Log::info("Updated SPT.");
                Log::debug(print_r($responseBody, true));
            }

            return true;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }
    }
}
