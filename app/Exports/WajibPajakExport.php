<?php

namespace App\Exports;

use App\WajibPajak;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class WajibPajakExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    use Exportable;

    public function __construct($params) {
        $this->params = $params;
    }

    public function collection()
    {
        $data = WajibPajak::index(...$this->params);

        return collect($data->items);
    }

    public function map($data): array
    {
        return [
            $data->_id,
            $data->nama,
            isset($data->nik) ? " " . $data->nik : "",
            isset($data->npwpd) ? " " . $data->npwpd : "",
            isset($data->npwp) ? " " . $data->npwp : "",
            $data->jenis,
            $data->email,
            isset($data->alamat) ? $data->alamat->kota : "",
            implode(" / ", [$data->nomor_hp, $data->nomor_telepon]),
            $data->created_at,
        ];
    }

    public function headings(): array
    {
        return [
            'ID',
            'Nama',
            'NIK',
            'NPWPD',
            'NPWP',
            'Jenis Wajib Pajak',
            'Email',
            'Alamat',
            'No. Contact',
            'Created At',
        ];
    }
}
