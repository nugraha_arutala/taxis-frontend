<?php

namespace App\Exports;

use App\ObjekPajak;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ObjekPajakExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    use Exportable;

    public function __construct($params) {
        $this->params = $params;
    }

    public function collection()
    {
        $data = ObjekPajak::index(...$this->params);
        
        return collect($data->items);
    }

    public function map($data): array
    {
        return [
            $data->_id,
            " " . $data->nop,
            isset($data->detail_objek->nama) ? $data->detail_objek->nama : "",
            isset($data->jenis_pajak->nama) ? $data->jenis_pajak->nama : "",
            isset($data->subjek_pajak->nama) ? $data->subjek_pajak->nama : "",
            isset($data->subjek_pajak->npwpd) ? " " . $data->subjek_pajak->npwpd : "",
            ($data->status === 0) ? "Belum diverifikasi" : "Terverifikasi",
            $data->created_at,
        ];
    }

    public function headings(): array
    {
        return [
            'ID',
            'NOP',
            'Nama',
            'Jenis Pajak',
            'Wajib Pajak',
            'NPWPD',
            'Status',
            'Created At',
        ];
    }
}