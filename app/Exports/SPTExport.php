<?php

namespace App\Exports;

use App\SPT;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SPTExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    use Exportable;

    public function __construct($params) {
        $this->params = $params;
    }

    public function collection()
    {
        $data = SPT::index(...$this->params);
        
        return collect($data->items);
    }

    public function map($data): array
    {
        return [
            $data->_id,
            isset($data->objek_pajak->nop) ? " " . $data->objek_pajak->nop : "",
            isset($data->periode->bulan) ? $data->periode->bulan . " - " . $data->periode->tahun  : "",
            isset($data->jenis_pajak->nama) ? $data->jenis_pajak->nama : "",
            isset($data->pembayar_pajak->nama) ? $data->pembayar_pajak->nama : "",
            isset($data->pembayar_pajak->npwpd) ? " " . $data->pembayar_pajak->npwpd : "",
            isset($data->jumlah_dpp) ? " " . $data->jumlah_dpp : "",
            isset($data->jumlah_pajak) ? " " . $data->jumlah_pajak : "",
            isset($data->denda_pajak) ? " " . $data->denda_pajak : "",
            isset($data->potongan_pajak) ? " " . $data->potongan_pajak : "",
            ($data->status === 0) ? "Belum dibayar" : "Lunas",
            $data->created_at,
        ];
    }

    public function headings(): array
    {
        return [
            'ID',
            'NOP',
            'Periode',
            'Jenis Pajak',
            'Pembayar Pajak',
            'NPWPD',
            'Jumlah DPP',
            'Jumlah Pajak',
            'Denda',
            'Potongan',
            'Status',
            'Created At',
        ];
    }
}
