<?php

namespace App\Exports;

use App\User;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class UsersExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    use Exportable;

    public function __construct($params) {
        $this->params = $params;
    }

    public function collection()
    {
        $user = User::index(...$this->params);

        return collect($user->items);
    }

    public function map($user): array
    {
        $roles = implode(", ", $user->roles);

        return [
            $user->_id,
            $user->nama,
            $user->username,
            $roles,
            isset($user->subjek_pajak) ? $user->subjek_pajak : "",
            $user->created_at,
        ];
    }

    public function headings(): array
    {
        return [
            'ID',
            'Nama',
            'Username',
            'Role',
            'Subjek Pajak',
            'Created At',
        ];
    }
}
