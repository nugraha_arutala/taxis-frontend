<?php

namespace App;

class ApiModel
{
    /**
     * Check whether required fields are empty. If so, throws an exception.
     */
    protected function checkRequired($except = [])
    {
        $empty = [];
        foreach($this->required() as $i) {
            if(in_array($i, $except)) { continue; }

            if(!isset($this->{$i})) {
                $empty[] = $i;
            }
        }
        if(count($empty) > 0) {
            throw new \Exception("Beberapa isian tidak terisi: " . implode(', ', $empty));
        }


        return true;
    }

    /**
     * Unsets all not-required fields from the object that are null in value.
     */
    protected function cleanupNulls()
    {
        foreach(get_object_vars($this) as $key => $i) {
            // Skip required fields
            if(in_array($key, $this->required())) { continue; }

            if($i === null) {
                unset($this->{$key});
            }
        }

        return true;
    }

    /**
     * Cut timestamps and created-bys.
     */
    protected function cutStamps()
    {
        unset($this->created_at);
        unset($this->created_by);
        unset($this->updated_at);
        unset($this->updated_by);
        unset($this->deleted_at);
        unset($this->deleted_by);
        unset($this->__v);

        return true;
    }
}
