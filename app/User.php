<?php

namespace App;

use App\Contracts\ApiModelContract;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException as HttpClientException;
use GuzzleHttp\Exception\ConnectException as HttpConnectException;
use GuzzleHttp\Exception\ServerException as HttpServerException;
use \Illuminate\Contracts\Auth\Authenticatable as Authenticatable;
use Illuminate\Support\Facades\Log;

class User extends ApiModel implements ApiModelContract, Authenticatable
{
    /**
     * Variables
     */
    public $_id;
    public $token;
    public $username;
    public $password;
    public $email;
    public $nama;
    public $roles;
    public $subjek_pajak;

    /**
     * Construct
     */
    public function __construct($data = array())
    {
        // typecast in case given array
        $data = (object) $data;

        // assign
        if(!empty($data->_id))
            $this->_id = $data->_id;

        if(!empty($data->token))
            $this->token = $data->token;

        if(!empty($data->username))
            $this->username = $data->username;

        if(!empty($data->password))
            $this->password = $data->password;

        if(!empty($data->roles))
            $this->roles = $data->roles;

        if(!empty($data->email))
            $this->email = $data->email;

        if(!empty($data->subjek_pajak))
            $this->subjek_pajak = $data->subjek_pajak;

        if(!empty($data->nama)) {
            $this->nama = $data->nama;
        } else {
            if(!empty($data->username))
                $this->nama = $data->username;
        }
    }

    /**
     * Returns the endpoint URL for this object model. It's a function so that it doesn't show up on the object's property list.
     * @return string The endpoint URL.
     */
    protected static function endpointUrl()
    {
        return env('API_URL', 'http://localhost:3000') . "/user";
    }

    /**
     * Returns required fields. It's a function so that it doesn't show up on the object's property list.
     * @return array Array of string listing required field names.
     */
    protected function required()
    {
        return ['username', 'password', 'roles', 'nama'];
    }

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return "token";
    }
    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->{$this->getAuthIdentifierName()};
    }
    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }
    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->token;
    }
    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        return false;
    }
    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'token';
    }

    /**
     * Fetch items
     * @param  object  $user      The active user
     * @param  integer $pageSize  Items per page
     * @param  integer $start     Item start from index number
     * @param  array   $ordering  Ordering specifications
     * @param  array   $search    Search array with [regex] (boolean) and [value] (string)
     *  @param  array   $filter    Filter data
     * @return object             Object with "count" (total number of items regardless of page) and "items" properties
     */
    public static function index($user, $pageSize = 10, $start = 0, $ordering = null, $search = null, $filter = null)
    {
        // calculate page index
        $pageIndex = floor($start / $pageSize);

        // create ordering query
        $order = 'created_at ASC';

        $query = [
            'page_size' => $pageSize,
            'page_num' => $pageIndex + 1, // pageIndex starts from 1 and not 0
            'orders' => $order
        ];
        if($filter) {
            if(isset($filter['roles'])) {
                $query['roles'] = $filter['roles'];
            }
            if(!empty($filter['sdate'])) {
                $query['sdate'] = $filter['sdate'];
            }
            if(!empty($filter['edate'])) {
                $query['edate'] = $filter['edate'];
            }
        }

        try {
            $query = http_build_query($query);

            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . '?' . $query);

            $response = json_decode($response->getBody());
            return $response;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }
    }

    public static function find($user, $id)
    {
        try {
            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . "/{$id}");

            $response = json_decode($response->getBody());

            // return null if deleted
            if(!empty($response->deleted_at)) {
                return null;
            }
            $obj = new self();
            foreach($response as $key => $value) {
                $obj->{$key} = $value;
            }
            return $obj;
        } catch(HttpClientException $e) {
            if($e->getCode() === 404) {
                return null;
            }
            throw $e;
        }
    }

    public function save($user)
    {
        // Check required values
        if(empty($this->_id)) {
            $this->checkRequired();
        } else {
            $this->checkRequired(['password']);
        }
        // Cut stamps
        $this->cutStamps();

        try {
            $fields = get_object_vars($this);
            unset($fields['_id']);
            unset($fields['__v']);

            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);

            // Create new
            if(empty($this->_id)) {
                $response = $client->request('POST', $this->endpointUrl(), [
                    'json' => $fields
                ]);
                $responseBody = json_decode($response->getBody());
                $this->_id = $responseBody->data->_id;
                $this->password = $responseBody->data->password;
                Log::info("New user.");
                Log::debug(print_r($responseBody, true));
            }
            // Update existing
            else if(!empty($this->_id)) {
                $response = $client->request('PUT', $this->endpointUrl() . '/' . $this->_id, [
                    'json' => $fields
                ]);
                $responseBody = json_decode($response->getBody());
                $this->password = $responseBody->data->password;
                Log::info("Updated user.");
                Log::debug(print_r($responseBody, true));
            }

            return true;
        } catch(HttpServerException $e) {
            $responseBody = json_decode($e->getResponse()->getBody());
            if(isset($responseBody->meta->message)) {
                throw new \Exception($responseBody->meta->message);
            }
            throw $e;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }
    }

    public function checkRole($check)
    {
        if($check === 'wajibPajak') {
            return (!in_array(365, $this->roles) && !in_array(2, $this->roles) && in_array(1, $this->roles));
        }
        if($check === 'admin') {
            return in_array(365, $this->roles);
        }
        if($check === 'fiskus') {
            return in_array(2, $this->roles);
        }

        return false;
    }

    public function getRoleNames()
    {
        $rolenames = [];
        foreach($this->roles as $i) {
            $rolenames[] = [
                '365' => 'Administrator',
                '1' => 'Wajib Pajak',
                '2' => 'Fiskus'
            ][$i];
        }

        return $rolenames;
    }
}
