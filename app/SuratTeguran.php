<?php

namespace App;

use App\Contracts\ApiModelContract;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException as HttpClientException;
use GuzzleHttp\Exception\ConnectException as HttpConnectException;
use GuzzleHttp\Exception\ServerException as HttpServerException;
use Illuminate\Support\Facades\Log;

use App\ObjekPajak;

class SuratTeguran extends ApiModel implements ApiModelContract
{
    /**
     * Returns the endpoint URL for this object model. It's a function so that it doesn't show up on the object's property list.
     * @return string The endpoint URL.
     */
    protected static function endpointUrl()
    {
        return env('API_URL', 'http://localhost:3000') . "/pajak/surat_teguran";
    }

    /**
     * Returns required fields. It's a function so that it doesn't show up on the object's property list
     * @return array Array of string listing required field names.
     */
    public function required()
    {
        return ['objekpajak_id', 'no_surat_teguran', 'periode'];
    }

    public static function find($user, $id)
    {
        try {
            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . "/{$id}");

            $response = json_decode($response->getBody());

            // return null if deleted
            if(!empty($response->deleted_at)) {
                return null;
            }
            $obj = new self();
            foreach($response as $key => $value) {
                $obj->{$key} = $value;
            }
            return $obj;
        } catch(HttpClientException $e) {
            if($e->getCode() === 404) {
                return null;
            }
            throw $e;
        }
    }

    /**
     * Fetch items
     * @param  object  $user      The active user
     * @param  integer $pageSize  Items per page
     * @param  integer $start     Item start from index number
     * @param  array   $ordering  Ordering specifications
     * @param  array   $search    Search array with [regex] (boolean) and [value] (string)
     * @param  array   $filter    Filter data
     * @return object             Object with "count" (total number of items regardless of page) and "items" properties
     */
    public static function index($user, $pageSize = 10, $start = 0, $ordering = null, $search = null, $filter = null)
    {
        // calculate page index
        $pageIndex = floor($start / $pageSize);

        // create ordering query
        $order = 'created_at DESC'; // default

        try {
            $query = [
                'page_size' => $pageSize,
                'page_num' => $pageIndex + 1, // pageIndex starts from 1 and not 0
                'orders' => $order
            ];
            if($filter) {
                if(!empty($filter['nopd'])) {
                    $objekPajak = ObjekPajak::find($user, $filter['nopd']);
                    if($objekPajak) {
                        $query['objek'] = $objekPajak->_id;
                    }
                }
                if(!empty($filter['objek'])) {
                    $query['objek'] = $filter['objek'];
                }
                if(!empty($filter['no_surat_teguran'])) {
                    $query['no_surat_teguran'] = $filter['no_surat_teguran'];
                }
            }
            $query = http_build_query($query);

            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);
            $response = $client->request('GET', self::endpointUrl() . '?' . $query);

            $response = json_decode($response->getBody());
            return $response;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }
    }

    public function save($user)
    {
        // Check required values
        $this->checkRequired();
        // Unset null values
        $this->cleanupNulls();
        // Cut stamps
        $this->cutStamps();

        try {
            $fields = get_object_vars($this);
            unset($fields['_id']);
            unset($fields['__v']);

            $client = new HttpClient(['headers' => ['Authorization' => "Bearer {$user->token}"]]);

            // Create new
            if(empty($this->_id)) {
                $response = $client->request('POST', $this->endpointUrl(), [
                    'json' => $fields
                ]);
                $responseBody = json_decode($response->getBody());
                $this->_id = $responseBody->data->_id;
                Log::info("New Surat Teguran");
                Log::debug(print_r($responseBody, true));
            }
            // Update existing
            else if(!empty($this->_id)) {
                $response = $client->request('PUT', $this->endpointUrl() . '/' . $this->_id, [
                    'json' => $fields
                ]);
                $responseBody = json_decode($response->getBody());
                Log::info("Updated Surat Teguran.");
                Log::debug(print_r($responseBody, true));
            }

            return true;
        } catch(HttpConnectException $e) {
            Log::error("Tidak dapat terhubung dengan server API.");
            Log::error($e);
            abort(500, 'Internal Server Error');
        }
    }
}
