<?php

namespace App\Contracts;

interface ApiModelContract
{
    // Index objects
    public static function index($user);
    // Save object
    public function save($user);
}
