<div style="display: flex; flex-wrap: nowrap; align-items: center">
    <div style="flex-basis: 30%; text-align: center">
        <img src="{{ asset('img/logo_mura.png') }}" style="height: 90px; width: auto">
    </div>
    <div style="flex-basis: 70%; text-align: center">
        <h1 style="text-transform: uppercase; font-weight: bold; font-size: 1.2em;">Pemerintah Kabupaten Musi Rawas</h1>
        <h1 style="text-transform: uppercase; font-weight: bold; font-size: 1.2em; margin-bottom: 0">Dinas Pelayanan Pajak</h1>
    </div>
</div>
