<?php // SKPD ?>

@extends('printableBase')

@php
function terbilang($x) {
    $angka = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"];

    if ($x < 12)
    return " " . $angka[$x];
    elseif ($x < 20)
    return terbilang($x - 10) . " belas";
    elseif ($x < 100)
    return terbilang($x / 10) . " puluh" . terbilang($x % 10);
    elseif ($x < 200)
    return "seratus" . terbilang($x - 100);
    elseif ($x < 1000)
    return terbilang($x / 100) . " ratus" . terbilang($x % 100);
    elseif ($x < 2000)
    return "seribu" . terbilang($x - 1000);
    elseif ($x < 1000000)
    return terbilang($x / 1000) . " ribu" . terbilang($x % 1000);
    elseif ($x < 1000000000)
    return terbilang($x / 1000000) . " juta" . terbilang($x % 1000000);
}
@endphp

@section('kop-kanan')
<div style="display: flex; text-align: center; align-items: center; height: 100%">
    <div style="width: 100%">
        <h1 style="text-transform: uppercase; font-weight: bold; font-size: 1.2em;">Surat Ketetapan Pajak Daerah (SKPD)</h1>
        <h1 style="text-transform: uppercase; font-weight: bold; font-size: 1.2em;">Tahun {{ date('Y') }}</h1>
    </div>
</div>
@endsection

@section('letter-body')
<tr>
    <td colspan="2">
        <div style="padding: 2em">
            <table class="noborder" style="width: 100%">
                <tr>
                    <td>1. NPWPD</td>
                    <td>: {{ $wajibPajak->npwpd ?? '?' }}</td>
                </tr>
                <tr>
                    <td>2. Nama Wajib Pajak</td>
                    <td>: {{ $wajibPajak->nama ?? '?' }}</td>
                </tr>
                <tr>
                    <td>3. Nama Usaha</td>
                    <td>: {{ $objekPajak->detail_objek->nama ?? '?' }}</td>
                </tr>
                <tr>
                    <td>4. Alamat Usaha</td>
                    <td><p>:
                        @if(!empty($objekPajak->detail_objek->alamat->jalan))
                            {{ $objekPajak->detail_objek->alamat->jalan }}<br>&nbsp;
                        @endif

                        @if(!empty($objekPajak->detail_objek->alamat->rt))
                        RT: {{ $objekPajak->detail_objek->alamat->rt }}
                        @endif
                        @if(!empty($objekPajak->detail_objek->alamat->rw))
                        RW: {{ $objekPajak->detail_objek->alamat->rw }}
                        @endif
                        @if(!empty($objekPajak->detail_objek->alamat->kode_pos))
                        Kode Pos: {{ $objekPajak->detail_objek->alamat->kode_pos }}
                        @endif

                        @if(!empty($objekPajak->detail_objek->alamat->rt) || !empty($objekPajak->detail_objek->alamat->rw) || !empty($objekPajak->detail_objek->alamat->kode_pos))
                        <br>&nbsp;
                        @endif

                        @if(!empty($objekPajak->detail_objek->alamat->provinsi))
                        Provinsi: {{ $objekPajak->detail_objek->alamat->provinsi }}
                        @endif
                        @if(!empty($objekPajak->detail_objek->alamat->kota))
                        Kabupaten/Kota: {{ $objekPajak->detail_objek->alamat->kota }}
                        @endif

                        @if(!empty($objekPajak->detail_objek->alamat->provinsi) || !empty($objekPajak->detail_objek->alamat->kota))
                        <br>&nbsp;
                        @endif


                        @if(!empty($objekPajak->detail_objek->alamat->kecamatan))
                        Kecamatan: {{ $objekPajak->detail_objek->alamat->kecamatan }}
                        @endif
                        @if(!empty($objekPajak->detail_objek->alamat->kelurahan))
                        Kelurahan: {{ $objekPajak->detail_objek->alamat->kelurahan }}
                        @endif
                    </p></td>
                </tr>
                <tr>
                    <td>5. Nomor Objek Pajak</td>
                    <td>: {{ $objekPajak->nop }}</td>
                </tr>
                <tr>
                    <td colspan="2">6. Setoran</td>
                </tr>
            </table>
            <?php $bulan = [1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juni', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember'] ?>
            <table class="bordered" style="width: 100%; margin-bottom: 1em">
                <thead style="font-weight: bold">
                    <tr>
                        <td style="text-align: center">No</td>
                        <td>Jenis Pajak</td>
                        <td style="text-align: center">Uraian</td>
                        <td style="text-align: center">Jumlah</td>
                    </tr>
                </thead>
                @if ($objekPajak->jenis_pajak->kode === '4.1.1.02')
                <tr>
                    <td style="text-align: center">1</td>
                    <td><b>Pajak Restoran</b></td>
                    <td style="text-align: center">Setoran Pajak {{ $bulan[(int) $spt->periode->bulan] }} {{ $spt->periode->tahun }}</td>
                    <td style="text-align: center">{{ number_format($spt->jumlah_pajak, 2, ',', '.') }}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="border: 0 solid white">
                    <td colspan="2" style="border: 0"></td>
                    <td style="text-align: center"><b>TOTAL</b></td>
                    <td style="text-align: center"><b>{{ number_format($spt->jumlah_pajak, 2, ',', '.') }}</b></td>
                </tr>
                @endif
            </table>
            <div style="text-align: center; font-weight: bold; margin-bottom: 2em">Terbilang: <span style="text-transform: capitalize">{{ terbilang($spt->jumlah_pajak) }}</span> Rupiah</div>

            <table class="bordered" style="width: 100%; margin-bottom: 0.5em">
                <tr>
                    <td style="width: 33%; padding: 1em; text-align: center">
                        Ruang untuk teraan mesin kas register,
                        <br><br>
                        cap
                    </td>
                    <td style="width: 33%; padding: 1em">
                        Diterima oleh petugas,<br>
                        tanggal ______________
                        <br><br>
                        ___________________________<br>
                        NIP _______________________
                    </td>
                    <td style="width: 33%; padding: 1em; text-align: center">
                        Musi Rawas, ______________<br>
                        Penyetor
                        <br><br>
                        __________________________<br>
                        Nama/Cap/Stempel
                    </td>
                </tr>
            </table>
            <div style="text-align: right">Tanggal pembayaran: {{ !empty($spt->data_pembayaran->tanggal_pembayaran) ? date('j/n/Y', strtotime($spt->data_pembayaran->tanggal_pembayaran)) : '__________________________' }}</div>
        </div>
    </td>
</tr>
<tr>
    <td colspan="2" style="text-align: right; padding: 1em 2em" class="barcode">
        {!! DNS1D::getBarcodeHTML($spt->no_spt, "C93") !!}
    </td>
</tr>
@endsection
