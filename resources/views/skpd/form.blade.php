@extends('skeleton')

@section('title', 'Input SKPD ' . ($objekPajak->detail_objek->nama))
@section('wrapper-title', 'Formulir Input SKPD')

@section('content')
    <div class="container-fluid">
        @if ($errors->any())
        <div class="card red darken-4">
            <div class="card-content white-text">
                <span class="card-title">Submit Gagal</span>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title activator">{{ $objekPajak->jenis_pajak->nama }}</h5>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Nomor Objek Pajak Daerah</div>
                            <div class="col s12 m6 l8"><p>{{ $objekPajak->nop }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Nama Usaha</div>
                            <div class="col s12 m6 l8"><p>{{ $objekPajak->detail_objek->nama ?? '-' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Alamat</div>
                            <div class="col s12 m6 l8">
                                <p>
                                    @if(!empty($objekPajak->detail_objek->alamat->jalan))
                                        {{ $objekPajak->detail_objek->alamat->jalan }}<br>
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->rt))
                                    RT: {{ $objekPajak->detail_objek->alamat->rt }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->rw))
                                    RW: {{ $objekPajak->detail_objek->alamat->rw }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->kode_pos))
                                    Kode Pos: {{ $objekPajak->detail_objek->alamat->kode_pos }}
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->rt) || !empty($objekPajak->detail_objek->alamat->rw) || !empty($objekPajak->detail_objek->alamat->kode_pos))
                                    <br>
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->provinsi))
                                    Provinsi: {{ $objekPajak->detail_objek->alamat->provinsi }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->kota))
                                    Kabupaten/Kota: {{ $objekPajak->detail_objek->alamat->kota }}
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->provinsi) || !empty($objekPajak->detail_objek->alamat->kota))
                                    <br>
                                    @endif


                                    @if(!empty($objekPajak->detail_objek->alamat->kecamatan))
                                    Kecamatan: {{ $objekPajak->detail_objek->alamat->kecamatan }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->kelurahan))
                                    Kelurahan: {{ $objekPajak->detail_objek->alamat->kelurahan }}
                                    @endif
                                </p>
                            </div>
                        </div>
                        <hr>
                        @php
                            $bulan = [1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juni', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember']
                        @endphp
                        <p class="center-align">Masa Pajak <b>{{ $bulan[(int) $spt->periode->bulan] }} {{ $spt->periode->tahun }}</b></p>

                        <form method="POST" action="{{ url('skpd/'.$objekPajak->_id.'/form/'.$spt->_id) }}">
                            @csrf
                            <input type="hidden" name="_id" value="{{ $spt->_id }}">
                            <div class="row">
                                <div class="col s12">
                                    <ul class="tabs" style="margin-bottom: 1.5rem">
                                        @foreach($spt->klasifikasi_pajak as $klasifikasiIndex => $i)
                                            <input type="hidden" name="klasifikasi[{{ $klasifikasiIndex }}]" value="{{ $i->_id }}">
                                            <li class="tab col s3"><a href="javascript:;" class="tab-toggle" data-id="{{ $klasifikasiIndex }}">{{ $i->nama }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @foreach($spt->klasifikasi_pajak as $klasifikasiIndex => $i)
                                <div class="tab-content" data-id="{{ $klasifikasiIndex }}">
                                    <div class="row row-total row-total">
                                        <div class="col s12 m2">
                                            <p class="center-align" style="padding-top: 1em">Total Tagihan</p>
                                        </div>
                                        
                                        <div class="col s12 m10">
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <input id="data_tagihan_{{ $klasifikasiIndex }}_total_tagihan" 
                                                        data-item="tagihan" data-klasifikasi="{{ $klasifikasiIndex }}" data-day="total"
                                                        class="numeric input-{{ $klasifikasiIndex }}-total total"
                                                        type="text" name="data_tagihan[{{ $klasifikasiIndex }}][total][tagihan]"
                                                        value="{{ old('data_tagihan') ? old("data_tagihan.{$klasifikasiIndex}.total.tagihan") :
                                                            ($spt->data_tagihan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->tagihan ?? '')
                                                        }}"
                                                    >
                                                    <label for="data_tagihan_{{ $klasifikasiIndex }}_total_tagihan">Jumlah Tunggakan Tagihan</label>
                                                </div>
                                                <div class="input-field col s12 m6">
                                                    <input id="data_tagihan_{{ $klasifikasiIndex }}_total_denda"
                                                        data-item="denda" data-klasifikasi="{{ $klasifikasiIndex }}" data-day="total"
                                                        class="numeric input-{{ $klasifikasiIndex }}-total total"
                                                        type="text" name="data_tagihan[{{ $klasifikasiIndex }}][total][denda]"
                                                        value="{{ old('data_tagihan') ? old("data_tagihan.{$klasifikasiIndex}.total.denda") :
                                                            ($spt->data_tagihan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->denda ?? '')
                                                        }}"
                                                    >
                                                    <label for="data_tagihan_{{ $klasifikasiIndex }}_total_denda">Jumlah Denda</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="row">
                                <div class="input-field col s12 l2">
                                    <a class="btn red waves-effect waves-light" style="width:100%" href="{{ url('/skpd/' . $objekPajak->_id) }}"><i class="material-icons left">chevron_left</i>Kembali</a>
                                </div>
                                <div class="input-field col s12 l2 push-l8">
                                    <button class="btn green waves-effect waves-light" style="width:100%" type="submit"><i class="material-icons left">save</i>Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script src="{{ asset('assets/libs/cleavejs/cleave.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('input.numeric').toArray().forEach(function (field) {
            $(field).data('cleave', new Cleave(field, {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralDecimalMark: ',',
                delimiter: '.'
            }));
        });
    });
</script>
@endsection
