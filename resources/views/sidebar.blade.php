<ul id="slide-out" class="sidenav">
    <li>
        <ul class="collapsible">
            <li>
                <a href="{{ url('/dashboard') }}" class="collapsible-header"><i class="material-icons">dashboard</i><span class="hide-menu"> Dashboard</span></a>
            </li>
            <li>
                <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">pageview</i><span class="hide-menu"> Lihat Data</span></a>
                <div class="collapsible-body">
                    <ul>
                        @if(Auth::user()->checkRole('admin'))
                        <li><a href="{{ url('/lihat-data/user') }}"><i class="material-icons">assignment_ind</i><span class="hide-menu">User</span></a></li>
                        @endif
                        @if(Auth::user()->checkRole('admin') || Auth::user()->checkRole('fiskus'))
                        <li><a href="{{ url('/lihat-data/wajib-pajak') }}"><i class="material-icons">person_pin</i><span class="hide-menu">Wajib Pajak</span></a></li>
                        @endif
                        <li><a href="{{ url('/lihat-data/objek-pajak') }}"><i class="material-icons">location_city</i><span class="hide-menu">Objek Pajak</span></a></li>
                    </ul>
                </div>
            </li>
            <li>
                <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">edit</i><span class="hide-menu"> Pendaftaran </span></a>
                <div class="collapsible-body">
                    <ul>
                        @if(Auth::user()->checkRole('admin'))
                        <li><a href="{{ url('/pendaftaran/user') }}"><i class="material-icons">assignment_ind</i><span class="hide-menu">User</span></a></li>
                        @endif
                        @if(Auth::user()->checkRole('admin') || Auth::user()->checkRole('fiskus'))
                        <li><a href="{{ url('/pendaftaran/wajib-pajak') }}"><i class="material-icons">person_pin</i><span class="hide-menu">Wajib Pajak</span></a></li>
                        @endif
                        <li><a href="{{ url('/pendaftaran/objek-pajak') }}"><i class="material-icons">location_city</i><span class="hide-menu">Objek Pajak</span></a></li>
                    </ul>
                </div>
            </li>
            <li>
                <a href="{{ url('/pelaporan') }}" class="collapsible-header"><i class="material-icons">collections_bookmark</i><span class="hide-menu"> SPTPD</span></a>
            </li>
            <li>
                <a href="{{ url('/pembayaran') }}" class="collapsible-header"><i class="material-icons">monetization_on</i><span class="hide-menu"> Pembayaran</span></a>
            </li>
            @if(Auth::user()->checkRole('admin') || Auth::user()->checkRole('fiskus'))
            <li>
                <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">playlist_add_check</i><span class="hide-menu"> Pemeriksaan</span></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="{{ url('/surat-teguran') }}"><i class="material-icons">insert_drive_file</i><span class="hide-menu">Surat Teguran</span></a></li>
                        <li><a href="{{ url('/skpd') }}"><i class="material-icons">warning</i><span class="hide-menu"> SKPD</span></a></li>
                    </ul>
                </div>
            </li>
            <li>
                <a href="{{ url('/reporting') }}" class="collapsible-header"><i class="material-icons">print</i><span class="hide-menu"> Reporting</span></a>
            </li>
            @endif
            @if(Auth::user()->checkRole('admin'))
            <li>
                <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">settings</i><span class="hide-menu"> Master</span></a>
                <div class="collapsible-body">
                    <ul>

                        <li><a href="{{ url('/master-data/jenis-klasifikasi') }}"><i class="material-icons">call_split</i><span class="hide-menu">Jenis &amp; Klasifikasi</span></a></li>
                    </ul>
                </div>
            </li>
            @endif
        </ul>
    </li>
</ul>
