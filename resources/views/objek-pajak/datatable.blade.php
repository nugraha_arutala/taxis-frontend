<?php // NOTE File halaman ini digunakan untuk datatable Lihat Data/Objek Pajak dan Pembayaran  ?>
<?php $page = $page ?? "objek pajak" ?>

@extends('skeleton')

@if($page === "objek pajak")
    @section('title', 'Daftar Objek Pajak')
@elseif($page === "pembayaran")
    @section('title', 'Pembayaran')
@endif

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                @if (Session::has('message'))
                    @if(Session::get('message') === 'save-successful')
                    <div class="card green darken-2">
                        <div class="card-content white-text">
                            Data baru berhasil disimpan.
                        </div>
                    </div>
                    @endif
                @endif
                <div class="card">
                    <div class="card-content">
                        @if($page === "objek pajak")
                            @if(Auth::user()->subjek_pajak)
                            <div class="row">
                                <?php $wajibPajak = Auth::user()->subjek_pajak ?>
                                <div class="col s12 m6 l3 font-bold m-right-align">NPWPD</div>
                                <div class="col s12 m6 l9"><p>{{ $wajibPajak->npwpd ?? '-' }}</p></div>
                                <div class="col s12 m6 l3 font-bold m-right-align">Nama Wajib Pajak</div>
                                <div class="col s12 m6 l9"><p>{{ $wajibPajak->nama ?? '-' }}</p></div>
                                <div class="col s12 m6 l3 font-bold m-right-align">Alamat</div>
                                <div class="col s12 m8 l9">
                                    <p>
                                        @if(!empty($wajibPajak->alamat->jalan))
                                            {{ $wajibPajak->alamat->jalan }}<br>
                                        @endif

                                        @if(!empty($wajibPajak->alamat->rt))
                                        RT: {{ $wajibPajak->alamat->rt }}
                                        @endif
                                        @if(!empty($wajibPajak->alamat->rw))
                                        RW: {{ $wajibPajak->alamat->rw }}
                                        @endif
                                        @if(!empty($wajibPajak->alamat->kode_pos))
                                        Kode Pos: {{ $wajibPajak->alamat->kode_pos }}
                                        @endif

                                        @if(!empty($wajibPajak->alamat->rt) || !empty($wajibPajak->alamat->rw) || !empty($wajibPajak->alamat->kode_pos))
                                        <br>
                                        @endif

                                        @if(!empty($wajibPajak->alamat->provinsi))
                                        Provinsi: {{ $wajibPajak->alamat->provinsi }}
                                        @endif
                                        @if(!empty($wajibPajak->alamat->kota))
                                        Kabupaten/Kota: {{ $wajibPajak->alamat->kota }}
                                        @endif

                                        @if(!empty($wajibPajak->alamat->provinsi) || !empty($wajibPajak->alamat->kota))
                                        <br>
                                        @endif


                                        @if(!empty($wajibPajak->alamat->kecamatan))
                                        Kecamatan: {{ $wajibPajak->alamat->kecamatan }}
                                        @endif
                                        @if(!empty($wajibPajak->alamat->kelurahan))
                                        Kelurahan: {{ $wajibPajak->alamat->kelurahan }}
                                        @endif
                                    </p>
                                </div>
                            </div>
                            @endif
                            <div class="right-align">
                                <a class="btn-floating btn-large waves-effect waves-light green" href="{{ url('/pendaftaran/objek-pajak') }}"><i class="material-icons">add</i></a>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col s12">
                                <div class="row">
                                    <div class="input-field col s8">
                                        <select id="search_jenisPajak">
                                            <option value="">Tampilkan semua</option>
                                            @foreach($jenisPajak as $i)
                                                <option value="{{ $i->_id }}" data-kode="{{ $i->kode }}">{{ $i->nama }}</option>
                                            @endforeach
                                        </select>
                                        <label for="search_jenisPajak">Tampilkan sesuai Jenis Pajak</label>
                                    </div>
                                    <div class="input-field col s4">
                                        <select id="search_status">
                                            <option value="">Tampilkan semua</option>
                                            <option value="0">Belum diverifikasi</option>
                                            <option value="1">Terverifikasi</option>
                                        </select>
                                        <label for="search_status">Status</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table id="datatable" class="responsive-table display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NOPD</th>
                                    <th>Nama Usaha</th>
                                    <th>Alamat</th>
                                    <th>Jenis Pajak</th>
                                    @if ($page === "objek pajak")
                                        <th>Status</th>
                                    @endif
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="99" style="text-align: center">Loading...</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hide">
        <!-- action button column for datatable -->
        @if ($page === "objek pajak")
            <div id="action-button-column-sample">
                <a class="waves-effect waves-light btn-small view" href="{{ url('/lihat-data/objek-pajak/[id]') }}"><i class="material-icons">search</i></a>
                <a class="waves-effect waves-light btn-small edit" href="{{ url('/lihat-data/objek-pajak/[id]/edit') }}"><i class="material-icons">edit</i></a>
            </div>
        @elseif ($page === "pembayaran")
            <div id="action-button-column-sample">
                <a class="waves-effect waves-light btn-small view" href="{{ url('/pembayaran/[id]') }}"><i class="material-icons">search</i></a>
            </div>
        @endif
    </div>
@endsection

@section('stylesheet')
    <link href="{{ asset('css/datatable-fix.css') }}" rel="stylesheet">
@endsection
@section('javascript')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script>
        // Element init
        $(function () {
            $("select").not(".disabled").formSelect();
        });

        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            lengthChange: false,
            ordering: false,
            order: [],
            searching: true,
            ajax: {
                url: '{{ url('/lihat-data/objek-pajak/datatable') }}',
                type: 'GET'
            },
            columns: [
                {
                    data: function(row, type, set, meta) {
                        var pginfo = $(meta.settings.nTable).DataTable().page.info();
                        return (pginfo.page * pginfo.length) + (meta.row + 1);
                    },
                    className: 'center-align'
                },
                {
                    data: function(row, type, set, meta) {
                        return row.nop || '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.detail_objek.nama || '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.detail_objek.alamat.kota || '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        if(!row.jenis_pajak) { return '???'; }
                        return row.jenis_pajak.nama || '???';
                    }
                },
                @if ($page === "objek pajak")
                    {
                        data: function(row, type, set, meta) {
                            return row.status === 1 ? 'Terverifikasi' : 'Belum diverifikasi';
                        }
                    },
                @endif
                {
                    data: function(row, type, set, meta) {
                        var div = $('#action-button-column-sample').clone();
                        var buttons = div.find('a');
                        for(i of buttons) {
                            $(i).prop('href', $(i).prop('href').replace('[id]', row._id));
                        }
                        return div.html();
                    },
                    orderable: false
                }
            ]
        });

        // manual search
        $('#datatable_filter').remove();

        $('#search_status').val('');
        $('#search_jenisPajak').val('');

        $('#search_status').on('change', function (e) {
            $('#datatable').DataTable().columns(5).search($('#search_status').val()).draw();
        })
        $('#search_jenisPajak').on('change', function (e) {
            $('#datatable').DataTable().columns(4).search($('#search_jenisPajak').val()).draw();
        })
    </script>
@endsection
