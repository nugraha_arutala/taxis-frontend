@extends('skeleton')

@section('title', 'Data Objek Pajak')

@section('content')
    <div class="container-fluid">
        @if ($errors->any())
        <div class="card red darken-4">
            <div class="card-content white-text">
                <span class="card-title">Submit Gagal</span>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        </div>
        @endif
        <form method="POST" action="{{ url('/pendaftaran/objek-pajak') }}">
            @csrf
            @if(!empty($objekPajak))
                <input type="hidden" name="_id" value="{{ $objekPajak->_id }}">
            @endif
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <h5 class="card-title activator">A. Data Wajib Pajak</h5>
                            @if(empty($fixedWajibPajak))
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="npwpd" type="text" name="npwpd" value="{{ old('npwpd') ?? $wajibPajak->npwpd ?? '' }}" required>
                                        <label for="npwpd">NPWPD</label>
                                        <span class="helper-text hide red-text" id="npwpd-not-found-helper">NPWPD tidak ditemukan. Apakah <a href="{{ url('pendaftaran/wajib-pajak') }}" target="_blank">data wajib pajak baru</a>?</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="nama-wp" type="text" value="{{ $wajibPajak->nama ?? '' }}" disabled>
                                        <label id="nama-wp-label">Nama Lengkap</label>
                                        <span class="helper-text">Nama akan terisi otomatis jika NPWPD yang dimasukkan benar. Jika tidak ditemukan, <a href="{{ url('pendaftaran/wajib-pajak') }}" target="_blank">klik di sini</a> untuk memasukkan data wajib pajak baru.</span>
                                    </div>
                                </div>
                            @else
                                <input type="hidden" name="npwpd" value="{{ $wajibPajak->npwpd ?? '?' }}">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="npwpd" type="text" name="npwpd" value="{{ $wajibPajak->npwpd ?? '' }}" disabled>
                                        <label for="npwpd">NPWPD</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="nama-wp" type="text" value="{{ $wajibPajak->nama ?? '' }}" disabled>
                                        <label id="nama-wp-label">Nama Lengkap</label>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-content">
                            <h5 class="card-title activator">B. Jenis Objek Pajak</h5>
                            <div class="row">
                                <div class="input-field col s12">
                                    <select name="jenis_pajak" class="select2" id="jenis">
                                        @foreach($klasifikasiPajak as $i)
                                            <option {{ !empty($objekPajak->jenis_pajak->_id) ? ($objekPajak->jenis_pajak->_id == $i->_id ? 'selected' : '') : (old('jenis_pajak') ? (old('jenis_pajak') == $i->_id ? 'selected': '') : '') }} value="{{ $i->_id }}" data-kode="{{ $i->kode }}">{{ $i->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-content">
                            <h5 class="card-title activator">C. Detail Objek Pajak</h5>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="nama" type="text" name="nama" value="{{ old('nama') ?? $objekPajak->detail_objek->nama ?? '' }}">
                                    <label for="nama">Nama Usaha</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <textarea id="jalan" class="materialize-textarea" name="jalan" rows="4">{{ old('jalan') ?? $objekPajak->detail_objek->alamat->jalan ?? '' }}</textarea>
                                    <label for="jalan">Alamat</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m4">
                                    <input id="rt" type="text" name="rt" value="{{ old('rt') ?? $objekPajak->detail_objek->alamat->rt ?? '' }}">
                                    <label for="rt">RT</label>
                                </div>
                                <div class="input-field col s12 m4">
                                    <input id="rw" type="text" name="rw" value="{{ old('rw') ?? $objekPajak->detail_objek->alamat->rw ?? '' }}">
                                    <label for="rw">RW</label>
                                </div>
                                <div class="input-field col s12 m4">
                                    <input id="kodePos" type="text" name="kode_pos" value="{{ old('kode_pos') ?? $objekPajak->detail_objek->alamat->kode_pos ?? '' }}">
                                    <label for="kodePos">Kode Pos</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="provinsi" type="text" name="provinsi" value="{{ old('provinsi') ?? $objekPajak->detail_objek->alamat->provinsi ?? '' }}">
                                    <label for="provinsi">Provinsi</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="kota" type="text" name="kota" value="{{ old('kota') ?? $objekPajak->detail_objek->alamat->kota ?? '' }}">
                                    <label for="kota">Kota / Kabupaten</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="kecamatan" type="text" name="kecamatan" value="{{ old('kecamatan') ?? $objekPajak->detail_objek->alamat->kecamatan ?? '' }}">
                                    <label for="kecamatan">Kecamatan</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="kelurahan" type="text" name="kelurahan" value="{{ old('kelurahan') ?? $objekPajak->detail_objek->alamat->kelurahan ?? '' }}">
                                    <label for="kelurahan">Kelurahan</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <input id="nomorTelepon" type="text" name="nomor_telepon" value="{{ old('nomor_telepon') ?? $objekPajak->detail_objek->nomor_telepon ?? '' }}">
                                    <label for="nomorTelepon">No. Telepon</label>
                                </div>
                                <div class="input-field col s12 m6">
                                    <input id="nomorHp" type="text" name="nomor_hp" value="{{ old('nomor_hp') ?? $objekPajak->detail_objek->nomor_hp ?? '' }}">
                                    <label for="nomorHp">No. Handphone</label>
                                </div>
                            </div>
                            <h5 class="card-title activator">Data Usaha</h5>
                            <div class="row">
                                <div class="input-field col s12">
                                    <div class="row">
                                        <div class="col s12 m3">
                                            <p>Status Usaha</p>
                                        </div>
                                        <div class="col s12 m9">
                                            <div class="row">
                                                <div class="col s12 m3">
                                                    <label>
                                                        <input name="status_usaha" type="radio" value="induk" {{ old('status_usaha') === 'induk' ? 'checked' : (($objekPajak->detail_objek->status_usaha ?? null) === 'induk' ? 'checked' : '') }} />
                                                        <span>Induk</span>
                                                    </label>
                                                </div>
                                                <div class="col s12 m3">
                                                    <label>
                                                        <input name="status_usaha" type="radio" value="cabang" {{ old('status_usaha') === 'cabang' ? 'checked' : (($objekPajak->detail_objek->status_usaha ?? null) === 'cabang' ? 'checked' : '') }} />
                                                        <span>Cabang</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                                /**
                                 * Pajak Restoran
                                 */
                            ?>
                            <div class="detail-container" id="detail-restoran" data-jenis="4.1.1.02">
                                <div class="row">
                                    <div class="col s12 m3">
                                        <p>Klasifikasi Usaha</p>
                                    </div>
                                    <div class="input-field col s12 m9">
                                        <div class="row">
                                        <?php foreach($klasifikasiPajak as $p) { if($p->kode !== "4.1.1.02") { continue; } foreach($p->children as $i) { ?>
                                            <div class="col s12 m6">
                                                <p>
                                                    <label>
                                                        @php
                                                            if(!empty($objekPajak)) {
                                                                $opKlas = $objekPajak->klasifikasi_pajak;
                                                                foreach($opKlas as &$klasif) {
                                                                    if(gettype($klasif) === 'object') {
                                                                        $klasif = $klasif->_id;
                                                                    }
                                                                }
                                                            } else {
                                                                $opKlas = [];
                                                            }
                                                        @endphp
                                                        <input name="klasifikasi_pajak[]" type="checkbox" value="{{ $i->_id }}" {{ in_array($i->_id, (old('klasifikasi_pajak') ?? [])) ? 'checked' : (in_array($i->_id, ($opKlas ?? [])) ? 'checked' : '') }} />
                                                        <span>{{ $i->nama }}</span>
                                                    </label>
                                                </p>
                                            </div>
                                        <?php } } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <div class="row">
                                        <div class="col s12 m3">
                                            <p>Jenis Makanan Utama</p>
                                        </div>
                                        <div class="input-field col s12 m9">
                                            <p>
                                                <label>
                                                    <input name="jenis_masakan_utama" type="radio" value="masakan_indonesia" {{ old('jenis_masakan_utama') === "masakan_indonesia" ? 'checked' : (($objekPajak->detail_objek->jenis_masakan_utama ?? null) === "masakan_indonesia" ? 'checked' : '') }} />
                                                    <span>Masakan Indonesia</span>
                                                </label>
                                            </p>
                                            <p>
                                                <label>
                                                    <input name="jenis_masakan_utama" type="radio" value="cepat_saji" {{ old('jenis_masakan_utama') === "cepat_saji" ? 'checked' : (($objekPajak->detail_objek->jenis_masakan_utama ?? null) === "cepat_saji" ? 'checked' : '') }} />
                                                    <span>Cepat Saji (Fast food)</span>
                                                </label>
                                            </p>
                                            <p>
                                                <label>
                                                    <input name="jenis_masakan_utama" type="radio" value="masakan_laut" {{ old('jenis_masakan_utama') === "masakan_laut" ? 'checked' : (($objekPajak->detail_objek->jenis_masakan_utama ?? null) === "masakan_laut" ? 'checked' : '') }} />
                                                    <span>Masakan Laut (Seafood)</span>
                                                </label>
                                            </p>
                                            <p>
                                                <label>
                                                    <input name="jenis_masakan_utama" type="radio" value="masakan_padang" {{ old('jenis_masakan_utama') === "masakan_padang" ? 'checked' : (($objekPajak->detail_objek->jenis_masakan_utama ?? null) === "masakan_padang" ? 'checked' : '') }} />
                                                    <span>Masakan Padang</span>
                                                </label>
                                            </p>
                                            <p>
                                                <label>
                                                    <input name="jenis_masakan_utama" type="radio" value="masakan_barat" {{ old('jenis_masakan_utama') === "masakan_barat" ? 'checked' : (($objekPajak->detail_objek->jenis_masakan_utama ?? null) === "masakan_barat" ? 'checked' : '') }} />
                                                    <span>Masakan Eropa / Amerika</span>
                                                </label>
                                            </p>
                                            <p>
                                                <label>
                                                    <input name="jenis_masakan_utama" type="radio" value="masakan_timur" {{ old('jenis_masakan_utama') === "masakan_timur" ? 'checked' : (($objekPajak->detail_objek->jenis_masakan_utama ?? null) === "masakan_timur" ? 'checked' : '') }} />
                                                    <span>Masakan Asia / Chinese / Japanese / Korean</span>
                                                </label>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                            /**
                             * Pajak MBLB
                             */
                        ?>
                        <div class="detail-container" id="detail-mblb" data-jenis="4.1.1.11">
                            <div class="row">
                                <div class="col s12 m3">
                                    <p>Jenis Bahan Mineral</p>
                                </div>
                                <div class="input-field col s12 m9">
                                    <div class="row">
                                    <?php foreach($klasifikasiPajak as $p) { if($p->kode !== "4.1.1.11") { continue; } foreach($p->children as $i) { ?>
                                        <div class="col s12 m6 l4">
                                            <p>
                                                <label>
                                                    @php
                                                        if(!empty($objekPajak)) {
                                                            $opKlas = $objekPajak->klasifikasi_pajak;
                                                            foreach($opKlas as &$klasif) {
                                                                if(gettype($klasif) === 'object') {
                                                                    $klasif = $klasif->_id;
                                                                }
                                                            }
                                                        } else {
                                                            $opKlas = [];
                                                        }
                                                    @endphp
                                                    <input name="klasifikasi_pajak[]" type="checkbox" value="{{ $i->_id }}" {{ in_array($i->_id, (old('klasifikasi_pajak') ?? [])) ? 'checked' : (in_array($i->_id, ($opKlas ?? [])) ? 'checked' : '') }} />
                                                    <span>{{ $i->nama }}</span>
                                                </label>
                                            </p>
                                        </div>
                                    <?php } } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m6">
                            <input id="luasTanah" type="text" name="luas_tanah" value="{{ old('luas_tanah') ?? $objekPajak->detail_objek->luas_tanah ?? '' }}">
                            <label for="luasTanah">Luas Tanah</sup></label>
                        </div>
                        <div class="input-field col s12 m6">
                            <input id="luasBangunan" type="text" name="luas_bangunan" value="{{ old('luas_bangunan') ?? $objekPajak->detail_objek->luas_bangunan ?? '' }}">
                            <label for="luasBangunan">Luas Bangunan</sup></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="luasTempatUsaha" type="text" name="luas_tempat_usaha" value="{{ old('luas_tempat_usaha') ?? $objekPajak->detail_objek->luas_tempat_usaha ?? '' }}">
                            <label for="luasTempatUsaha">Luas Tempat Usaha</sup></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <div class="row">
                                <div class="col s12 m3">
                                    <p>Status Kepemilikan</p>
                                </div>
                                <div class="col s12 m9">
                                    <div class="row">
                                        <div class="col s12 m3">
                                            <label>
                                                <input name="status_kepemilikan" type="radio" value="milik sendiri" {{ old('status_kepemilikan') === "milik sendiri" ? 'checked' : (($objekPajak->detail_objek->status_kepemilikan ?? null) === "milik sendiri" ? 'checked' : '') }} />
                                                <span>Milik sendiri</span>
                                            </label>
                                        </div>
                                        <div class="col s12 m3">
                                            <label>
                                                <input name="status_kepemilikan" type="radio" value="sewa / kontrak" {{ old('status_kepemilikan') === "sewa / kontrak" ? 'checked' : (($objekPajak->detail_objek->status_kepemilikan ?? null) === "sewa / kontrak" ? 'checked' : '') }} />
                                                <span>Sewa / Kontrak</span>
                                            </label>
                                        </div>
                                        <div class="col s12 m3">
                                            <label>
                                                <input name="status_kepemilikan" type="radio" value="bagi hasil" {{ old('status_kepemilikan') === "bagi hasil" ? 'checked' : (($objekPajak->detail_objek->status_kepemilikan ?? null) === "bagi hasil" ? 'checked' : '') }} />
                                                <span>Bagi Hasil</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m3">
                            <p>Jam Operasional</p>
                        </div>
                        <div class="col s12 m9">
                            <?php
                                if(old('jam_operasional')) {
                                    $jamOperasional = old('jam_operasional');
                                } else if(!empty($objekPajak->detail_objek->jam_operasional)) {
                                    $jamOperasional = $objekPajak->detail_objek->jam_operasional;
                                } else {
                                    $jamOperasional = [
                                        0 => [
                                            'dari_jam' => '',
                                            'sampai_jam' => ''
                                        ],
                                        1 => [
                                            'dari_jam' => '',
                                            'sampai_jam' => ''
                                        ]
                                    ];
                                }
                                // typecast
                                foreach($jamOperasional as &$i) {
                                    $i = (array) $i;
                                }
                                $jamOperasional = (array) $jamOperasional;
                                $alphabet = range('a', 'z');
                            ?>
                            @foreach($jamOperasional as $index => $jam)
                                <div class="row flex">
                                    <div class="col s12 m1 center-align" style="display: flex; justify-content: center; align-items: center;">
                                        <span>{{ $alphabet[$index] }}.</span>
                                    </div>
                                    <div class="input-field col s12 m5">
                                        <input id="jamOperasional_{{ $index }}_dari" type="text" name="jam_operasional[{{ $index }}][dari_jam]" value="{{ $jamOperasional[$index]['dari_jam'] ?? '' }}">
                                        <label for="jamOperasional_{{ $index }}_dari">Dari pukul</label>
                                    </div>
                                    <div class="input-field col s12 m5">
                                        <input id="jamOperasional_{{ $index }}_sampai" type="text" name="jam_operasional[{{ $index }}][sampai_jam]" value="{{ $jamOperasional[$index]['sampai_jam'] ?? '' }}">
                                        <label for="jamOperasional_{{ $index }}_sampai">Sampai dengan pukul</label>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12 m4 push-m8 l2 push-l10">
                            <button class="btn green waves-effect waves-light" style="width:100%" type="submit"><i class="material-icons left">save</i>Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('assets/libs/sweetalert2/dist/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <script>
        $(function () {
            $(".select2").select2({ width: '100%' });

            $("#jenis").on('select2:select', function () {
                var kode = $("#jenis option:selected").data('kode');
                $(".detail-container").hide();
                $(".detail-container[data-jenis='"+kode+"']").show();
            });
            $("#jenis").trigger('change');
            $("#jenis").trigger('select2:select');

            // fetch wajib pajak
            var findWPbyNPWPD = function () {
                if(!$('input#npwpd').val()) { return; }

                $('span#npwpd-not-found-helper').addClass('hide');
                $('input#nama-wp').val("");
                $('label#nama-wp-label').removeClass('active');

                $.ajax({
                    url: @json(url('/ajax/wajib-pajak')),
                    method: 'GET',
                    data: {
                        npwpd: $('input#npwpd').val()
                    }
                })
                .done(function (data) {
                    $('input#nama-wp').val(data.nama);
                    $('label#nama-wp-label').addClass('active');
                })
                .fail(function (error) {
                    if(error.status == 404) {
                        $('span#npwpd-not-found-helper').removeClass('hide');
                    } else {
                        swal({
                            type: 'error',
                            title: 'Error',
                            text: 'Sedang ada masalah dengan server. Mohon coba lagi nanti.'
                        });
                    }
                })
            };
            // set trigger
            var typingTimer;
            $('input#npwpd').on('keydown', function () {
                clearTimeout(typingTimer);
                typingTimer = setTimeout(findWPbyNPWPD, 1000);
            });
            $('input#npwpd').on('keyup', function () {
                clearTimeout(typingTimer);
                typingTimer = setTimeout(findWPbyNPWPD, 1000);
            });
            $('input#npwpd').on('focusout', function () {
                clearTimeout(typingTimer);
                findWPbyNPWPD();
            });
            // trigger it on page load if text is not empty
            if($('input#npwpd').val()) {
                findWPbyNPWPD();
            }
        });
    </script>
@endsection

@section('stylesheet')
    <link href="{{ asset('assets/libs/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form-fix.css') }}" rel="stylesheet">
@endsection
