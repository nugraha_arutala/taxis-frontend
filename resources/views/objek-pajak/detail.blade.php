@extends('skeleton')

<?php
    $titleItems = [];
    if(!empty($objekPajak->nop)) {
        $titleItems[] = $objekPajak->nop;
    }
    if(!empty($objekPajak->nama)) {
        $titleItems[] = $objekPajak->nama;
    }
    $titleItems[] = 'Data Objek Pajak';
    $titleItems = implode(' - ', $titleItems);
?>
@section('title', $titleItems)
@section('wrapper-title', 'Data Objek Pajak')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title activator">Data Wajib Pajak</h5>
                        <div class="row">
                            <?php $wajibPajak = $objekPajak->subjek_pajak ?>
                            <div class="col s12 m4 l2 font-bold">NPWPD</div>
                            <div class="col s12 m6 l10"><p>{{ $wajibPajak->npwpd ?? '-' }}</p></div>
                            <div class="col s12 m4 l2 font-bold">Nama Wajib Pajak</div>
                            <div class="col s12 m6 l10"><p>{{ $wajibPajak->nama ?? '-' }}</p></div>
                            <div class="col s12 m4 l2 font-bold">NIK/paspor</div>
                            <div class="col s12 m8 l10">
                                <p>
                                    @if(!empty($wajibPajak->nik) && !empty($wajibPajak->paspor))
                                    {{ $wajibPajak->nik }} / {{ $wajibPajak->paspor }}
                                    @elseif(!empty($wajibPajak->nik) || !empty($wajibPajak->paspor))
                                    {{ $wajibPajak->nik }}{{ $wajibPajak->paspor }}
                                    @else
                                    -
                                    @endif
                                </p>
                            </div>
                            <div class="col s12 m4 l2 font-bold">Tempat, Tanggal Lahir</div>
                            <div class="col s12 m8 l10"><p>{{ isset($wajibPajak->tempat_lahir) ? $wajibPajak->tempat_lahir . ', ' : '' }}{{ $wajibPajak->tanggal_lahir ?? '-' }}</p></div>
                            <div class="col s12 m4 l2 font-bold">Alamat</div>
                            <div class="col s12 m8 l10">
                                <p>
                                    {!! htmlDisplayAlamat($wajibPajak) !!}
                                </p>
                            </div>
                            <div class="col s12 m4 l2 font-bold">Nomor Telepon</div>
                            <div class="col s12 m8 l10">
                                <p>
                                    @if(!empty($wajibPajak->nomor_telepon) && !empty($wajibPajak->nomor_hp))
                                    {{ $wajibPajak->nomor_telepon }} / {{ $wajibPajak->nomor_hp }}
                                    @elseif(!empty($wajibPajak->nomor_telepon) || !empty($wajibPajak->nomor_hp))
                                    {{ $wajibPajak->nomor_telepon }}{{ $wajibPajak->nomor_hp }}
                                    @else
                                    -
                                    @endif
                                </p>
                            </div>
                        </div>
                        <br>
                        <hr>
                        <h5 class="card-title activator">{{ $objekPajak->jenis_pajak->nama }}</h5>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">NOPD</div>
                            <div class="col s12 m8 l10"><p>{{ $objekPajak->nop }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Nama Usaha</div>
                            <div class="col s12 m8 l10"><p>{{ $objekPajak->nama ?? '-' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Alamat</div>
                            <div class="col s12 m8 l10">
                                <p>
                                    {!! htmlDisplayAlamat($objekPajak) !!}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Nomor Telepon</div>
                            <div class="col s12 m8 l10">
                                <p>
                                    @if(!empty($objekPajak->detail_objek->nomor_telepon) && !empty($objekPajak->detail_objek->nomor_hp))
                                    {{ $objekPajak->detail_objek->nomor_telepon }} / {{ $objekPajak->detail_objek->nomor_hp }} (No. HP)
                                    @elseif(!empty($objekPajak->detail_objek->nomor_telepon) || !empty($objekPajak->detail_objek->nomor_hp))
                                    {{ $objekPajak->detail_objek->nomor_telepon ?? '' }}{{ $objekPajak->detail_objek->nomor_hp ?? '' }}
                                    @else
                                    -
                                    @endif
                                </p>
                            </div>
                        </div>
                        @if(!empty($objekPajak->detail_objek->status_usaha))
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Status Usaha</div>
                                <div class="col s12 m8 l10"><p>{{ ucfirst($objekPajak->detail_objek->status_usaha) ?? '-' }}</p></div>
                            </div>
                        @endif
                        @if ($objekPajak->jenis_pajak->kode === '4.1.1.02')
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Klasifikasi Usaha</div>
                                <div class="col s12 m8 l10">
                                    <ul>
                                        @foreach($objekPajak->klasifikasi_pajak as $i)
                                            <li>- {{ $i->nama }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Jenis Masakan Utama</div>
                                <div class="col s12 m8 l10">
                                    <?php
                                        $masakan = [
                                            'masakan_indonesia' => "Masakan Indonesia",
                                            'cepat_saji' => "Cepat Saji (fast food)",
                                            'masakan_laut' => "Masakan Laut (sea food)",
                                            'masakan_padang' => "Masakan Padang",
                                            'masakan_barat' => "Masakan Eropa / Amerika",
                                            'masakan_timur' => "Masakan Asia / Chinese / Japanese / Korean"
                                        ];
                                    ?>
                                    <p>{{ !empty($objekPajak->detail_objek->jenis_masakan_utama) ? $masakan[$objekPajak->detail_objek->jenis_masakan_utama] : '-' }}</p>
                                </div>
                            </div>
                        @elseif($objekPajak->jenis_pajak->kode === '4.1.1.11')
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Jenis Bahan Mineral</div>
                                @php
                                    $jbm = [];
                                    foreach($objekPajak->klasifikasi_pajak as $i) {
                                        $jbm[] = $i->nama;
                                    }
                                @endphp
                                <div class="col s12 m8 l10"><p>{{ implode($jbm, ', ') }}</p></div>
                            </div>
                        @endif
                        @if(!empty($objekPajak->detail_objek->luas_tanah) || !empty($objekPajak->detail_objek->luas_bangunan) || !empty($objekPajak->detail_objek->luas_tempat_usaha))
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Luas Tempat Usaha</div>
                                <div class="col s12 m8 l10">
                                    <p>
                                        @if(!empty($objekPajak->detail_objek->luas_tanah))
                                            Luas Tanah {{ $objekPajak->detail_objek->luas_tanah }} {!! is_numeric($objekPajak->detail_objek->luas_tanah) ? 'm<sup>2</sup>' : '' !!}<br>
                                        @endif
                                        @if(!empty($objekPajak->detail_objek->luas_bangunan))
                                            Luas Bangunan {{ $objekPajak->detail_objek->luas_bangunan }} {!! is_numeric($objekPajak->detail_objek->luas_bangunan) ? 'm<sup>2</sup>' : '' !!}<br>
                                        @endif
                                        @if(!empty($objekPajak->detail_objek->luas_tempat_usaha))
                                            Luas Tempat Usaha {{ $objekPajak->detail_objek->luas_tempat_usaha }} {!! is_numeric($objekPajak->detail_objek->luas_tempat_usaha) ? 'm<sup>2</sup>' : '' !!}<br>
                                        @endif
                                    </p>
                                </div>
                            </div>
                        @endif
                        @if(!empty($objekPajak->detail_objek->status_kepemilikan))
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Status Kepemilikan</div>
                                <div class="col s12 m8 l10"><p>{{ ucfirst($objekPajak->detail_objek->status_kepemilikan ?? '-') }}</p></div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Jam Operasional</div>
                            <div class="col s12 m8 l10">
                                <p>
                                    @if(!empty($objekPajak->detail_objek->jam_operasional))
                                        <?php $alphabet = range('a', 'z') ?>
                                        @foreach($objekPajak->detail_objek->jam_operasional as $index => $i)
                                            {{ $alphabet[$index] }}. Jam {{ $i->dari_jam }} s.d. Jam {{ $i->sampai_jam }}<br>
                                        @endforeach
                                    @else
                                        -
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Status Objek Pajak</div>
                            <div class="col s12 m8 l10"><p>{{ $objekPajak->status ? 'Sudah Diverifikasi' : 'Belum Diverifikasi' }}</p></div>
                        </div>
                        @if(!$objekPajak->status && (Auth::user()->checkRole('fiskus') || Auth::user()->checkRole('admin')))
                            <div class="row">
                                <div class="col s12 m3 offset-m8">
                                    <form method="POST" action="{{ url('/lihat-data/objek-pajak/'.$objekPajak->_id.'/verifikasi') }}">
                                        @csrf
                                        <input type="hidden" name="_id" value="{{ $objekPajak->_id }}">
                                        <button class="waves-effect waves-light green btn" style="width: 100%" type="submit"><i class="material-icons left">check</i>Verifikasi Objek Pajak</a>
                                    </form>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
