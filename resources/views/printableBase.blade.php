<style>
#print-area * {
    font-family: sans-serif;
    font-size: 12px !important;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
}
table.bordered tr td {
    border: 2px solid black;
    vertical-align: top;
}
table.noborder tr, table.noborder tr td {
    border: 0 solid white;
}
@page {
    margin: 0.5cm;
}
.pagebreak {
    page-break-before: always;
}
.barcode > div div {
    box-shadow: inset 0 0 0 1000px black; <?php // HACK biar muncul pas di print tanpa ceklisan "print background colors" di window opsi print ?>
}
</style>

<div id="print-area" style="width: 100%">
    <table class="bordered" style="border: 0 solid white; width: 100%">
        <tr>
            @if(View::hasSection('kop-kanan'))
                <td style="width: 70%; padding: 1em; height: 100%">
                    @include('print/musirawas/kop')
                </td>
                <td style="width: 30%; padding: 1em; height: 100%">
                    @yield('kop-kanan')
                </td>
            @else
                <td style="width: 100%; padding: 1em; height: 100%"
                    @if(View::hasSection('kop-colspan'))
                        @yield('kop-colspan')
                    @endif>
                    @include('print/musirawas/kop')
                </td>
            @endif
        </tr>
        @if(!isset($loop))
            @yield('letter-body')
        @else
            @yield('letter-body-'.$loop)
        @endif
    </table>
</div>
