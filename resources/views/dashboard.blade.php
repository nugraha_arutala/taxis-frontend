@extends('skeleton')

@section('title', 'Dashboard')

@section('content')
    <div class="container-fluid">
        <!-- Sales Summary -->
        <div class="row">
            <div class="col l4 m6 s12">
                <div class="card danger-gradient card-hover">
                    <div class="card-content">
                        <div class="d-flex no-block align-items-center">
                            <div>
                                <h2 class="white-text m-b-5">{{ $spt->count }}</h2>
                                <h6 class="white-text op-5 light-blue-text">Total SPTPD</h6>
                            </div>
                            <div class="ml-auto">
                                <span class="white-text display-6"><i class="material-icons">assignment</i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col l4 m6 s12">
                <div class="card info-gradient card-hover">
                    <div class="card-content">
                        <div class="d-flex no-block align-items-center">
                            <div>
                                <h2 class="white-text m-b-5">{{ $pembayaran->count }}</h2>
                                <h6 class="white-text op-5">Total Pembayaran</h6>
                            </div>
                            <div class="ml-auto">
                                <span class="white-text display-6"><i class="material-icons">attach_money</i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col l4 m6 s12">
                <div class="card warning-gradient card-hover">
                    <div class="card-content">
                        <div class="d-flex no-block align-items-center">
                            <div>
                                <h2 class="white-text m-b-5">{{ $skpd->count }}</h2>
                                <h6 class="white-text op-5">Total SKPD</h6>
                            </div>
                            <div class="ml-auto">
                                <span class="white-text display-6"><i class="material-icons">warning</i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sales Summary -->
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <div class="d-flex align-items-center">
                            <div>
                                <h5 class="card-title">Pembandingan SPTPD dan SKPD</h5>
                            </div>
                            <div class="ml-auto">
                                <ul class="list-inline font-12 dl m-r-10">
                                    <li class="cyan-text"><i class="fa fa-circle"></i> SKPD</li>
                                    <li class="blue-text text-accent-4"><i class="fa fa-circle"></i> SPTPD</li>
                                </ul>
                            </div>
                        </div>
                        <!-- Sales Summary -->
                        <div class="p-t-20">
                            <div class="row">
                                <div class="col s12">
                                    <!-- drawn on using js -->
                                    <div id="sales" style="height: 332px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('assets/libs/chartist/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js') }}"></script>
    <script>
        $(function() {
            var chart = new Chartist.Line('#sales', {
                labels: ['0', '4', '8', '12', '16', '20', '24', '30'],
                series: [
                    [0, 2, 5.5, 2, 14, 1, 8, 1],
                    [0, 8, 2, 7, 3, 4, 0, 10]
                ]
            }, {
                high: 15,
                low: 0,
                showArea: true,
                fullWidth: true,
                plugins: [
                    Chartist.plugins.tooltip()
                ], // As this is axis specific we need to tell Chartist to use whole numbers only on the concerned axis
                axisY: {
                    onlyInteger: true,
                    offset: 20,
                    labelInterpolationFnc: function(value) {
                        return (value / 1) + 'k';
                    }
                }
            });

            // Offset x1 a tiny amount so that the straight stroke gets a bounding box
            // Straight lines don't get a bounding box
            // Last remark on -> http://www.w3.org/TR/SVG11/coords.html#ObjectBoundingBox
            chart.on('draw', function(ctx) {
                if (ctx.type === 'area') {
                    ctx.element.attr({
                        x1: ctx.x1 + 0.001
                    });
                }
            });

            // Create the gradient definition on created event (always after chart re-render)
            chart.on('created', function(ctx) {
                var defs = ctx.svg.elem('defs');
                defs.elem('linearGradient', {
                    id: 'gradient',
                    x1: 0,
                    y1: 1,
                    x2: 0,
                    y2: 0
                }).elem('stop', {
                    offset: 0,
                    'stop-color': '#2ddeff'
                }).parent().elem('stop', {
                    offset: 1,
                    'stop-color': '#7f9bff'
                });
            });

            var chart = [chart];
        });
    </script>
@endsection

@section('stylesheet')
    <link href="{{ asset('css/pages/dashboard1.css') }}" rel="stylesheet">
@endsection
