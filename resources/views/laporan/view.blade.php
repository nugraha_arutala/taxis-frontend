@extends('skeleton')

@section('title', 'Unduh Laporan')

@section('content')
    <div class="container-fluid">
    @if ($errors->any())
        <div class="card red darken-4">
            <div class="card-content white-text">
                <span class="card-title">Submit Gagal</span>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        </div>
        @endif
        <form method="POST" action="{{ url('/reporting') }}">
            @csrf
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <h5 class="card-title activator">Rentan Waktu</h5>
                            <div class="row">
                                <div class="input-field col s3">
                                    <p><label>
                                        <input name="time_frame" type="radio" value="all" checked/>
                                        <span>Semua</span>
                                    </label></p>
                                </div>
                                <div class="input-field col s3">
                                    <p><label>
                                        <input name="time_frame" type="radio" value="daily" />
                                        <span>Hari ini</span>
                                    </label></p>
                                </div>
                                <div class="input-field col s3">
                                    <p><label>
                                        <input name="time_frame" type="radio" value="monthly" />
                                        <span>Bulanan</span>
                                    </label></p>
                                </div>
                                <div class="input-field col s3">
                                    <p><label>
                                        <input name="time_frame" type="radio" value="costum" />
                                        <span>Kostum</span>
                                    </label></p>
                                </div>
                            </div>

                            <div id="monthly" class="row time_frame">
                                <div class="input-field col s6">
                                    <select class="browser-default" id="bulan" name="bulan">
                                        <option value="01" >Jan</option>
                                        <option value="03" >Feb</option>
                                        <option value="03" >Mar</option>
                                        <option value="04" >Apr</option>
                                        <option value="05" >Mei</option>
                                        <option value="06" >Jul</option>
                                        <option value="07" >Jun</option>
                                        <option value="08" >Agu</option>
                                        <option value="09" >Sep</option>
                                        <option value="10" >Okt</option>
                                        <option value="11" >Nov</option>
                                        <option value="12" >Des</option>
                                    </select>
                                    <span>Bulan</span>
                                </div>
                                <div class="input-field col s6">
                                    <select class="browser-default" id="tahun" name="tahun">
                                        <option value="2018" >2018</option>
                                        <option value="2019" >2019</option>
                                        <option value="2020" >2020</option>
                                        <option value="2021" >2021</option>
                                        <option value="2022" >2022</option>
                                        <option value="2023" >2023</option>
                                        <option value="2024" >2024</option>
                                    </select>
                                    <span>Tahun</span>
                                </div>
                            </div>

                            <div id="costum" class="row time_frame">
                                <div class="input-field col s6">
                                    <input type="text" name="start_date" class="datepicker">
                                    <span>Tanggal Mulai</span>
                                </div>
                                <div class="input-field col s6">
                                    <input type="text" name="end_date" class="datepicker">
                                    <span>Tanggal Akhir</span>
                                </div>
                            </div>

                            <h5 class="card-title activator">Modul Laporan</h5>
                            <div class="row">
                                <div class="input-field col s3">
                                    <p><label>
                                        <input name="module" type="radio" value="pembayaran" checked/>
                                        <span>SPT/Pembayaran</span>
                                    </label></p>
                                </div>
                                <div class="input-field col s3">
                                    <p><label>
                                        <input name="module" type="radio" value="wajib_pajak"/>
                                        <span>Wajib Pajak</span>
                                    </label></p>
                                </div>
                                <div class="input-field col s3">
                                    <p><label>
                                        <input name="module" type="radio" value="objek_pajak" />
                                        <span>Objek Pajak</span>
                                    </label></p>
                                </div>
                                <div class="input-field col s3">
                                    <p><label>
                                        <input name="module" type="radio" value="user"/>
                                        <span>Pengguna</span>
                                    </label></p>
                                </div>
                            </div>

                            <h5 class="card-title activator module pembayaran">Filter SPT/pembayaran</h5>
                            <div class="row module pembayaran">
                                <div class="input-field col s4">
                                    <p><label>
                                        <input name="stp_status" type="radio" value="" checked/>
                                        <span>ALL</span>
                                    </label></p>
                                </div>
                                <div class="input-field col s4">
                                    <p><label>
                                        <input name="stp_status" type="radio" value="1"/>
                                        <span>Lunas</span>
                                    </label></p>
                                </div>
                                <div class="input-field col s4">
                                    <p><label>
                                        <input name="stp_status" type="radio" value="0"/>
                                        <span>Belum dibayar</span>
                                    </label></p>
                                </div>
                                <!-- <div class="input-field col s12">
                                    <select name="jenis_pajak" multiple>
                                        @foreach($klasifikasiPajak as $klasifikasiP)
                                        <option value="{{ $klasifikasiP->_id }}" data-kode="{{ $klasifikasiP->kode }}" {{ old('klasifikasi_pajak') === $klasifikasiP->_id ? 'selected' : (($objekPajak->klasifikasi_pajak->_id ?? null) === $klasifikasiP->_id ? 'selected' : '') }}>{{ $klasifikasiP->nama }}</option>
                                        @endforeach
                                    </select>
                                    <span>Jenis Pajak</span>
                                </div> -->
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="btn cyan waves-effect waves-light right" type="submit">Unduh</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('javascript')
<script>
    $(function () {
        $("select").not(".disabled").formSelect();

        $(".datepicker").datepicker({ format: 'yyyy-mm-dd' });

        $(this).on('change', 'input[name="time_frame"]', function () {
            var value = $(this).val();
            $('.time_frame').slideUp();
            $('#' + value).slideDown();
        });

        $(this).on('change', 'input[name="module"]', function () {
            var value = $(this).val();
            $('.module').slideUp();
            $('.' + value).slideDown();
        });
    });
</script>
@endsection

@section('stylesheet')
    <style type="text/css">
        .time_frame {
            display: none;
        }
    </style>
@endsection
