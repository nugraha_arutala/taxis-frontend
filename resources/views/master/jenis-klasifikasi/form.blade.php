@extends('skeleton')

@section('title', 'Form Data Objek Pajak dan Sub Objek Pajak')

@section('content')
    <div class="container-fluid">
        @if ($errors->any())
        <div class="card red darken-4">
            <div class="card-content white-text">
                <span class="card-title">Submit Gagal</span>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        </div>
        @endif
        <form method="POST" action="{{ empty($klasifikasiPajak) ? url('/master-data/jenis-klasifikasi/') : url('/master-data/jenis-klasifikasi/' . $klasifikasiPajak->_id) }}">
            @csrf
            @if(!empty($klasifikasiPajak))
                <input type="hidden" name="_id" value="{{ $klasifikasiPajak->_id }}">
            @endif
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="kode" type="text" name="kode" value="{{ old('kode') ?? $klasifikasiPajak->kode ?? '' }}" required>
                                    <label for="kode">Kode</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="nama" type="text" name="nama" value="{{ old('nama') ?? $klasifikasiPajak->nama ?? '' }}" required>
                                    <label for="nama">Nama</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="deskripsi" type="text" name="deskripsi" value="{{ old('deskripsi') ?? $klasifikasiPajak->deskripsi ?? '' }}">
                                    <label for="deskripsi">Deskripsi</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <div class="col s12 m3">
                                        <p>Jenis</p>
                                    </div>
                                    <div class="input-field col s12 m9">
                                        <p>
                                            <label>
                                                <input name="tipe" type="radio" value="jenis_pajak" {{ old('tipe') === "jenis_pajak" ? 'checked' : (($klasifikasiPajak->parent_id ?? null) === null ? 'checked' : '') }} required/>
                                                <span>Objek Pajak</span>
                                            </label>
                                        </p>
                                        <p>
                                            <label>
                                                <input name="tipe" type="radio" value="klasifikasi_usaha" {{ old('tipe') === "klasifikasi_usaha" ? 'checked' : (($klasifikasiPajak->parent_id ?? null) ? 'checked' : '') }} required/>
                                                <span>Sub Objek Pajak</span>
                                            </label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div id="klasifikasi-usaha" style="display: none">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <p>Kode Jenis Pajak</p>
                                        <select name="parent" class="browser-default">
                                            @foreach ($listParent as $i)
                                                <option value="{{ $i->_id }}" {{ (old('parent') ?? $klasifikasiPajak->parent->_id ?? '') === $i->_id ? 'selected' : '' }}>{{ $i->kode }}: {{ $i->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="tarif" type="number" name="tarif" value="{{ old('tarif') ?? $klasifikasiPajak->tarif ?? '' }}" required>
                                        <label for="tarif">Tarif % DPP</label>
                                    </div>
                                </div>
                                <div class="row" style="display: none" id="row-harga">
                                    <div class="input-field col s12">
                                        <input id="harga" type="number" name="harga" value="{{ old('harga') ?? $klasifikasiPajak->harga ?? '' }}" required>
                                        <label for="harga">Harga Perunit (Rp.)</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="btn cyan waves-effect waves-light right" type="submit">Simpan Data</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('assets/libs/sweetalert2/dist/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <script>
        $(function () {
            $('input[name=tipe]').change(function () {
                if($('input[name=tipe]:checked').val() === 'klasifikasi_usaha') {
                    $('div#klasifikasi-usaha').show();
                    $('input#parent_kode').prop('required', true);
                    $('input#tarif').prop('required', true);
                } else {
                    $('div#klasifikasi-usaha').hide();
                    $('input#parent_kode').prop('required', false);
                    $('input#tarif').prop('required', false);
                }
            }).trigger('change');

            $('select[name=parent]').change(function () {
                tx = $('select[name=parent] option:selected').text().split(':');
                if(tx[0] === '4.1.1.11') {
                    $('div#row-harga').show();
                } else {
                    $('div#row-harga').hide();
                }
            }).trigger('change');
        });
    </script>
@endsection
