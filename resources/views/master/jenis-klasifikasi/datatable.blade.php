@extends('skeleton')

@section('title', 'Daftar Objek Pajak dan Sub Objek Pajak')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                @if (Session::has('message'))
                    @if(Session::get('message') === 'save-successful')
                    <div class="card green darken-2">
                        <div class="card-content white-text">
                            Data baru berhasil disimpan.
                        </div>
                    </div>
                    @endif
                @endif
                <div class="card">
                    <div class="card-content">
                        <div class="right-align">
                            <a class="btn-floating btn-large waves-effect waves-light green" href="{{ url('/master-data/jenis-klasifikasi/tambah') }}"><i class="material-icons">add</i></a>
                        </div>
                        <table id="datatable" class="responsive-table display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Jenis</th>
                                    <th>Objek Pajak</th>
                                    <th>Tarif</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="99" style="text-align: center">Loading...</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hide">
        <!-- action button column for datatable -->
        <div id="action-button-column-sample">
            <a class="waves-effect waves-light btn-small view" href="{{ url('/master-data/jenis-klasifikasi/[id]') }}"><i class="material-icons">search</i></a>
            <a class="waves-effect waves-light btn-small edit" href="{{ url('/master-data/jenis-klasifikasi/[id]/edit') }}"><i class="material-icons">edit</i></a>
        </div>
        <div id="graveyard" style="display:none"></div>
    </div>
@endsection

@section('stylesheet')
    <link href="{{ asset('css/datatable-fix.css') }}" rel="stylesheet">
@endsection
@section('javascript')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script>
        // Element init
        $(function () {
            $("select").not(".disabled").formSelect();
        });

        $('#datatable').DataTable({
            serverSide: true,
            lengthChange: false,
            ordering: false,
            order: [],
            searching: true,
            ajax: {
                url: '{{ url('/master-data/jenis-klasifikasi/datatable') }}',
                type: 'GET'
            },
            columns: [
                { data: 'kode' },
                { data: 'nama' },
                {
                    data: function(row, type, set, meta) {
                        if(row.parent) {
                            return 'Sub Objek Pajak';
                        } else {
                            return 'Objek Pajak';
                        }
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        if(row.parent) {
                            return row.parent.nama;
                        } else {
                            return row.nama;
                        }
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        if(row.tarif) {
                            return row.tarif + " %";
                        } else {
                            return "";
                        }
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        var div = $('#action-button-column-sample').clone();
                        var buttons = div.find('a');
                        for(i of buttons) {
                            $(i).prop('href', $(i).prop('href').replace('[id]', row._id));
                        }
                        return div.html();
                    },
                    orderable: false
                }
            ]
        });

        // manual search
        $('#datatable_filter').remove();
        var typingTimer;
        $('#search_value').on('keydown', function () {
            clearTimeout(typingTimer);
        });
        $('#search_value').on('keyup', function () {
            var self = $(this);
            clearTimeout(typingTimer);
            typingTimer = setTimeout(function () {
                $('#datatable').DataTable().search($('#search_field').val() + "|" + self.val()).draw();
            }, 1000);
        });
    </script>
@endsection
