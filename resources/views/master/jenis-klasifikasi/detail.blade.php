@extends('skeleton')

@php
    $tipe = empty($klasifikasiPajak->parent) ? "Objek Pajak" : "Sub Objek Pajak";
@endphp

@section('title', ($klasifikasiPajak->nama ?? '?') . ' - Data Objek Pajak dan Sub Objek Pajak')
@section('wrapper-title', 'Data Objek Pajak dan Sub Objek Pajak')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title activator">{{ $tipe }}</h5>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Kode</div>
                            <div class="col s12 m8 l10"><p>{{ $klasifikasiPajak->kode ?? '-' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Nama</div>
                            <div class="col s12 m8 l10"><p>{{ $klasifikasiPajak->nama ?? '-' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Deskripsi</div>
                            <div class="col s12 m8 l10"><p>{{ $klasifikasiPajak->deskripsi ?? '-' }}</p></div>
                        </div>
                        @if ($tipe === "Sub Objek Pajak")
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Kode Jenis Pajak</div>
                                <div class="col s12 m8 l10"><p>{{ $klasifikasiPajak->parent->kode ?? '-' }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Nama Jenis Pajak</div>
                                <div class="col s12 m8 l10"><p>{{ $klasifikasiPajak->parent->nama ?? '-' }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Tarif % DPP</div>
                                <div class="col s12 m8 l10"><p>{{ $klasifikasiPajak->tarif ? rtrim($klasifikasiPajak->tarif, '%') : '0' }}%</p></div>
                            </div>
                            @if(isset($klasifikasiPajak->harga))
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Harga (Rp.)</div>
                                <div class="col s12 m8 l10"><p>{{ number_format($klasifikasiPajak->harga, 2, ',', '.') ?? '0' }}</p></div>
                            </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
