<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!-- This page CSS -->
    <link href="{{ asset('css/pages/authentication.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div class="main-wrapper">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="loader">
                <div class="loader__figure"></div>
                <p class="loader__label">TaxIS</p>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url('{{ asset('assets/images/big/auth-bg.jpg') }}') no-repeat center center;">
            <div class="auth-box">
                <div id="loginform">
                    <div class="logo">
                        <span class="db"><img src="{{ asset('img/taxis-dark-icon.png') }}" alt="logo" /></span>
                        <h5 class="font-medium m-b-20">Pajak Daerah Online</h5>
                    </div>
                    <!-- Form -->
                    <div class="row">
                        @if ($errors->has('wrong-password'))
                        <div class="card red darken-4">
                            <div class="card-content white-text">
                                <span class="card-title">Login gagal</span>
                                <p>Username atau password salah.</p>
                            </div>
                        </div>
                        @endif
                        <form class="col s12" method="POST" action="{{ url('login') }}">
                            <!-- csrf -->
                            @csrf
                            <!-- username -->
                            <div class="row">
                                <div class="input-field col s12">
                                    <input name="username" id="username" type="text" class="validate" required>
                                    <label for="username">Username</label>
                                </div>
                            </div>
                            <!-- pwd -->
                            <div class="row">
                                <div class="input-field col s12">
                                    <input name="password" id="password" type="password" class="validate" required>
                                    <label for="password">Password</label>
                                </div>
                            </div>
                            <!-- button -->
                            <div class="row m-t-40">
                                <div class="col s12">
                                    <button class="btn-large w100 blue accent-4" type="submit">Login</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('js/materialize.min.js') }}"></script>
    <script>
    $('.tooltipped').tooltip();
    $(function() {
        $(".preloader").fadeOut();
    });
    </script>
</body>

</html>
