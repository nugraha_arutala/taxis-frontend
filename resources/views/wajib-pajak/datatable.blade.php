@extends('skeleton')

@section('title', 'Daftar Wajib Pajak')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                @if (Session::has('message'))
                    @if(Session::get('message') === 'save-successful')
                    <div class="card green darken-2">
                        <div class="card-content white-text">
                            Data baru berhasil disimpan.
                        </div>
                    </div>
                    @endif
                @endif
                <div class="card">
                    <div class="card-content">
                        <div class="right-align">
                            <a class="btn-floating btn-large waves-effect waves-light green" href="{{ url('/pendaftaran/wajib-pajak') }}"><i class="material-icons">add</i></a>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 offset-m6">
                                <div class="row">
                                    <div class="input-field col s4">
                                        <select id="search_field">
                                            <option value="npwpd" selected>NPWPD</option>
                                            <option value="npwp">NPWP</option>
                                        </select>
                                    </div>
                                    <div class="input-field col s8">
                                        <input id="search_value" type="text">
                                        <label for="search_value">Cari</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table id="datatable" class="responsive-table display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NPWPD</th>
                                    <th>NPWP</th>
                                    <th>NIK / Paspor</th>
                                    <th>Nama</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="99" style="text-align: center">Loading...</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hide">
        <!-- action button column for datatable -->
        <div id="action-button-column-sample">
            <a class="waves-effect waves-light btn-small view" href="{{ url('/lihat-data/wajib-pajak/[id]') }}"><i class="material-icons">search</i></a>
            <a class="waves-effect waves-light btn-small edit" href="{{ url('/lihat-data/wajib-pajak/[id]/edit') }}"><i class="material-icons">edit</i></a>
        </div>
    </div>
@endsection

@section('stylesheet')
    <link href="{{ asset('css/datatable-fix.css') }}" rel="stylesheet">
@endsection
@section('javascript')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script>
        // Element init
        $(function () {
            $("select").not(".disabled").formSelect();
        });

        $('#datatable').DataTable({
            serverSide: true,
            lengthChange: false,
            ordering: false,
            order: [],
            searching: true,
            ajax: {
                url: '{{ url('/lihat-data/wajib-pajak/datatable') }}',
                type: 'GET'
            },
            columns: [
                {
                    data: function(row, type, set, meta) {
                        var pginfo = $(meta.settings.nTable).DataTable().page.info();
                        return (pginfo.page * pginfo.length) + (meta.row + 1);
                    },
                    className: 'center-align'
                },
                {
                    data: function(row, type, set, meta) {
                        return row.npwpd || '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.npwp || '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        if(row.nik && row.paspor) {
                            return row.nik + ' / ' + row.paspor;
                        } else if(row.nik || row.paspor) {
                            return row.nik || row.paspor;
                        }
                        return '-';
                    }
                },
                { data: 'nama' },
                {
                    data: function(row, type, set, meta) {
                        var div = $('#action-button-column-sample').clone();
                        var buttons = div.find('a');
                        for(i of buttons) {
                            $(i).prop('href', $(i).prop('href').replace('[id]', row._id));
                        }
                        return div.html();
                    },
                    orderable: false
                }
            ]
        });

        // manual search
        $('#datatable_filter').remove();
        var typingTimer;
        $('#search_value').on('keydown', function () {
            clearTimeout(typingTimer);
        });
        $('#search_value').on('keyup', function () {
            var self = $(this);
            clearTimeout(typingTimer);
            typingTimer = setTimeout(function () {
                $('#datatable').DataTable().search($('#search_field').val() + "|" + self.val()).draw();
            }, 1000);
        });
    </script>
@endsection
