@extends('skeleton')

@section('title', 'Data Wajib Pajak')

@section('content')
    <div class="container-fluid">
        @if ($errors->any())
        <div class="card red darken-4">
            <div class="card-content white-text">
                <span class="card-title">Submit Gagal</span>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        </div>
        @endif
        <form method="POST" action="{{ url(!isset($profile) ? '/pendaftaran/wajib-pajak' : '/profil/edit-wp') }}">
            @csrf
            @if(!empty($wajibPajak))
                <input type="hidden" name="_id" value="{{ $wajibPajak->_id }}">
            @endif
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <h5 class="card-title activator">Data Diri</h5>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="nama" type="text" name="nama" value="{{ old('nama') ?? $wajibPajak->nama ?? '' }}">
                                    <label for="nama">Nama Lengkap</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="kewarganegaraan" type="text" name="kewarganegaraan" value="{{ old('kewarganegaraan') ?? $wajibPajak->kewarganegaraan ?? '' }}">
                                    <label for="kewarganegaraan">Kewarganegaraan</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <input id="tempatLahir" type="text" name="tempat_lahir" value="{{ old('tempat_lahir') ?? $wajibPajak->tempat_lahir ?? '' }}">
                                    <label for="tempatLahir">Tempat Lahir</label>
                                </div>
                                <div class="input-field col s12 m6">
                                    <input id="tanggalLahir" type="text" name="tanggal_lahir" placeholder="DD/MM/YYYY" maxlength="10" value="{{ old('tanggal_lahir') ?? $wajibPajak->tanggal_lahir ?? '' }}">
                                    <label for="tanggalLahir">Tanggal Lahir</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="nik" type="text" name="nik" value="{{ old('nik') ?? $wajibPajak->nik ?? '' }}">
                                    <label for="nik">NIK</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="paspor" type="text" name="paspor" value="{{ old('paspor') ?? $wajibPajak->paspor ?? '' }}">
                                    <label for="paspor">Nomor paspor</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="npwp" type="text" name="npwp" value="{{ old('npwp') ?? $wajibPajak->npwp ?? '' }}">
                                    <label for="npwp">NPWP</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="npwpd" type="text" name="npwpd" value="{{ old('npwpd') ?? $wajibPajak->npwpd ?? '' }}">
                                    <label for="npwpd">NPWPD</label>
                                </div>
                            </div>
                            <hr>
                            <h5 class="card-title activator">Alamat</h5>
                            <div class="row">
                                <div class="input-field col s12">
                                    <textarea id="jalan" class="materialize-textarea" name="jalan" rows="4">{{ old('jalan') ?? $wajibPajak->alamat_jalan ?? '' }}</textarea>
                                    <label for="jalan">Alamat</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m4">
                                    <input id="rt" type="text" name="rt" value="{{ old('rt') ?? $wajibPajak->alamat_rt ?? '' }}">
                                    <label for="rt">RT</label>
                                </div>
                                <div class="input-field col s12 m4">
                                    <input id="rw" type="text" name="rw" value="{{ old('rw') ?? $wajibPajak->alamat_rw ?? '' }}">
                                    <label for="rw">RW</label>
                                </div>
                                <div class="input-field col s12 m4">
                                    <input id="kodePos" type="text" name="kode_pos" value="{{ old('kode_pos') ?? $wajibPajak->alamat_kode_pos ?? '' }}">
                                    <label for="kodePos">Kode Pos</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="provinsi" type="text" name="provinsi" value="{{ old('provinsi') ?? $wajibPajak->alamat_provinsi ?? '' }}">
                                    <label for="provinsi">Provinsi</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="kota" type="text" name="kota" value="{{ old('kota') ?? $wajibPajak->alamat_kota ?? '' }}">
                                    <label for="kota">Kota / Kabupaten</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="kecamatan" type="text" name="kecamatan" value="{{ old('kecamatan') ?? $wajibPajak->alamat_kecamatan ?? '' }}">
                                    <label for="kecamatan">Kecamatan</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="kelurahan" type="text" name="kelurahan" value="{{ old('kelurahan') ?? $wajibPajak->alamat_kelurahan ?? '' }}">
                                    <label for="kelurahan">Kelurahan</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m6">
                                    <input id="nomorTelepon" type="text" name="nomor_telepon" value="{{ old('nomor_telepon') ?? $wajibPajak->nomor_telepon ?? '' }}">
                                    <label for="nomorTelepon">No. Telepon</label>
                                </div>
                                <div class="input-field col s12 m6">
                                    <input id="nomorHp" type="text" name="nomor_hp" value="{{ old('nomor_hp') ?? $wajibPajak->nomor_hp ?? '' }}">
                                    <label for="nomorHp">No. Handphone</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="email" type="text" name="email" value="{{ old('email') ?? $wajibPajak->email ?? '' }}">
                                    <label for="email">E-Mail</label>
                                </div>
                            </div>
                            <hr>
                            <h5 class="card-title activator">Jenis Wajib Pajak</h5>
                            <div class="row">
                                <div class="input-field col s12">
                                    <div class="col s12 m3">
                                        <p>Jenis Wajib Pajak</p>
                                    </div>
                                    <div class="input-field col s12 m9">
                                        <p>
                                            <label>
                                                <input name="jenis" type="radio" value="perorangan" {{ old('jenis') === 'perorangan' ? 'checked' : (($wajibPajak->jenis ?? null) === 'perorangan' ? 'checked' : '') }} />
                                                <span>Perorangan</span>
                                            </label>
                                        </p>
                                        <p>
                                            <label>
                                                <input name="jenis" type="radio" value="perusahaan" {{ old('jenis') === 'perusahaan' ? 'checked' : (($wajibPajak->jenis ?? null) === 'perusahaan' ? 'checked' : '') }} />
                                                <span>Perusahaan</span>
                                            </label>
                                        </p>
                                        <p>
                                            <label>
                                                <input name="jenis" type="radio" value="bendaharawan" {{ old('jenis') === 'bendaharawan' ? 'checked' : (($wajibPajak->jenis ?? null) === 'bendaharawan' ? 'checked' : '') }} />
                                                <span>Bendaharawan</span>
                                            </label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="btn cyan waves-effect waves-light right" type="submit">Simpan Data</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
