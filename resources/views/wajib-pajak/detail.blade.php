@extends('skeleton')

@section('title', ($wajibPajak->nama ?? '?') . ' - Data Wajib Pajak')
@section('wrapper-title', 'Data Wajib Pajak')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title activator">Wajib Pajak</h5>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">NPWPD</div>
                            <div class="col s12 m8 l10"><p>{{ $wajibPajak->npwpd ?? '-' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">NPWP</div>
                            <div class="col s12 m8 l10"><p>{{ $wajibPajak->npwp ?? '-' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">NIK/paspor</div>
                            <div class="col s12 m8 l10">
                                <p>
                                    @if(!empty($wajibPajak->nik) && !empty($wajibPajak->paspor))
                                    {{ $wajibPajak->nik }} / {{ $wajibPajak->paspor }}
                                    @elseif(!empty($wajibPajak->nik) || !empty($wajibPajak->paspor))
                                    {{ $wajibPajak->nik }}{{ $wajibPajak->paspor }}
                                    @else
                                    -
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Kewarganegaraan</div>
                            <div class="col s12 m8 l10"><p>{{ $wajibPajak->kewarganegaraan ?? '-' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Nama Lengkap</div>
                            <div class="col s12 m8 l10"><p>{{ $wajibPajak->nama ?? '-' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Tempat, Tanggal Lahir</div>
                            <div class="col s12 m8 l10"><p>{{ isset($wajibPajak->tempat_lahir) ? $wajibPajak->tempat_lahir . ', ' : '' }}{{ $wajibPajak->tanggal_lahir ?? '-' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Alamat</div>
                            <div class="col s12 m8 l10">
                                <p>
                                    @if(!empty($wajibPajak->alamat->jalan))
                                        {{ $wajibPajak->alamat->jalan }}<br>
                                    @endif

                                    @if(!empty($wajibPajak->alamat->rt))
                                    RT: {{ $wajibPajak->alamat->rt }}
                                    @endif
                                    @if(!empty($wajibPajak->alamat->rw))
                                    RW: {{ $wajibPajak->alamat->rw }}
                                    @endif
                                    @if(!empty($wajibPajak->alamat->kode_pos))
                                    Kode Pos: {{ $wajibPajak->alamat->kode_pos }}
                                    @endif

                                    @if(!empty($wajibPajak->alamat->rt) || !empty($wajibPajak->alamat->rw) || !empty($wajibPajak->alamat->kode_pos))
                                    <br>
                                    @endif

                                    @if(!empty($wajibPajak->alamat->provinsi))
                                    Provinsi: {{ $wajibPajak->alamat->provinsi }}
                                    @endif
                                    @if(!empty($wajibPajak->alamat->kota))
                                    Kabupaten/Kota: {{ $wajibPajak->alamat->kota }}
                                    @endif

                                    @if(!empty($wajibPajak->alamat->provinsi) || !empty($wajibPajak->alamat->kota))
                                    <br>
                                    @endif


                                    @if(!empty($wajibPajak->alamat->kecamatan))
                                    Kecamatan: {{ $wajibPajak->alamat->kecamatan }}
                                    @endif
                                    @if(!empty($wajibPajak->alamat->kelurahan))
                                    Kelurahan: {{ $wajibPajak->alamat->kelurahan }}
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Nomor Telepon</div>
                            <div class="col s12 m8 l10">
                                <p>
                                    @if(!empty($wajibPajak->nomor_telepon) && !empty($wajibPajak->nomor_hp))
                                    {{ $wajibPajak->nomor_telepon }} / {{ $wajibPajak->nomor_hp }}
                                    @elseif(!empty($wajibPajak->nomor_telepon) || !empty($wajibPajak->nomor_hp))
                                    {{ $wajibPajak->nomor_telepon }}{{ $wajibPajak->nomor_hp }}
                                    @else
                                    -
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Email</div>
                            <div class="col s12 m8 l10"><p>{{ $wajibPajak->email ?? '-' }}</p></div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title activator">Objek Pajak</h5>
                        <div class="row">
                            <div class="right-align">
                                <a class="waves-effect waves-light btn green" href="{{ url('/pendaftaran/objek-pajak/' . $wajibPajak->_id) }}"><i class="material-icons left">add_circle</i>Tambah Objek Pajak</a>
                            </div>
                            <table id="datatable" class="responsive-table display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NOPD</th>
                                        <th>Nama Usaha</th>
                                        <th>Alamat</th>
                                        <th>Jenis Pajak</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="99" style="text-align: center">Loading...</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hide">
        <!-- action button column for datatable -->
        <div id="action-button-column-sample">
            <a class="waves-effect waves-light btn-small view" href="{{ url('/lihat-data/objek-pajak/[id]') }}"><i class="material-icons">search</i></a>
            <a class="waves-effect waves-light btn-small edit" href="{{ url('/lihat-data/objek-pajak/[id]/edit') }}"><i class="material-icons">edit</i></a>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script>
        $('#datatable').DataTable({
            serverSide: true,
            ordering: false,
            order: [],
            searching: false,
            ajax: {
                url: '{{ url('/lihat-data/wajib-pajak/'.$wajibPajak->_id.'/objek-pajak/datatable') }}',
                type: 'GET'
            },
            columns: [
                {
                    data: function(row, type, set, meta) {
                        var pginfo = $(meta.settings.nTable).DataTable().page.info();
                        return (pginfo.page * pginfo.length) + (meta.row + 1);
                    },
                    className: 'center-align'
                },
                {
                    data: function(row, type, set, meta) {
                        return row.nop || '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.detail_objek.nama || '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.detail_objek.alamat.kota || '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.jenis_pajak.nama || '???';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        var div = $('#action-button-column-sample').clone();
                        var buttons = div.find('a');
                        for(i of buttons) {
                            $(i).prop('href', $(i).prop('href').replace('[id]', row._id));
                        }
                        return div.html();
                    },
                    orderable: false
                }
            ]
        });
    </script>
@endsection
