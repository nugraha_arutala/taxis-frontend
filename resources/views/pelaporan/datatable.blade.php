@extends('skeleton')

@section('title', 'Pelaporan')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                @if (Session::has('message'))
                    @if(Session::get('message') === 'save-successful')
                    <div class="card green darken-2">
                        <div class="card-content white-text">
                            Data baru berhasil disimpan.
                        </div>
                    </div>
                    @endif
                @endif
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col s12">
                                <div class="row">
                                    <div class="input-field col s8">
                                        <select id="search_jenisPajak">
                                            <option value="">Tampilkan semua</option>
                                            @foreach($jenisPajak as $i)
                                                <option value="{{ $i->_id }}" data-kode="{{ $i->kode }}">{{ $i->nama }}</option>
                                            @endforeach
                                        </select>
                                        <label for="search_jenisPajak">Tampilkan sesuai Jenis Pajak</label>
                                    </div>
                                    <div class="input-field col s4">
                                        <select id="search_status">
                                            <option value="">Tampilkan semua</option>
                                            <option value="0">Belum diverifikasi</option>
                                            <option value="1">Terverifikasi</option>
                                        </select>
                                        <label for="search_status">Status</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table id="datatable" class="responsive-table display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NOPD</th>
                                    <th>Nama Usaha</th>
                                    <th>Alamat</th>
                                    <th>Jenis Pajak</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="99" style="text-align: center">Loading...</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hide">
        <!-- action button column for datatable -->
        <div id="action-button-column-sample">
            <a class="waves-effect waves-light btn-small view" href="{{ url('/pelaporan/[id]') }}"><i class="material-icons">search</i></a>
        </div>
    </div>
@endsection

@section('stylesheet')
    <link href="{{ asset('css/datatable-fix.css') }}" rel="stylesheet">
@endsection
@section('javascript')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script>
        // Element init
        $(function () {
            $("select").not(".disabled").formSelect();
        });

        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            lengthChange: false,
            ordering: false,
            order: [],
            searching: true,
            ajax: {
                url: '{{ url('/pelaporan/datatable') }}',
                type: 'GET'
            },
            columns: [
                {
                    data: function(row, type, set, meta) {
                        var pginfo = $(meta.settings.nTable).DataTable().page.info();
                        return (pginfo.page * pginfo.length) + (meta.row + 1);
                    },
                    className: 'center-align'
                },
                {
                    data: function(row, type, set, meta) {
                        return row.nop || '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.detail_objek.nama || '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.detail_objek.alamat.kota || '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.jenis_pajak.nama || '???';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        var div = $('#action-button-column-sample').clone();
                        var buttons = div.find('a');
                        for(i of buttons) {
                            $(i).prop('href', $(i).prop('href').replace('[id]', row._id));
                        }
                        return div.html();
                    },
                    orderable: false
                }
            ]
        });

        // manual search
        $('#datatable_filter').remove();

        $('#search_status').val('');
        $('#search_jenisPajak').val('');

        $('#search_status').on('change', function (e) {
            $('#datatable').DataTable().columns(5).search($('#search_status').val()).draw();
        })
        $('#search_jenisPajak').on('change', function (e) {
            $('#datatable').DataTable().columns(4).search($('#search_jenisPajak').val()).draw();
        })
    </script>
@endsection
