@extends('skeleton')

@section('title', 'Cetak SPTPD ' . ($objekPajak->detail_objek->nama))

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <div id="print-target">
                            @include('pelaporan/printarea')
                        </div>
                        <div class="row">
                            <div class="input-field col s12 l3 push-l6">
                                @if($spt->status === 0)
                                    <a class="btn purple waves-effect waves-light" style="width:100%" href="{{ url('pelaporan/'.$objekPajak->_id.'/form/'.$spt->_id) }}"><i class="material-icons left">edit</i>Koreksi</a>
                                @endif
                            </div>
                            <div class="input-field col s12 l3 push-l6">
                                <button class="btn blue waves-effect waves-light" style="width:100%" id="print"><i class="material-icons left">print</i>Cetak</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('stylesheet')
<style>
    #print-target { border: 2px solid #000; }
</style>
@endsection

@section('javascript')
<script>
$(function () {
    $('#print').click(function (e) {
        w=window.open();
        w.document.write($('#print-target').html());
        w.print();
        w.close();
    })
});
</script>
@endsection
