@extends('skeleton')

@section('title', 'Detail SPTPD ' . ($objekPajak->detail_objek->nama))
@section('wrapper-title', 'Detail SPTPD')

@section('content')
    <div class="container-fluid">
        @if ($errors->any())
        <div class="card red darken-4">
            <div class="card-content white-text">
                <span class="card-title">Submit Gagal</span>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title activator">{{ $objekPajak->jenis_pajak->nama }}</h5>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Nomor Objek Pajak Daerah</div>
                            <div class="col s12 m6 l8"><p>{{ $objekPajak->nop }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Nama Usaha</div>
                            <div class="col s12 m6 l8"><p>{{ $objekPajak->detail_objek->nama ?? '-' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Alamat</div>
                            <div class="col s12 m6 l8">
                                <p>
                                    @if(!empty($objekPajak->detail_objek->alamat->jalan))
                                        {{ $objekPajak->detail_objek->alamat->jalan }}<br>
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->rt))
                                    RT: {{ $objekPajak->detail_objek->alamat->rt }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->rw))
                                    RW: {{ $objekPajak->detail_objek->alamat->rw }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->kode_pos))
                                    Kode Pos: {{ $objekPajak->detail_objek->alamat->kode_pos }}
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->rt) || !empty($objekPajak->detail_objek->alamat->rw) || !empty($objekPajak->detail_objek->alamat->kode_pos))
                                    <br>
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->provinsi))
                                    Provinsi: {{ $objekPajak->detail_objek->alamat->provinsi }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->kota))
                                    Kabupaten/Kota: {{ $objekPajak->detail_objek->alamat->kota }}
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->provinsi) || !empty($objekPajak->detail_objek->alamat->kota))
                                    <br>
                                    @endif


                                    @if(!empty($objekPajak->detail_objek->alamat->kecamatan))
                                    Kecamatan: {{ $objekPajak->detail_objek->alamat->kecamatan }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->kelurahan))
                                    Kelurahan: {{ $objekPajak->detail_objek->alamat->kelurahan }}
                                    @endif
                                </p>
                            </div>
                        </div>
                        <hr>
                        @php
                            $bulan = [1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juni', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember']
                        @endphp
                        <p class="center-align">Masa Pajak <b>{{ $bulan[(int) $spt->periode->bulan] }} {{ $spt->periode->tahun }}</b></p>

                        <hr/>

                        @if ($objekPajak->jenis_pajak->kode == '4.1.1.02')
                            <div class="row">
                                <div class="col s12">
                                    <ul class="tabs" style="margin-bottom: 1.5rem">
                                        @foreach($spt->klasifikasi_pajak as $klasifikasiIndex => $i)
                                            <li class="tab col s3"><a href="javascript:;" class="tab-toggle" data-id="{{ $klasifikasiIndex }}">{{ $i->nama }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @foreach($spt->klasifikasi_pajak as $klasifikasiIndex => $i)
                                <div class="tab-content" data-id="{{ $klasifikasiIndex }}">
                                    @if (isset($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->data_per_hari))
                                        @foreach ($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->data_per_hari as $hariKe => $hari)
                                            <div class="row">
                                                <div class="col s12 m2">
                                                    <p class="center-align" style="padding-top: 1em">{{ $hariKe+1 }}/{{ $spt->periode->bulan }}/{{ $spt->periode->tahun }}</p>
                                                </div>
                                                <div class="col s12 m10">
                                                    <table>
                                                        <tr>
                                                            <td style="width: 40%;">Pendapatan dari Makanan/Minuman</td>
                                                            <td style="width: 5%;">:</td>
                                                            <td style="width: 50%";>Rp{{ number_format($hari->makanan, 2, ',', '.') }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 40%;">Pendapatan dari Service Charge</td>
                                                            <td style="width: 5%;">:</td>
                                                            <td style="width: 50%;">Rp{{ number_format($hari->service, 2, ',', '.') }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 40%;">Pendapatan Lain-lain</td>
                                                            <td style="width: 5%;">:</td>
                                                            <td style="width: 50%;">Rp{{ number_format($hari->lainnya, 2, ',', '.') }}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                    <div class="row">
                                        <div class="col s12 m2">
                                            <p class="center-align" style="padding-top: 1em">Total Satu Bulan</p>
                                        </div>
                                        <div class="col s12 m10">
                                            <table>
                                                <tr>
                                                    <td style="width: 40%;">Pendapatan dari Makanan/Minuman</td>
                                                    <td style="width: 5%;">:</td>
                                                    <td style="width: 50%;">Rp{{ number_format($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->makanan, 2, ',', '.') }}</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 40%;">Pendapatan dari Service Charge</td>
                                                    <td style="width: 5%;">:</td>
                                                    <td style="width: 50%;">Rp{{ number_format($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->service, 2, ',', '.') }}</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 40%;">Pendapatan Lain-lain</td>
                                                    <td style="width: 5%;">:</td>
                                                    <td style="width: 50%;">Rp{{ number_format($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->lainnya, 2, ',', '.') }}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @elseif ($objekPajak->jenis_pajak->kode == '4.1.1.11')
                            <div class="row">
                                <div class="col s12">
                                    <ul class="tabs" style="margin-bottom: 1.5rem">
                                        @foreach($spt->klasifikasi_pajak as $klasifikasiIndex => $i)
                                            <li class="tab col s3"><a href="javascript:;" class="tab-toggle" data-id="{{ $klasifikasiIndex }}">{{ $i->nama }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @foreach($spt->klasifikasi_pajak as $klasifikasiIndex => $i)
                                <div class="tab-content" data-id="{{ $klasifikasiIndex }}">
                                    @if (isset($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->data_per_hari))
                                        @foreach ($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->data_per_hari as $hariKe => $hari)
                                            <div class="row">
                                                <div class="col s12 m2">
                                                    <p class="center-align" style="padding-top: 1em">{{ $hariKe+1 }}/{{ $spt->periode->bulan }}/{{ $spt->periode->tahun }}</p>
                                                </div>
                                                <div class="col s12 m10">
                                                    <table>
                                                        <tr>
                                                            <td style="width: 40%;">Harga Material Perunit</td>
                                                            <td style="width: 5%;">:</td>
                                                            <td style="width: 50%";>Rp{{ number_format($hari->harga, 2, ',', '.') }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 40%;">Jumlah Penjualan Material</td>
                                                            <td style="width: 5%;">:</td>
                                                            <td style="width: 50%;">{{ $hari->penjualan }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 40%;">Jumlah Pendapatan</td>
                                                            <td style="width: 5%;">:</td>
                                                            <td style="width: 50%;">Rp{{ number_format($hari->harga * $hari->penjualan, 2, ',', '.') }}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                    <div class="row">
                                        <div class="col s12 m2">
                                            <p class="center-align" style="padding-top: 1em">Total Satu Bulan</p>
                                        </div>
                                        <div class="col s12 m10">
                                            <table>
                                                <tr>
                                                    <td style="width: 40%;">Harga Material Perunit</td>
                                                    <td style="width: 5%;">:</td>
                                                    <td style="width: 50%";>Rp{{ number_format($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->harga, 2, ',', '.') }}</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 40%;">Jumlah Penjualan Material</td>
                                                    <td style="width: 5%;">:</td>
                                                    <td style="width: 50%;">{{ $spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->penjualan }}</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 40%;">Jumlah Pendapatan</td>
                                                    <td style="width: 5%;">:</td>
                                                    <td style="width: 50%;">Rp{{ number_format(
                                                        $spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->harga
                                                        *
                                                        $spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->penjualan
                                                        , 2, ',', '.') }}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        <div class="row">
                            <div class="input-field col s12 l2">
                                <a class="btn red waves-effect waves-light" style="width:100%" href="{{ url('/pelaporan/' . $objekPajak->_id) }}"><i class="material-icons left">chevron_left</i>Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script src="{{ asset('assets/libs/cleavejs/cleave.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.tabs').tabs();
        $('.tabs .tab-toggle').click(function () {
            $('.tab-content').hide();
            $('.tab-content[data-id="'+$(this).data('id')+'"]').show();
        });
        $($('.tabs .tab-toggle')[0]).trigger('click');

        $('input.numeric').toArray().forEach(function (field) {
            $(field).data('cleave', new Cleave(field, {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralDecimalMark: ',',
                delimiter: '.'
            }));
        });

        $('#isidata-perhari').click(function () {
            $('input.daily').each(function (k, i) {
                $(i).prop('disabled', false);
                $(i).closest('.row-daily').show(500);
            });
            $('input.total').each(function (k, i) {
                $(i).prop('disabled', true);
                $(i).closest('.row-total').hide(500);
            });
            $('#isidata-perhari').addClass('lighten-2');
            $('#isidata-perhari').removeClass('darken-4');
            $('#isidata-perbulan').addClass('darken-4');
            $('#isidata-perbulan').removeClass('lighten-2');
        });
        $('#isidata-perbulan').click(function () {
            $('input.total').each(function (k, i) {
                $(i).prop('disabled', false);
                $(i).closest('.row-total').show(500);
            });
            $('input.daily').each(function (k, i) {
                $(i).prop('disabled', true);
                $(i).closest('.row-daily').hide(500);
            });
            $('#isidata-perbulan').addClass('lighten-2');
            $('#isidata-perbulan').removeClass('darken-4');
            $('#isidata-perhari').addClass('darken-4');
            $('#isidata-perhari').removeClass('lighten-2');
        });
        $(this).on('change', 'input[data-item="penjualan"]', function () {
            var klasifikasi = $(this).attr('data-klasifikasi');
            var penjualan = $(this).val().replace(/[.]/gm, "");
            var harga = $('input[data-item="harga"][data-klasifikasi="'+ klasifikasi +'"]').val().replace(/[.]/gm, "");
            var total = harga * penjualan;

            $('input[data-item="jumlah"][data-klasifikasi="'+ klasifikasi +'"]').val(total);
        });
        @if (isset($spt->data_pendapatan->{$spt->klasifikasi_pajak[0]->_id}->data_per_hari))
            $('#isidata-perhari').click();
        @else
            @if (isset($spt->data_pendapatan->{$spt->klasifikasi_pajak[0]->_id}->dpp))
                $('#isidata-perbulan').click();
            @else
                $('#isidata-perhari').click();
            @endif
        @endif

        @if ($objekPajak->jenis_pajak->kode == '4.1.1.11')
            $('#isidata-perbulan').click();
            $('#isidata-perhari').hide();
            $('#isidata-perbulan').hide();
        @endif
    });
</script>
@endsection
