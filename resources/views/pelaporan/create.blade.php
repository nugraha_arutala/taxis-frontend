@extends('skeleton')

@section('title', 'Buat SPTPD ' . ($objekPajak->detail_objek->nama))
@section('wrapper-title', 'Pembuatan SPTPD')

@section('content')
    <div class="container-fluid">
        @if ($errors->any())
        <div class="card red darken-4">
            <div class="card-content white-text">
                <span class="card-title">Submit Gagal</span>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title activator">{{ $objekPajak->jenis_pajak->nama }}</h5>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Nomor Objek Pajak Daerah</div>
                            <div class="col s12 m6 l8"><p>{{ $objekPajak->nop }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Nama Usaha</div>
                            <div class="col s12 m6 l8"><p>{{ $objekPajak->detail_objek->nama ?? '-' }}</p></div>
                        </div>
                        <form method="POST" action="{{ url('pendaftaran/spt/'.$objekPajak->_id) }}" class="h-form">
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="h-form-label col s12 m6 l4 font-bold m-right-align">Bulan</div>
                                    <div class="input-field col s12 m6 l8">
                                        <select id="bulan" name="bulan" class="browser-default">
                                            @php
                                                $bulan = [1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juni', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember'];
                                            @endphp
                                            @for($i = 1; $i <= 12; $i++)
                                                <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}" {{ date('n') == $i ? 'selected' : '' }}>{{ $bulan[$i] }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="h-form-label col s12 m6 l4 font-bold m-right-align">Tahun</div>
                                    <div class="input-field col s12 m6 l8">
                                        <select id="tahun" name="tahun" class="browser-default">
                                            @for($i = 2018; $i <= date('Y'); $i++)
                                                <option value="{{ $i }}" {{ date('Y') == $i ? 'selected' : '' }}>{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 l2">
                                        <a class="btn red waves-effect waves-light" style="width:100%" href="{{ url('/pelaporan/' . $objekPajak->_id) }}"><i class="material-icons left">chevron_left</i>Kembali</a>
                                    </div>
                                    <div class="input-field col s12 l2 push-l8">
                                        <button class="btn green waves-effect waves-light" style="width:100%" type="submit"><i class="material-icons left">add</i>Buat</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('stylesheet')
    <link href="{{ asset('css/pages/form-page.css') }}" rel="stylesheet">
@endsection

@section('javascript')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script>
        // Element init
        $(function () {
            $("select").not(".disabled").formSelect();
        });
    </script>
@endsection
