@extends('skeleton')

@section('title', 'Input SPTPD ' . ($objekPajak->detail_objek->nama))
@section('wrapper-title', 'Formulir Input SPTPD')

@section('content')
    <div class="container-fluid">
        @if ($errors->any())
        <div class="card red darken-4">
            <div class="card-content white-text">
                <span class="card-title">Submit Gagal</span>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title activator">{{ $objekPajak->jenis_pajak->nama }}</h5>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Nomor Objek Pajak Daerah</div>
                            <div class="col s12 m6 l8"><p>{{ $objekPajak->nop }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Nama Usaha</div>
                            <div class="col s12 m6 l8"><p>{{ $objekPajak->detail_objek->nama ?? '-' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Alamat</div>
                            <div class="col s12 m6 l8">
                                <p>
                                    @if(!empty($objekPajak->detail_objek->alamat->jalan))
                                        {{ $objekPajak->detail_objek->alamat->jalan }}<br>
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->rt))
                                    RT: {{ $objekPajak->detail_objek->alamat->rt }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->rw))
                                    RW: {{ $objekPajak->detail_objek->alamat->rw }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->kode_pos))
                                    Kode Pos: {{ $objekPajak->detail_objek->alamat->kode_pos }}
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->rt) || !empty($objekPajak->detail_objek->alamat->rw) || !empty($objekPajak->detail_objek->alamat->kode_pos))
                                    <br>
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->provinsi))
                                    Provinsi: {{ $objekPajak->detail_objek->alamat->provinsi }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->kota))
                                    Kabupaten/Kota: {{ $objekPajak->detail_objek->alamat->kota }}
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->provinsi) || !empty($objekPajak->detail_objek->alamat->kota))
                                    <br>
                                    @endif


                                    @if(!empty($objekPajak->detail_objek->alamat->kecamatan))
                                    Kecamatan: {{ $objekPajak->detail_objek->alamat->kecamatan }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->kelurahan))
                                    Kelurahan: {{ $objekPajak->detail_objek->alamat->kelurahan }}
                                    @endif
                                </p>
                            </div>
                        </div>
                        <hr>
                        @php
                            $bulan = [1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juni', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember']
                        @endphp
                        <p class="center-align">Masa Pajak <b>{{ $bulan[(int) $spt->periode->bulan] }} {{ $spt->periode->tahun }}</b></p>

                        <div class="row">
                            <div class="col s12 m6 l4 push-l2">
                                <a id="isidata-perhari" class="btn cyan waves-effect waves-light" style="width:100%"><i class="material-icons left">view_day</i>Isi Data Perhari</a>
                            </div>
                            <div class="col s12 m6 l4 push-l2">
                                <a id="isidata-perbulan" class="btn cyan waves-effect waves-light" style="width:100%"><i class="material-icons left">date_range</i>Isi Data Perbulan</a>
                            </div>
                        </div>

                        <br/>

                        <form method="POST" action="{{ url('pelaporan/'.$objekPajak->_id.'/form/'.$spt->_id) }}">
                            @csrf
                            <input type="hidden" name="_id" value="{{ $spt->_id }}">
                            @if ($objekPajak->jenis_pajak->kode == '4.1.1.02')
                                <div class="row">
                                    <div class="col s12">
                                        <ul class="tabs" style="margin-bottom: 1.5rem">
                                            @foreach($spt->klasifikasi_pajak as $klasifikasiIndex => $i)
                                                <input type="hidden" name="klasifikasi[{{ $klasifikasiIndex }}]" value="{{ $i->_id }}">
                                                <li class="tab col s3"><a href="javascript:;" class="tab-toggle" data-id="{{ $klasifikasiIndex }}">{{ $i->nama }}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                @foreach($spt->klasifikasi_pajak as $klasifikasiIndex => $i)
                                    <div class="tab-content" data-id="{{ $klasifikasiIndex }}">
                                        @php
                                            // days in month
                                            $days = date('t', mktime(0, 0, 0, (int) $spt->periode->bulan, 1, (int) $spt->periode->tahun));
                                        @endphp
                                        @for($day = 1; $day <= $days+1; $day++)
                                        @php
                                            if($day === $days+1) { $day = 'total'; }
                                        @endphp
                                        <div class="row row-{{ $day }} {{ $day === 'total' ? 'row-total' : 'row-daily' }}">
                                            @if(is_numeric($day))
                                                <div class="col s12 m2">
                                                    <p class="center-align" style="padding-top: 1em">Tanggal<br>{{ $day }}/{{ $spt->periode->bulan }}/{{ $spt->periode->tahun }}</p>
                                                </div>
                                            @else
                                                <div class="col s12 m2">
                                                    <p class="center-align" style="padding-top: 1em">Total Satu Bulan</p>
                                                </div>
                                            @endif
                                            <div class="col s12 m10">
                                                <div class="row">
                                                    <div class="input-field col s12 m4">
                                                        <input id="data_pendapatan_{{ $klasifikasiIndex }}_{{ $day }}_makanan"
                                                            data-item="makanan" data-klasifikasi="{{ $klasifikasiIndex }}" data-day="{{ $day }}"
                                                            class="numeric input-{{ $klasifikasiIndex }}-{{ $day }} {{ $day === 'total' ? 'total' : 'daily' }}"
                                                            type="text" name="data_pendapatan[{{ $klasifikasiIndex }}][{{ $day }}][makanan]"
                                                            value="{{ old('data_pendapatan') ? old("data_pendapatan.{$klasifikasiIndex}.{$day}.makanan") :
                                                                ($day !== 'total' ?
                                                                    (isset($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->data_per_hari) ?
                                                                        ($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->data_per_hari[$day-1]->makanan ?? '')
                                                                        :
                                                                        ''
                                                                    )
                                                                    :
                                                                    ($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->makanan ?? '')
                                                                )
                                                            }}"
                                                        >
                                                        <label for="data_pendapatan_{{ $klasifikasiIndex }}_{{ $day }}_makanan">Pendapatan dari Makanan dan Minuman</label>
                                                    </div>
                                                    <div class="input-field col s12 m4">
                                                        <input id="data_pendapatan_{{ $klasifikasiIndex }}_{{ $day }}_service"
                                                            data-item="service" data-klasifikasi="{{ $klasifikasiIndex }}" data-day="{{ $day }}"
                                                            class="numeric input-{{ $klasifikasiIndex }}-{{ $day }} {{ $day === 'total' ? 'total' : 'daily' }}"
                                                            type="text" name="data_pendapatan[{{ $klasifikasiIndex }}][{{ $day }}][service]"
                                                            value="{{ old('data_pendapatan') ? old("data_pendapatan.{$klasifikasiIndex}.{$day}.service") :
                                                                ($day !== 'total' ?
                                                                    (isset($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->data_per_hari) ?
                                                                        ($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->data_per_hari[$day-1]->service ?? '')
                                                                        :
                                                                        ''
                                                                    )
                                                                    :
                                                                    ($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->service ?? '')
                                                                )
                                                            }}"
                                                        >
                                                        <label for="data_pendapatan_{{ $klasifikasiIndex }}_{{ $day }}_service">Pendapatan dari Service Charge</label>
                                                    </div>
                                                    <div class="input-field col s12 m4">
                                                        <input id="data_pendapatan_{{ $klasifikasiIndex }}_{{ $day }}_lainnya"
                                                            data-item="lainnya" data-klasifikasi="{{ $klasifikasiIndex }}" data-day="{{ $day }}"
                                                            class="numeric input-{{ $klasifikasiIndex }}-{{ $day }} {{ $day === 'total' ? 'total' : 'daily' }}"
                                                            type="text" name="data_pendapatan[{{ $klasifikasiIndex }}][{{ $day }}][lainnya]"
                                                            value="{{ old('data_pendapatan') ? old("data_pendapatan.{$klasifikasiIndex}.{$day}.lainnya") :
                                                                ($day !== 'total' ?
                                                                    (isset($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->data_per_hari) ?
                                                                        ($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->data_per_hari[$day-1]->lainnya ?? '')
                                                                        :
                                                                        ''
                                                                    )
                                                                    :
                                                                    ($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->lainnya ?? '')
                                                                )
                                                            }}"
                                                        >
                                                        <label for="data_pendapatan_{{ $klasifikasiIndex }}_{{ $day }}_lainnya">Pendapatan Lain-lain</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @php
                                            if($day === 'total') { break; }
                                        @endphp
                                        @endfor
                                    </div>
                                @endforeach
                            @elseif ($objekPajak->jenis_pajak->kode == '4.1.1.11')
                                <div class="row">
                                    <div class="col s12">
                                        <ul class="tabs" style="margin-bottom: 1.5rem">
                                            @foreach($spt->klasifikasi_pajak as $klasifikasiIndex => $i)
                                                <input type="hidden" name="klasifikasi[{{ $klasifikasiIndex }}]" value="{{ $i->_id }}">
                                                <li class="tab col s3"><a href="javascript:;" class="tab-toggle" data-id="{{ $klasifikasiIndex }}">{{ $i->nama }}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                @foreach($spt->klasifikasi_pajak as $klasifikasiIndex => $i)
                                    <div class="tab-content" data-id="{{ $klasifikasiIndex }}">
                                        <div class="row row-total row-total">
                                            <div class="col s12 m2">
                                                <p class="center-align" style="padding-top: 1em">Total Satu Bulan</p>
                                            </div>

                                            <div class="col s12 m10">
                                                <div class="row">
                                                    <div class="input-field col s12 m4">
                                                        <input id="data_pendapatan_{{ $klasifikasiIndex }}_total_harga" readonly
                                                            data-item="harga" data-klasifikasi="{{ $klasifikasiIndex }}" data-day="total"
                                                            class="numeric input-{{ $klasifikasiIndex }}-total total"
                                                            type="text" name="data_pendapatan[{{ $klasifikasiIndex }}][total][harga]"
                                                            value="{{ str_replace('.', ',', old('data_pendapatan') ? old("{$i->harga}") : ($i->harga ?? '')) }}"
                                                        >
                                                        <label for="data_pendapatan_{{ $klasifikasiIndex }}_total_harga">Harga Material Perunit</label>
                                                    </div>
                                                    <div class="input-field col s12 m4">
                                                        <input id="data_pendapatan_{{ $klasifikasiIndex }}_total_penjualan"
                                                            data-item="penjualan" data-klasifikasi="{{ $klasifikasiIndex }}" data-day="total"
                                                            class="numeric input-{{ $klasifikasiIndex }}-total total"
                                                            type="text" name="data_pendapatan[{{ $klasifikasiIndex }}][total][penjualan]"
                                                            value="{{ old('data_pendapatan') ? old("data_pendapatan.{$klasifikasiIndex}.total.penjualan") :
                                                                ($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->penjualan ?? '')
                                                            }}"
                                                        >
                                                        <label for="data_pendapatan_{{ $klasifikasiIndex }}_total_penjualan">Jumlah Penjualan Material</label>
                                                    </div>
                                                    <div class="input-field col s12 m4">
                                                        <input id="data_pendapatan_{{ $klasifikasiIndex }}_total_jumlah" readonly
                                                            data-item="jumlah" data-klasifikasi="{{ $klasifikasiIndex }}" data-day="total"
                                                            class="numeric input-{{ $klasifikasiIndex }}-total total"
                                                            type="text" name="data_pendapatan[{{ $klasifikasiIndex }}][total][jumlah]"
                                                            value="{{ old('data_pendapatan') ? old("data_pendapatan.{$klasifikasiIndex}.total.jumlah") :
                                                                ($spt->data_pendapatan->{$spt->klasifikasi_pajak[$klasifikasiIndex]->_id}->total ?? '0')
                                                            }}"
                                                        >
                                                        <label for="data_pendapatan_{{ $klasifikasiIndex }}_total_jumlah">Jumlah Pendapatan</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            <div class="row">
                                <div class="input-field col s12 l2">
                                    <a class="btn red waves-effect waves-light" style="width:100%" href="{{ url('/pelaporan/' . $objekPajak->_id) }}"><i class="material-icons left">chevron_left</i>Kembali</a>
                                </div>
                                <div class="input-field col s12 l2 push-l8">
                                    <button class="btn green waves-effect waves-light" style="width:100%" type="submit"><i class="material-icons left">save</i>Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script src="{{ asset('assets/libs/cleavejs/cleave.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.tabs').tabs();
        $('.tabs .tab-toggle').click(function () {
            $('.tab-content').hide();
            $('.tab-content[data-id="'+$(this).data('id')+'"]').show();
        });
        $($('.tabs .tab-toggle')[0]).trigger('click');

        $('input.numeric').toArray().forEach(function (field) {
            $(field).data('cleave', new Cleave(field, {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand',
                numeralDecimalMark: ',',
                numeralDecimalScale: 2,
                delimiter: '.'
            }));
        });

        $('#isidata-perhari').click(function () {
            $('input.daily').each(function (k, i) {
                $(i).prop('disabled', false);
                $(i).closest('.row-daily').show(500);
            });
            $('input.total').each(function (k, i) {
                $(i).prop('disabled', true);
                $(i).closest('.row-total').hide(500);
            });
            $('#isidata-perhari').addClass('lighten-2');
            $('#isidata-perhari').removeClass('darken-4');
            $('#isidata-perbulan').addClass('darken-4');
            $('#isidata-perbulan').removeClass('lighten-2');
        });
        $('#isidata-perbulan').click(function () {
            $('input.total').each(function (k, i) {
                $(i).prop('disabled', false);
                $(i).closest('.row-total').show(500);
            });
            $('input.daily').each(function (k, i) {
                $(i).prop('disabled', true);
                $(i).closest('.row-daily').hide(500);
            });
            $('#isidata-perbulan').addClass('lighten-2');
            $('#isidata-perbulan').removeClass('darken-4');
            $('#isidata-perhari').addClass('darken-4');
            $('#isidata-perhari').removeClass('lighten-2');
        });
        $(this).on('change', 'input[data-item="penjualan"]', function () {
            var klasifikasi = $(this).attr('data-klasifikasi');
            var penjualan = $(this).data('cleave').getRawValue();
            var harga = $('input[data-item="harga"][data-klasifikasi="'+ klasifikasi +'"]').data('cleave').getRawValue();
            var total = harga * penjualan;

            $('input[data-item="jumlah"][data-klasifikasi="'+ klasifikasi +'"]').data('cleave').setRawValue(total);
        });
        @if (isset($spt->data_pendapatan->{$spt->klasifikasi_pajak[0]->_id}->data_per_hari))
            $('#isidata-perhari').click();
        @else
            @if (isset($spt->data_pendapatan->{$spt->klasifikasi_pajak[0]->_id}->dpp))
                $('#isidata-perbulan').click();
            @else
                $('#isidata-perhari').click();
            @endif
        @endif

        @if ($objekPajak->jenis_pajak->kode == '4.1.1.11')
            $('#isidata-perbulan').click();
            $('#isidata-perhari').hide();
            $('#isidata-perbulan').hide();
        @endif
    });
</script>
@endsection
