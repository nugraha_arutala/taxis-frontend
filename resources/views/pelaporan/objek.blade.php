@extends('skeleton')

@section('title', 'Pelaporan ' . ($objekPajak->detail_objek->nama))
@section('wrapper-title', 'Laporan SPTPD')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                @if (Session::has('message'))
                    @if(Session::get('message') === 'create-successful')
                    <div class="card green darken-2">
                        <div class="card-content white-text">
                            SPT baru berhasil dibuat.
                            @if(Session::has('spt'))
                                <a href="{{ url('/pelaporan/'.$objekPajak->_id.'/form/'.Session::get('spt')) }}" class="btn blue waves-effect waves-light">Klik di sini untuk isi data</a>.
                            @endif
                        </div>
                    </div>
                    @endif
                @endif
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title activator">Pajak Restoran</h5>
                        <div class="row">
                            <?php $wajibPajak = $objekPajak->subjek_pajak ?>
                            <div class="col s12 m6 l4 font-bold m-right-align">NPWPD</div>
                            <div class="col s12 m6 l8"><p>{{ $wajibPajak->npwpd ?? '-' }}</p></div>
                            <div class="col s12 m6 l4 font-bold m-right-align">Nama Wajib Pajak</div>
                            <div class="col s12 m6 l8"><p>{{ $wajibPajak->nama ?? '-' }}</p></div>
                            <div class="row">
                                <div class="col s12 m6 l4 font-bold m-right-align">Alamat</div>
                                <div class="col s12 m8 l8">
                                    <p>
                                        @if(!empty($wajibPajak->alamat->jalan))
                                            {{ $wajibPajak->alamat->jalan }}<br>
                                        @endif

                                        @if(!empty($wajibPajak->alamat->rt))
                                        RT: {{ $wajibPajak->alamat->rt }}
                                        @endif
                                        @if(!empty($wajibPajak->alamat->rw))
                                        RW: {{ $wajibPajak->alamat->rw }}
                                        @endif
                                        @if(!empty($wajibPajak->alamat->kode_pos))
                                        Kode Pos: {{ $wajibPajak->alamat->kode_pos }}
                                        @endif

                                        @if(!empty($wajibPajak->alamat->rt) || !empty($wajibPajak->alamat->rw) || !empty($wajibPajak->alamat->kode_pos))
                                        <br>
                                        @endif

                                        @if(!empty($wajibPajak->alamat->provinsi))
                                        Provinsi: {{ $wajibPajak->alamat->provinsi }}
                                        @endif
                                        @if(!empty($wajibPajak->alamat->kota))
                                        Kabupaten/Kota: {{ $wajibPajak->alamat->kota }}
                                        @endif

                                        @if(!empty($wajibPajak->alamat->provinsi) || !empty($wajibPajak->alamat->kota))
                                        <br>
                                        @endif


                                        @if(!empty($wajibPajak->alamat->kecamatan))
                                        Kecamatan: {{ $wajibPajak->alamat->kecamatan }}
                                        @endif
                                        @if(!empty($wajibPajak->alamat->kelurahan))
                                        Kelurahan: {{ $wajibPajak->alamat->kelurahan }}
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">NOPD</div>
                            <div class="col s12 m6 l8"><p>{{ $objekPajak->nop ?? '-' }}</p></div>
                            <div class="col s12 m6 l4 font-bold m-right-align">Nama Usaha</div>
                            <div class="col s12 m6 l8"><p>{{ $objekPajak->detail_objek->nama ?? '-' }}</p></div>
                            <div class="col s12 m6 l4 font-bold m-right-align">Alamat</div>
                            <div class="col s12 m8 l8">
                                <p>
                                    @if(!empty($objekPajak->detail_objek->alamat->jalan))
                                        {{ $objekPajak->detail_objek->alamat->jalan }}<br>
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->rt))
                                    RT: {{ $objekPajak->detail_objek->alamat->rt }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->rw))
                                    RW: {{ $objekPajak->detail_objek->alamat->rw }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->kode_pos))
                                    Kode Pos: {{ $objekPajak->detail_objek->alamat->kode_pos }}
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->rt) || !empty($objekPajak->detail_objek->alamat->rw) || !empty($objekPajak->detail_objek->alamat->kode_pos))
                                    <br>
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->provinsi))
                                    Provinsi: {{ $objekPajak->detail_objek->alamat->provinsi }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->kota))
                                    Kabupaten/Kota: {{ $objekPajak->detail_objek->alamat->kota }}
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->provinsi) || !empty($objekPajak->detail_objek->alamat->kota))
                                    <br>
                                    @endif


                                    @if(!empty($objekPajak->detail_objek->alamat->kecamatan))
                                    Kecamatan: {{ $objekPajak->detail_objek->alamat->kecamatan }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->kelurahan))
                                    Kelurahan: {{ $objekPajak->detail_objek->alamat->kelurahan }}
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <div class="right-align">
                            <a class="btn-floating btn-large waves-effect waves-light green" href="{{ url('/pendaftaran/spt/'.$objekPajak->_id) }}"><i class="material-icons">add</i></a>
                        </div>
                        <div class="row">
                            <table id="datatable" class="responsive-table display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NO SPTPD</th>
                                        <th>Masa Pajak</th>
                                        <th>Tahun Pajak</th>
                                        <th>Jumlah</th>
                                        <th>Status</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="99" style="text-align: center">Loading...</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hide">
        <!-- action button column for datatable -->
        <div id="action-button-column-sample-0">
            <a class="waves-effect waves-light btn-small edit" href="{{ url('/pelaporan/'.$objekPajak->_id.'/form/[id]') }}"><i class="material-icons">edit</i></a>
            <a class="waves-effect waves-light btn-small print" href="{{ url('/pelaporan/'.$objekPajak->_id.'/form/[id]/print') }}"><i class="material-icons">print</i></a>
        </div>
        <div id="action-button-column-sample-1">
            <a class="waves-effect waves-light btn-small view" href="{{ url('/pelaporan/'.$objekPajak->_id.'/detail/[id]') }}"><i class="material-icons">search</i></a>
            <a class="waves-effect waves-light btn-small print" href="{{ url('/pelaporan/'.$objekPajak->_id.'/form/[id]/print') }}"><i class="material-icons">print</i></a>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script>
        $('#datatable').DataTable({
            serverSide: true,
            ordering: false,
            searching: false,
            order: [],
            ajax: {
                url: '{{ url('/pelaporan/'.$objekPajak->_id.'/datatable') }}',
                type: 'GET'
            },
            columns: [
                {
                    data: function(row, type, set, meta) {
                        var pginfo = $(meta.settings.nTable).DataTable().page.info();
                        return (pginfo.page * pginfo.length) + (meta.row + 1);
                    },
                    className: 'center-align'
                },
                {
                    data: function(row, type, set, meta) {
                        return row.no_spt || '???';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        var bulan = {
                            1: 'Januari', 2: 'Februari', 3: 'Maret', 4: 'April',
                            5: 'Mei', 6: 'Juni', 7: 'Juli', 8: 'Agustus',
                            9: 'September', 10: 'Oktober', 11: 'November',
                            12: 'Desember'
                        };
                        return bulan[parseInt(row.periode.bulan)] || '???';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.periode.tahun || '???';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        if(row.jumlah_pajak) {
                            return row.jumlah_pajak.toLocaleString('id', {minimumFractionDigits: 2, maximumFractionDigits: 2}) || '-';
                        } else {
                            return '';
                        }
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        if(row.data_pendapatan === null) {
                            return 'Belum diisi';
                        } else {
                            return row.status === 1 ? 'Lunas' : 'Belum dibayar';
                        }
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        var div = $('#action-button-column-sample-' + row.status).clone();
                        var buttons = div.find('a');
                        for(i of buttons) {
                            $(i).prop('href', $(i).prop('href').replace('[id]', row._id));
                        }
                        // If not filled yet, remove print button
                        if(row.data_pendapatan === null) {
                            div.find('a.print').remove();
                        }
                        return div.html();
                    },
                    orderable: false
                }
            ],
            language: {
                emptyTable: "<center>Tidak ada data SPT ditemukan.</center>"
            }
        });
    </script>
@endsection
