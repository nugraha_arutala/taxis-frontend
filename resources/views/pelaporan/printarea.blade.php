<?php // SPTPD ?>

@extends('printableBase')

@section('kop-colspan')
colspan="2"
@endsection

@section('letter-body')
<tr>
    <td style="text-align: center; width: 50%; padding: 1em">
        <b>SURAT PEMBERITAHUAN PAJAK DAERAH (SPTPD)</b>
        <br/>
        <b>{{ strtoupper($spt->jenis_pajak->nama) }}</b>
        <br/>
        @php
            $bulan = [1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juni', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember']
        @endphp
        <table class="noborder" style="width: 70%; margin: auto">
            <tr>
                <td>Masa Pajak</td>
                <td>: {{ $bulan[(int) $spt->periode->bulan] }}</td>
            </tr>
            <tr>
                <td>Tahun Pajak</td>
                <td>: {{ $spt->periode->tahun }}</td>
            </tr>
        </table>
    </td>
    <td style="text-align: center; width: 50%; padding: 1em">
        <table class="noborder">
            <tr style="font-weight: bold">
                <td></td>
                <td>Kepada</td>
            </tr>
            <tr style="font-weight: bold">
                <td style="text-align: right">Yth.</td>
                <td>Dispenda Kabupaten Musi Rawas</td>
            </tr>
            <tr style="font-weight: bold">
                <td></td>
                <td>di</td>
            </tr>
            <tr style="font-weight: bold">
                <td></td>
                <td>Musi Rawas</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td colspan="2" style="padding: 0.5em 1em"><b>Perhatian</b></td>
</tr>
<tr>
    <td colspan="2" style="padding: 0.5em 1em">
        <ol style="margin-bottom: 0">
            <li>Harap diserahkan kembali kepada Suku Dinas Pendapatan Daerah Seksi Dinas Pendapatan Daerah Kecamatan di mana Wajib Pajak terdaftar, paling lambat tanggal 20 pada bulan berikutnya.</li>
            <li>Keterlambatan penyerahan SPTPD dikenakan sanksi sesuai ketentuan yang berlaku.</li>
        </ol>
    </td>
</tr>
<tr>
    <td colspan="2" style="padding: 0.5em 1em"><b>I. Identitas Wajib Pajak</b></td>
</tr>
<tr>
    <td colspan="2" style="padding: 0.5em 1em">
        <table class="noborder" style="width: 100%">
            <tr>
                <td style="width: 30%">a. Nama Wajib Pajak</td>
                <td>: {{ $spt->pembayar_pajak->nama ?? '?' }}</td>
            </tr>
            <tr>
                <td style="width: 30%">b. Alamat</td>
                <td>: @if(!empty($spt->pembayar_pajak->alamat->jalan))
                    {{ $spt->pembayar_pajak->alamat->jalan }}
                    @endif
                    @if(!empty($spt->pembayar_pajak->alamat->rt))
                    RT: {{ $spt->pembayar_pajak->alamat->rt }}
                    @endif
                    @if(!empty($spt->pembayar_pajak->alamat->rw))
                    RW: {{ $spt->pembayar_pajak->alamat->rw }}
                    @endif
                    @if(!empty($spt->pembayar_pajak->alamat->kode_pos))
                    Kode Pos: {{ $spt->pembayar_pajak->alamat->kode_pos }}
                    @endif

                    @if(!empty($spt->pembayar_pajak->alamat->rt) || !empty($spt->pembayar_pajak->alamat->rw) || !empty($spt->pembayar_pajak->alamat->kode_pos))
                    <br>&nbsp;
                    @endif

                    @if(!empty($spt->pembayar_pajak->alamat->provinsi))
                    Provinsi: {{ $spt->pembayar_pajak->alamat->provinsi }}
                    @endif
                    @if(!empty($spt->pembayar_pajak->alamat->kota))
                    Kabupaten/Kota: {{ $spt->pembayar_pajak->alamat->kota }}
                    @endif

                    @if(!empty($spt->pembayar_pajak->alamat->provinsi) || !empty($spt->pembayar_pajak->alamat->kota))
                    <br>&nbsp;
                    @endif


                    @if(!empty($spt->pembayar_pajak->alamat->kecamatan))
                    Kecamatan: {{ $spt->pembayar_pajak->alamat->kecamatan }}
                    @endif
                    @if(!empty($spt->pembayar_pajak->alamat->kelurahan))
                    Kelurahan: {{ $spt->pembayar_pajak->alamat->kelurahan }}
                    @endif
                </td>
            </tr>
            <tr>
                <td style="width: 30%">c. NPWPD</td>
                <td>: {{ $spt->pembayar_pajak->npwpd ?? '?' }}</td>
            </tr>
            <tr>
                <td style="width: 30%">d. NOPD</td>
                <td>: {{ $objekPajak->nop ?? '?' }}</td>
            </tr>
            <tr>
                <td style="width: 30%">e. Nama Usaha</td>
                <td>: {{ $objekPajak->detail_objek->nama ?? '?' }}</td>
            </tr>
            <tr>
                <td style="width: 30%">f. Alamat Usaha</td>
                <td>: @if(!empty($objekPajak->detail_objek->alamat->jalan))
                    {{ $objekPajak->detail_objek->alamat->jalan }}
                    @endif
                    @if(!empty($objekPajak->detail_objek->alamat->rt))
                    RT: {{ $objekPajak->detail_objek->alamat->rt }}
                    @endif
                    @if(!empty($objekPajak->detail_objek->alamat->rw))
                    RW: {{ $objekPajak->detail_objek->alamat->rw }}
                    @endif
                    @if(!empty($objekPajak->detail_objek->alamat->kode_pos))
                    Kode Pos: {{ $objekPajak->detail_objek->alamat->kode_pos }}
                    @endif

                    @if(!empty($objekPajak->detail_objek->alamat->rt) || !empty($objekPajak->detail_objek->alamat->rw) || !empty($objekPajak->detail_objek->alamat->kode_pos))
                    <br>&nbsp;
                    @endif

                    @if(!empty($objekPajak->detail_objek->alamat->provinsi))
                    Provinsi: {{ $objekPajak->detail_objek->alamat->provinsi }}
                    @endif
                    @if(!empty($objekPajak->detail_objek->alamat->kota))
                    Kabupaten/Kota: {{ $objekPajak->detail_objek->alamat->kota }}
                    @endif

                    @if(!empty($spt->pembayar_pajak->alamat->provinsi) || !empty($spt->pembayar_pajak->alamat->kota))
                    <br>&nbsp;
                    @endif


                    @if(!empty($spt->pembayar_pajak->alamat->kecamatan))
                    Kecamatan: {{ $spt->pembayar_pajak->alamat->kecamatan }}
                    @endif
                    @if(!empty($spt->pembayar_pajak->alamat->kelurahan))
                    Kelurahan: {{ $spt->pembayar_pajak->alamat->kelurahan }}
                    @endif
                </td>
            </tr>
        </table>
    </td>
</tr>
@if ($spt->jenis_pajak->kode == '4.1.1.02')
<tr>
    <td colspan="2" style="padding: 0.5em 1em"><b>II. Diisi oleh Pengusaha Restoran</b></td>
</tr>
<tr>
    <td style="border-right: 0px solid white">a. Sub Objek Pajak</td>
    <td style="border-left: 0px solid white; text-align: center">
        {{ implode(', ', array_map(function($item) { return $item->nama ?? null; }, $spt->klasifikasi_pajak)) }}
    </td>
</tr>
<tr>
    <td>b. Pendapatan dari Makanan dan Minuman</td>
    <td>Rp{{ number_format(array_sum(array_map(function($item) { return $item->makanan ?? 0; }, (array) $spt->data_pendapatan)), 2, ',', '.') }}</td>
</tr>
<tr>
    <td>c. Pendapatan dari Service Charge</td>
    <td>Rp{{ number_format(array_sum(array_map(function($item) { return $item->service ?? 0; }, (array) $spt->data_pendapatan)), 2, ',', '.') }}</td>
</tr>
<tr>
    <td>d. Pendapatan Lainnya</td>
    <td>Rp{{ number_format(array_sum(array_map(function($item) { return $item->lainnya ?? 0; }, (array) $spt->data_pendapatan)), 2, ',', '.') }}</td>
</tr>
<tr>
    <td>e. Dasar Pengenaan Pajak (DPP)</td>
    <td>Rp{{ number_format(array_sum(array_map(function($item) { return $item->dpp ?? 0; }, (array) $spt->data_pendapatan)), 2, ',', '.') }}</td>
</tr>
@php
    $pajak_terutang = array_sum(array_map(function($item) { return $item->pajak ?? 0; }, (array) $spt->data_pendapatan));
@endphp
<tr>
    <td>f. Pajak Terutang</td>
    <td>Rp{{ number_format($pajak_terutang, 2, ',', '.') }}</td>
</tr>
<tr>
    <td>g. Denda</td>
    <td>Rp{{ number_format(0, 2, ',', '.') }}</td> <?php // TODO ?>
</tr>
<tr>
    <td>h. Total Pajak yang harus dibayar</td>
    <td>Rp{{ number_format($pajak_terutang, 2, ',', '.') }}</td>
</tr>
<tr>
    <td style="border-right: 0px solid white">i. Data Pendukung</td>
    <td style="border-left: 0px solid white; text-align: center">Lampiran</td>
</tr>
<tr>
    <td style="padding: 1em 2em">
        <p style="margin-top: 0; margin-bottom: 0">a) Surat Setoran Pajak Daerah (SPD)</p>
        <p style="margin-top: 0; margin-bottom: 0">b) Rekapitulasi Penjualan/Omzet</p>
        <p style="margin-top: 0; margin-bottom: 0">c) Rekapitulasi Penggunaan Bon/Bill</p>
        <p style="margin-top: 0; margin-bottom: 0">d) ------</p>
    </td>
    <td style="padding: 1em 2em">
        <p style="margin-top: 0; margin-bottom: 0">Ada/Tidak Ada</p>
        <p style="margin-top: 0; margin-bottom: 0">Ada/Tidak Ada</p>
        <p style="margin-top: 0; margin-bottom: 0">Ada/Tidak Ada</p>
        <p style="margin-top: 0; margin-bottom: 0">Ada/Tidak Ada</p>
    </td>
</tr>
<tr>
    <td colspan="2" style="text-align: center; padding: 1em 2em">
        <p>
            Demikian formulir ini diisi dengan sebenar-benarnya dan apabila terdapat
            ketidakbenaran dalam memenuhi kewajiban pengisian SPTPD ini, saya bersedia
            dikenakan sanksi sesuai dengan Peraturan Daerah yang berlaku.
        </p>
        <br>
        <table class="noborder" style="width: 100%">
            <tr>
                <td style="width: 40%; text-align: center">
                    Diterima oleh petugas,<br/>
                    tanggal ______________
                    <br/><br/><br/><br/>
                    ______________________<br/>
                    NIP __________________
                </td>
                <td style="width: 60%; text-align: center">
                    Musi Rawas __________________,<br/>
                    Wajib Pajak/Penanggung Jawab Kuasa
                    <br/><br/><br/><br/>
                    ______________________<br/>
                    Nama/Cap/Stempel
                </td>
            </tr>
        </table>
    </td>
</tr>
@elseif ($spt->jenis_pajak->kode == '4.1.1.11')
<tr>
    <td colspan="2" style="padding: 0.5em 1em"><b>II. Diisi oleh Pengusaha Restoran</b></td>
</tr>
<tr>
    <td style="border-right: 0px solid white">a. Sub Objek Pajak</td>
    <td style="border-left: 0px solid white; text-align: center">
        {{ implode(', ', array_map(function($item) { return $item->nama ?? null; }, $spt->klasifikasi_pajak)) }}
    </td>
</tr>
<tr>
    <td>b. Harga Material Perunit</td>
    <?php // average ?>
    <td>Rp{{ number_format((array_sum(array_map(function($item) { return $item->harga ?? 0; }, (array) $spt->data_pendapatan))/count((array) $spt->data_pendapatan)), 2, ',', '.') }}</td>
</tr>
<tr>
    <td>c. Jumlah Penjualan Material</td>
    <td>{{ number_format(array_sum(array_map(function($item) { return $item->penjualan ?? 0; }, (array) $spt->data_pendapatan)), 0, ',', '.') }}</td>
</tr>

<tr>
    <td>e. Dasar Pengenaan Pajak (DPP)</td>
    <td>Rp{{ number_format(array_sum(array_map(function($item) { return $item->dpp ?? 0; }, (array) $spt->data_pendapatan)), 2, ',', '.') }}</td>
</tr>
@php
    $pajak_terutang = array_sum(array_map(function($item) { return $item->pajak ?? 0; }, (array) $spt->data_pendapatan));
@endphp
<tr>
    <td>f. Pajak Terutang</td>
    <td>Rp{{ number_format($pajak_terutang, 2, ',', '.') }}</td>
</tr>
<tr>
    <td>g. Denda</td>
    <td>Rp{{ number_format(0, 2, ',', '.') }}</td> <?php // TODO ?>
</tr>
<tr>
    <td>h. Total Pajak yang harus dibayar</td>
    <td>Rp{{ number_format($pajak_terutang, 2, ',', '.') }}</td>
</tr>
<tr>
    <td style="border-right: 0px solid white">i. Data Pendukung</td>
    <td style="border-left: 0px solid white; text-align: center">Lampiran</td>
</tr>
<tr>
    <td style="padding: 1em 2em">
        <p style="margin-top: 0; margin-bottom: 0">a) Surat Setoran Pajak Daerah (SPD)</p>
        <p style="margin-top: 0; margin-bottom: 0">b) Rekapitulasi Penjualan/Omzet</p>
        <p style="margin-top: 0; margin-bottom: 0">c) Rekapitulasi Penggunaan Bon/Bill</p>
        <p style="margin-top: 0; margin-bottom: 0">d) ------</p>
    </td>
    <td style="padding: 1em 2em">
        <p style="margin-top: 0; margin-bottom: 0">Ada/Tidak Ada</p>
        <p style="margin-top: 0; margin-bottom: 0">Ada/Tidak Ada</p>
        <p style="margin-top: 0; margin-bottom: 0">Ada/Tidak Ada</p>
        <p style="margin-top: 0; margin-bottom: 0">Ada/Tidak Ada</p>
    </td>
</tr>
<tr>
    <td colspan="2" style="text-align: center; padding: 1em 2em">
        <p>
            Demikian formulir ini diisi dengan sebenar-benarnya dan apabila terdapat
            ketidakbenaran dalam memenuhi kewajiban pengisian SPTPD ini, saya bersedia
            dikenakan sanksi sesuai dengan Peraturan Daerah yang berlaku.
        </p>
        <br>
        <table class="noborder" style="width: 100%">
            <tr>
                <td style="width: 40%; text-align: center">
                    Diterima oleh petugas,<br/>
                    tanggal ______________
                    <br/><br/><br/><br/>
                    ______________________<br/>
                    NIP __________________
                </td>
                <td style="width: 60%; text-align: center">
                    Musi Rawas __________________,<br/>
                    Wajib Pajak/Penanggung Jawab Kuasa
                    <br/><br/><br/><br/>
                    ______________________<br/>
                    Nama/Cap/Stempel
                </td>
            </tr>
        </table>
    </td>
</tr>
@endif
<tr>
    <td colspan="2" style="text-align: right; padding: 1em 2em" class="barcode">
        {!! DNS1D::getBarcodeHTML($spt->no_spt, "C93") !!}
    </td>
</tr>
@endsection
