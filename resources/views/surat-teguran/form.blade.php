@extends('skeleton')

@section('title', 'Unggah Surat Teguran')
@section('wrapper-title', 'Unggah Surat Teguran')

@section('content')
    <div class="container-fluid">
        @if ($errors->any())
        <div class="card red darken-4">
            <div class="card-content white-text">
                <span class="card-title">Submit Gagal</span>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col s12">
                <form method="POST" action="{{ url('pendaftaran/surat-teguran') }}" class="h-form" enctype="multipart/form-data">
                    @csrf
                    @if (isset($suratTeguran))
                    <input type="hidden" name="_id" value="{{ $suratTeguran->_id }}">
                    @endif
                    <div class="card">
                        <div class="card-content">
                            <h5 class="card-title activator">Data Objek Pajak</h5>
                            <div class="form-body">
                                <div class="row">
                                    <div class="h-form-label col s12 m6 l3 font-bold s-flex-start m-right-align">NPWPD</div>
                                    <div class="input-field col s12 m6 l9">
                                        <input type="text" name="npwpd" id="npwpd" value="{{ old('npwpd') ?? $suratTeguran->objek_pajak->subjek_pajak->npwpd ?? "" }}">
                                        <span class="helper-text hide red-text" id="npwpd-not-found-helper">NPWPD tidak ditemukan.</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="h-form-label col s12 m6 l3 font-bold s-flex-start m-right-align">NOPD</div>
                                    <div class="input-field col s12 m6 l9">
                                        <select name="objek_pajak" id="nopd" class="browser-default">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="card-content">
                            <div class="form-body">
                                <div class="row">
                                    <div class="h-form-label col s12 m6 l3 font-bold s-flex-start m-right-align">Unggah Surat Teguran</div>
                                    <div class="input-field col s12 m6 l9">
                                        <div class="file-field input-field">
                                            <div class="btn blue darken-1">
                                                <span>File</span>
                                                <input type="file" name="berkas_surat_teguran">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="h-form-label col s12 m6 l3 font-bold s-flex-start m-right-align">No. Surat Teguran</div>
                                    <div class="input-field col s12 m6 l9">
                                        <input type="text" name="no_surat_teguran" value="{{ old('no_surat_teguran') ?? $suratTeguran->no_surat_teguran ?? "" }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="h-form-label col s12 m6 l3 font-bold s-flex-start m-right-align">Masa Pajak</div>
                                </div>
                                <div class="row">
                                    <div class="h-form-label col s12 m6 l3 font-bold s-flex-start m-right-align">Bulan</div>
                                    <div class="input-field col s12 m6 l9">
                                        <select id="bulan" name="bulan" class="browser-default">
                                            @php
                                                $bulan = [1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juni', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember'];
                                            @endphp
                                            @for($i = 1; $i <= 12; $i++)
                                                <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}" {{ (old('bulan') ?? $suratTeguran->periode->bulan ?? (date('n') - 1)) == $i ? 'selected' : '' }}>{{ $bulan[$i] }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="h-form-label col s12 m6 l3 font-bold s-flex-start m-right-align">Tahun</div>
                                    <div class="input-field col s12 m6 l9">
                                        <select id="tahun" name="tahun" class="browser-default">
                                            @for($i = 2018; $i <= date('Y'); $i++)
                                                <option value="{{ $i }}" {{ (old('tahun') ?? $suratTeguran->periode->tahun ?? date('Y')) == $i ? 'selected' : '' }}>{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12 l2">
                                        <a class="btn red waves-effect waves-light" style="width:100%" href="{{ url('/surat-teguran') }}"><i class="material-icons left">chevron_left</i>Kembali</a>
                                    </div>
                                    <div class="input-field col s12 l2 push-l8">
                                        <button class="btn green waves-effect waves-light" style="width:100%" type="submit"><i class="material-icons left">save</i>Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('stylesheet')
    <link href="{{ asset('css/pages/form-page.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/libs/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form-fix.css') }}" rel="stylesheet">
@endsection

@section('javascript')
    <script src="{{ asset('assets/libs/sweetalert2/dist/sweetalert2.min.js') }}"></sc
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script>
        // fetch wajib pajak
        var findWPbyNPWPD = function () {
            if(!$('input#npwpd').val()) { return; }

            $('span#npwpd-not-found-helper').addClass('hide');
            $('select#nopd option').remove();

            $.ajax({
                url: @json(url('/ajax/wajib-pajak')),
                method: 'GET',
                data: {
                    npwpd: $('input#npwpd').val()
                }
            })
            .done(function (data) {
                // fetch OP list
                $.ajax({
                    url: @json(url('/ajax/objek-pajak/list')),
                    method: 'GET',
                    data: {
                        subjek: data._id
                    }
                })
                .done(function (data) {
                    // add to select
                    $.each(data, function(index, i) {
                        var option = $('<option></option>');
                        option.prop('value', i._id);
                        var nama = i.nopd;
                        if(i.nama) {
                            nama += ' - ' + i.nama;
                        }
                        option.html(nama);
                        @if (old('objek_pajak'))
                            if(i._id === @json(old('objek_pajak'))) {
                                option.prop('selected', true);
                            }
                        @endif
                        @if (!empty($suratTeguran->objek_pajak))
                            if(i._id === @json($suratTeguran->objek_pajak->_id)) {
                                option.prop('selected', true);
                            }
                        @endif
                        option.appendTo('select#nopd');
                    });
                })
                .fail(function (error) {
                    if(error.status == 404) {
                    } else {
                        swal({
                            type: 'error',
                            title: 'Error',
                            text: 'Sedang ada masalah dengan server. Mohon coba lagi nanti.'
                        });
                    }
                })
            })
            .fail(function (error) {
                if(error.status == 404) {
                    $('span#npwpd-not-found-helper').removeClass('hide');
                } else {
                    swal({
                        type: 'error',
                        title: 'Error',
                        text: 'Sedang ada masalah dengan server. Mohon coba lagi nanti.'
                    });
                }
            })
        };

        // set trigger
        var typingTimer;
        $('input#npwpd').on('keydown', function () {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(findWPbyNPWPD, 1000);
        });
        $('input#npwpd').on('keyup', function () {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(findWPbyNPWPD, 1000);
        });
        $('input#npwpd').on('focusout', function () {
            clearTimeout(typingTimer);
            findWPbyNPWPD();
        });
        // trigger it on page load if text is not empty
        if($('input#npwpd').val()) {
            findWPbyNPWPD();
        }
    </script>
@endsection
