@extends('skeleton')

@section('title', 'Database Surat Teguran')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                @if (Session::has('message'))
                    @if(Session::get('message') === 'save-successful')
                    <div class="card green darken-2">
                        <div class="card-content white-text">
                            Data baru berhasil disimpan.
                        </div>
                    </div>
                    @endif
                @endif
                <div class="card">
                    <div class="card-content">
                        <div class="right-align">
                            <a class="btn-floating btn-large waves-effect waves-light green" href="{{ url('/pendaftaran/surat-teguran') }}"><i class="material-icons">add</i></a>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <div class="row">
                                    <div class="input-field col s12 m8 l6">
                                        <input id="search_nopd" type="text">
                                        <label for="search_nopd">Cari NOPD</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table id="datatable" class="responsive-table display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Masa Pajak</th>
                                    <th>NOPD</th>
                                    <th>NPWPD</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="99" style="text-align: center">Loading...</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hide">
        <!-- action button column for datatable -->
        <div id="action-button-column-sample">
            <a class="waves-effect waves-light btn-small view" href="{{ url('/surat-teguran/[id]/view') }}"><i class="material-icons">search</i></a>
            <a class="waves-effect waves-light btn-small view" href="{{ url('/surat-teguran/[id]/edit') }}"><i class="material-icons">edit</i></a>
        </div>
    </div>
@endsection

@section('stylesheet')
    <link href="{{ asset('css/datatable-fix.css') }}" rel="stylesheet">
@endsection
@section('javascript')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script>
        // Element init
        $(function () {
            $("select").not(".disabled").formSelect();
        });

        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            lengthChange: false,
            ordering: false,
            order: [],
            searching: true,
            ajax: {
                url: '{{ url('/surat-teguran/datatable') }}',
                type: 'GET'
            },
            columns: [
                {
                    data: function(row, type, set, meta) {
                        var pginfo = $(meta.settings.nTable).DataTable().page.info();
                        return (pginfo.page * pginfo.length) + (meta.row + 1);
                    },
                    className: 'center-align'
                },
                {
                    data: function(row, type, set, meta) {
                        var date = new Date(row.created_at);
                        if(date) {
                            var day = date.getDate();
                            var month = date.getMonth() + 1;
                            var year = date.getFullYear();

                            return day + '/' + month + '/' + year;
                        } else {
                            return "-";
                        }
                        return row.created_at || '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juni', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                        return row.periode ? ( bulan[parseInt(row.periode.bulan) - 1] + ' ' + row.periode.tahun ) : '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.objek_pajak.nop || '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.objek_pajak.subjek_pajak.npwpd || '???';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        var div = $('#action-button-column-sample').clone();
                        var buttons = div.find('a');
                        for(i of buttons) {
                            $(i).prop('href', $(i).prop('href').replace('[id]', row._id));
                        }
                        return div.html();
                    },
                    orderable: false
                }
            ]
        });

        // manual search
        $('#datatable_filter').remove();

        $('#search_status').val('');
        $('#search_jenisPajak').val('');

        $('#search_status').on('change', function (e) {
            $('#datatable').DataTable().columns(5).search($('#search_status').val()).draw();
        })
        $('#search_jenisPajak').on('change', function (e) {
            $('#datatable').DataTable().columns(4).search($('#search_jenisPajak').val()).draw();
        })
    </script>
@endsection
