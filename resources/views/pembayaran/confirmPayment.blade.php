@extends('skeleton')

@section('title', 'Konfirmasi Pembayaran Pajak ' . ($objekPajak->detail_objek->nama))
@section('wrapper-title', 'Konfirmasi Pembayaran ' . ($objekPajak->jenis_pajak->nama))

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <p>Anda akan melakukan konfirmasi pembayaran untuk {{ $objekPajak->jenis_pajak->nama }} dengan <b>NOPD {{ $objekPajak->nop ?? '???' }}</b></p>
                        <p>Jumlah yang dibayarkan: <b>Rp{{ number_format($spt->jumlah_pajak, 2, ',', '.') }}</b></p>
                        <?php $bulan = [1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juni', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember'] ?>
                        <p>Masa Pajak: <b>{{ $bulan[(int) $spt->periode->bulan] }} {{ $spt->periode->tahun }}</b></p>
                        <div class="row">
                            <div class="input-field col s12 l2 push-l8">
                                <button class="btn green waves-effect waves-light" id="submitConfirmation" style="width:100%" type="submit"><i class="material-icons left">check</i>Konfirmasi</button>
                            </div>
                            <div class="input-field col s12 l2 push-l8">
                                <a class="btn red waves-effect waves-light" style="width:100%" href="{{ url('/pembayaran/' . $objekPajak->_id . '/detail/' . $spt->_id) }}"><i class="material-icons left">close</i>Batal</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form id="form" method="POST" action="{{ url('/pembayaran/' . $objekPajak->_id . '/bayar/' . $spt->_id) }}">
            @csrf
            <input type="hidden" name="pembayaran" value="{{ $spt->_id }}">
            <input type="hidden" name="konfirmasi" value="1">
        </form>
    </div>
@endsection

@section('javascript')
<script>
    $(function() {
        $('#submitConfirmation').click(function (e) {
            e.preventDefault();
            $('#form').submit();
        })
    })
</script>
@endsection
