@extends('skeleton')

@section('title', 'Detail Pembayaran Pajak ' . ($objekPajak->detail_objek->nama))
@section('wrapper-title', 'Setoran')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title activator">{{ $objekPajak->jenis_pajak->nama }}</h5>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">NPWPD</div>
                            <div class="col s12 m6 l8"><p>{{ $wajibPajak->npwpd ?? '?' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Nama Wajib Pajak</div>
                            <div class="col s12 m6 l8"><p>{{ $wajibPajak->nama ?? '?' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Alamat</div>
                            <div class="col s12 m6 l8">
                                <p>
                                    @if(!empty($wajibPajak->alamat->jalan))
                                        {{ $wajibPajak->alamat->jalan }}<br>
                                    @endif

                                    @if(!empty($wajibPajak->alamat->rt))
                                    RT: {{ $wajibPajak->alamat->rt }}
                                    @endif
                                    @if(!empty($wajibPajak->alamat->rw))
                                    RW: {{ $wajibPajak->alamat->rw }}
                                    @endif
                                    @if(!empty($wajibPajak->alamat->kode_pos))
                                    Kode Pos: {{ $wajibPajak->alamat->kode_pos }}
                                    @endif

                                    @if(!empty($wajibPajak->alamat->rt) || !empty($wajibPajak->alamat->rw) || !empty($wajibPajak->alamat->kode_pos))
                                    <br>
                                    @endif

                                    @if(!empty($wajibPajak->alamat->provinsi))
                                    Provinsi: {{ $wajibPajak->alamat->provinsi }}
                                    @endif
                                    @if(!empty($wajibPajak->alamat->kota))
                                    Kabupaten/Kota: {{ $wajibPajak->alamat->kota }}
                                    @endif

                                    @if(!empty($wajibPajak->alamat->provinsi) || !empty($wajibPajak->alamat->kota))
                                    <br>
                                    @endif


                                    @if(!empty($wajibPajak->alamat->kecamatan))
                                    Kecamatan: {{ $wajibPajak->alamat->kecamatan }}
                                    @endif
                                    @if(!empty($wajibPajak->alamat->kelurahan))
                                    Kelurahan: {{ $wajibPajak->alamat->kelurahan }}
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Nomor Objek Pajak Daerah</div>
                            <div class="col s12 m6 l8"><p>{{ $objekPajak->nop }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Nama Usaha</div>
                            <div class="col s12 m6 l8"><p>{{ $objekPajak->detail_objek->nama ?? '-' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Jumlah yang harus dibayar</div>
                            <div class="col s12 m6 l8"><p>Rp{{ number_format($spt->jumlah_pajak, 2, ',', '.') }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Masa Pajak</div>
                            <?php $bulan = [1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juni', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember'] ?>
                            <div class="col s12 m6 l8">{{ $bulan[(int) $spt->periode->bulan] }} {{ $spt->periode->tahun }}</div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col s12 m10 push-m1 center-align" style="font-weight: bold; text-transform: uppercase">
                                <p>Kode Bayar: XXXXX.XXXXX.XXXXX <?php // TODO ?></p>
                            </div>
                        </div>
                        <br style="margin-top: 2em"/>
                        <div class="row">
                            <div class="input-field col s12 l2">
                                <a class="btn red waves-effect waves-light" style="width:100%" href="{{ url('/pembayaran/' . $objekPajak->_id) }}"><i class="material-icons left">chevron_left</i>Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
