@extends('printableBase')

<?php
function terbilang($x) {
    $angka = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"];

    if ($x < 12)
    return " " . $angka[$x];
    elseif ($x < 20)
    return terbilang($x - 10) . " belas";
    elseif ($x < 100)
    return terbilang($x / 10) . " puluh" . terbilang($x % 10);
    elseif ($x < 200)
    return "seratus" . terbilang($x - 100);
    elseif ($x < 1000)
    return terbilang($x / 100) . " ratus" . terbilang($x % 100);
    elseif ($x < 2000)
    return "seribu" . terbilang($x - 1000);
    elseif ($x < 1000000)
    return terbilang($x / 1000) . " ribu" . terbilang($x % 1000);
    elseif ($x < 1000000000)
    return terbilang($x / 1000000) . " juta" . terbilang($x % 1000000);
}
?>

@section('letter-body')
<tr>
    <td style="font-weight: bold; text-align: center">INVOICE PEMBAYARAN ELEKTRONIK</td>
</tr>
<tr>
    <td>
        <div style="padding: 2em">
            <table class="noborder" style="width: 100%">
                <tr>
                    <td>NPWPD</td>
                    <td>: {{ $wajibPajak->npwpd ?? '?' }}</td>
                </tr>
                <tr>
                    <td>Nama Wajib Pajak</td>
                    <td>: {{ $wajibPajak->nama ?? '?' }}</td>
                </tr>
                <tr>
                    <td>NIK/paspor</td>
                    <td>: @if(!empty($wajibPajak->nik) && !empty($wajibPajak->paspor))
                    {{ $wajibPajak->nik }} / {{ $wajibPajak->paspor }}
                    @elseif(!empty($wajibPajak->nik) || !empty($wajibPajak->paspor))
                    {{ $wajibPajak->nik }}{{ $wajibPajak->paspor }}
                    @else
                    -
                    @endif</td>
                </tr>
                <tr>
                    <td>Nomor Objek Pajak</td>
                    <td>: {{ $objekPajak->nop ?? '?' }}</td>
                </tr>
                <tr>
                    <td>Nama Usaha</td>
                    <td>: {{ $objekPajak->detail_objek->nama ?? '?' }}</td>
                </tr>
                <tr>
                    <td>Alamat Usaha</td>
                    <td><p>:
                        @if(!empty($objekPajak->detail_objek->alamat->jalan))
                            {{ $objekPajak->detail_objek->alamat->jalan }}<br>&nbsp;
                        @endif

                        @if(!empty($objekPajak->detail_objek->alamat->rt))
                        RT: {{ $objekPajak->detail_objek->alamat->rt }}
                        @endif
                        @if(!empty($objekPajak->detail_objek->alamat->rw))
                        RW: {{ $objekPajak->detail_objek->alamat->rw }}
                        @endif
                        @if(!empty($objekPajak->detail_objek->alamat->kode_pos))
                        Kode Pos: {{ $objekPajak->detail_objek->alamat->kode_pos }}
                        @endif

                        @if(!empty($objekPajak->detail_objek->alamat->rt) || !empty($objekPajak->detail_objek->alamat->rw) || !empty($objekPajak->detail_objek->alamat->kode_pos))
                        <br>&nbsp;
                        @endif

                        @if(!empty($objekPajak->detail_objek->alamat->provinsi))
                        Provinsi: {{ $objekPajak->detail_objek->alamat->provinsi }}
                        @endif
                        @if(!empty($objekPajak->detail_objek->alamat->kota))
                        Kabupaten/Kota: {{ $objekPajak->detail_objek->alamat->kota }}
                        @endif

                        @if(!empty($objekPajak->detail_objek->alamat->provinsi) || !empty($objekPajak->detail_objek->alamat->kota))
                        <br>&nbsp;
                        @endif


                        @if(!empty($objekPajak->detail_objek->alamat->kecamatan))
                        Kecamatan: {{ $objekPajak->detail_objek->alamat->kecamatan }}
                        @endif
                        @if(!empty($objekPajak->detail_objek->alamat->kelurahan))
                        Kelurahan: {{ $objekPajak->detail_objek->alamat->kelurahan }}
                        @endif
                    </p></td>
                </tr>
                <?php $bulan = [1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juni', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember'] ?>
                <tr>
                    <td>Masa Pajak</td>
                    <td>: {{ $bulan[(int) $spt->periode->bulan] }} {{ $spt->periode->tahun }}</td>
                </tr>
                <?php $tanggal = isset($spt->data_pembayaran->tanggal_pembayaran) ? date("d/m/Y", strtotime($spt->data_pembayaran->tanggal_pembayaran)) : '?'; ?>
                <tr>
                    <td>Tanggal Pembayaran</td>
                    <td>: {{ $tanggal }}</td>
                </tr>
            </table>
            <br><br>
            <table class="bordered" style="width: 100%; margin-bottom: 1em">
                <thead style="font-weight: bold">
                    <tr>
                        <td style="text-align: center">No</td>
                        <td>Jenis Pajak</td>
                        <td style="text-align: center">Uraian</td>
                        <td style="text-align: center">Jumlah</td>
                    </tr>
                </thead>
                @if ($objekPajak->jenis_pajak->kode === '4.1.1.02')
                <tr>
                    <td style="text-align: center">1</td>
                    <td><b>Pajak Restoran</b></td>
                    <td style="text-align: center">Pokok</td>
                    <td style="text-align: center">{{ number_format($spt->jumlah_pajak, 2, ',', '.') }}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="border: 0 solid white">
                    <td colspan="2" style="border: 0"></td>
                    <td style="text-align: center"><b>TOTAL</b></td>
                    <td style="text-align: center"><b>{{ number_format($spt->jumlah_pajak, 2, ',', '.') }}</b></td>
                </tr>
                @endif
            </table>
            <div style="text-align: center; font-weight: bold; margin-bottom: 2em">Terbilang: <span style="text-transform: capitalize">{{ terbilang($spt->jumlah_pajak) }}</span> Rupiah</div>
        </div>
    </td>
</tr>
@endsection
