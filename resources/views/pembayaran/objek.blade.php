@extends('skeleton')

@section('title', 'Pembayaran Pajak ' . ($objekPajak->detail_objek->nama))
@section('wrapper-title', 'Pembayaran ' . ($objekPajak->jenis_pajak->nama))

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                @if (Session::has('message'))
                    @if(Session::get('message') === 'confirm-failed')
                    <div class="card green darken-2">
                        <div class="card-content white-text">
                            Gagal mengkonfirmasi pembayaran. Silakan coba lagi.
                        </div>
                    </div>
                    @endif
                    @if(Session::get('message') === 'set-failed')
                    <div class="card green darken-2">
                        <div class="card-content white-text">
                            Gagal memproses metode pembayaran. Silakan coba lagi.
                        </div>
                    </div>
                    @endif
                @endif
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">NOPD</div>
                            <div class="col s12 m8 l4"><p>{{ $objekPajak->nop ?? '-' }}</p></div>
                            <div class="col s12 m4 l2 font-bold">Nama Usaha</div>
                            <div class="col s12 m8 l4"><p>{{ $objekPajak->detail_objek->nama ?? '-' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">NPWPD</div>
                            <div class="col s12 m8 l4"><p>{{ $objekPajak->subjek_pajak->npwpd ?? '-' }}</p></div>
                            <div class="col s12 m4 l2 font-bold">Nama Wajib Pajak</div>
                            <div class="col s12 m8 l4"><p>{{ $objekPajak->subjek_pajak->nama ?? '-' }}</p></div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <table id="datatable" class="responsive-table display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Masa Pajak</th>
                                        <th>Tahun Pajak</th>
                                        <th>Jumlah</th>
                                        <th>Status</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="99" style="text-align: center">Loading...</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hide">
        <!-- action button column for datatable -->
        @if($pembayar)
            <div id="action-button-column-sample-0">
                <a class="waves-effect waves-light btn-small view" href="{{ url('/pembayaran/'.$objekPajak->_id.'/bayar/[id]') }}"><i class="material-icons left">attach_money</i>Bayar</a>
            </div>
            <div id="action-button-column-sample-1">
                <a class="waves-effect waves-light btn-small view" href="{{ url('/pembayaran/'.$objekPajak->_id.'/detail/[id]') }}"><i class="material-icons">search</i></a>
            </div>
            <div id="action-button-column-sample-2">
                <a class="waves-effect waves-light btn-small view" href="{{ url('/pelaporan/'.$objekPajak->_id.'/form/[id]') }}"><i class="material-icons left">edit</i>Pelaporan</a>
            </div>
        @else
            <div id="action-button-column-sample">
                <a class="waves-effect waves-light btn-small view" href="{{ url('/pembayaran/'.$objekPajak->_id.'/detail/[id]') }}"><i class="material-icons">search</i></a>
            </div>
        @endif
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script>
        $('#datatable').DataTable({
            serverSide: true,
            ordering: false,
            searching: false,
            order: [],
            ajax: {
                url: '{{ url('/pembayaran/'.$objekPajak->_id.'/datatable') }}',
                type: 'GET'
            },
            columns: [
                {
                    data: function(row, type, set, meta) {
                        var pginfo = $(meta.settings.nTable).DataTable().page.info();
                        return (pginfo.page * pginfo.length) + (meta.row + 1);
                    },
                    className: 'center-align'
                },
                {
                    data: function(row, type, set, meta) {
                        var bulan = {
                            1: 'Januari', 2: 'Februari', 3: 'Maret', 4: 'April',
                            5: 'Mei', 6: 'Juni', 7: 'Juli', 8: 'Agustus',
                            9: 'September', 10: 'Oktober', 11: 'November',
                            12: 'Desember'
                        };
                        return bulan[parseInt(row.periode.bulan)] || '???';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.periode.tahun || '???';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        if(row.jumlah_pajak) {
                            return row.jumlah_pajak.toLocaleString('id', {minimumFractionDigits: 2, maximumFractionDigits: 2}) || '-';
                        } else {
                            return '';
                        }
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.status === 1 ? 'Lunas' : 'Belum dibayar';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        @if($pembayar)
                        var status = row.status;
                        if(!row.jumlah_pajak) { status = 2; }
                        var div = $('#action-button-column-sample-' + status).clone();
                        @else
                        if(!row.jumlah_pajak) { return 'Belum Pelaporan'; }
                        var div = $('#action-button-column-sample').clone();
                        @endif
                        var buttons = div.find('a');
                        for(i of buttons) {
                            $(i).prop('href', $(i).prop('href').replace('[id]', row._id));
                        }
                        return div.html();
                    },
                    orderable: false
                }
            ],
            language: {
                emptyTable: "<center>Tidak ada data SPT ditemukan.</center>"
            }
        });
    </script>
@endsection
