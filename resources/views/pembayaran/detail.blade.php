@extends('skeleton')

@section('title', 'Detail Pembayaran Pajak ' . ($objekPajak->detail_objek->nama))
@section('wrapper-title', 'Setoran')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title activator">{{ $objekPajak->jenis_pajak->nama }}</h5>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">NPWPD</div>
                            <div class="col s12 m6 l8"><p>{{ $wajibPajak->npwpd ?? '?' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">NOPD</div>
                            <div class="col s12 m6 l8"><p>{{ $objekPajak->nop }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Nama Usaha</div>
                            <div class="col s12 m6 l8"><p>{{ $objekPajak->detail_objek->nama ?? '-' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Alamat</div>
                            <div class="col s12 m6 l8">
                                <p>
                                    @if(!empty($objekPajak->detail_objek->alamat->jalan))
                                        {{ $objekPajak->detail_objek->alamat->jalan }}<br>
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->rt))
                                    RT: {{ $objekPajak->detail_objek->alamat->rt }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->rw))
                                    RW: {{ $objekPajak->detail_objek->alamat->rw }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->kode_pos))
                                    Kode Pos: {{ $objekPajak->detail_objek->alamat->kode_pos }}
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->rt) || !empty($objekPajak->detail_objek->alamat->rw) || !empty($objekPajak->detail_objek->alamat->kode_pos))
                                    <br>
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->provinsi))
                                    Provinsi: {{ $objekPajak->detail_objek->alamat->provinsi }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->kota))
                                    Kabupaten/Kota: {{ $objekPajak->detail_objek->alamat->kota }}
                                    @endif

                                    @if(!empty($objekPajak->detail_objek->alamat->provinsi) || !empty($objekPajak->detail_objek->alamat->kota))
                                    <br>
                                    @endif


                                    @if(!empty($objekPajak->detail_objek->alamat->kecamatan))
                                    Kecamatan: {{ $objekPajak->detail_objek->alamat->kecamatan }}
                                    @endif
                                    @if(!empty($objekPajak->detail_objek->alamat->kelurahan))
                                    Kelurahan: {{ $objekPajak->detail_objek->alamat->kelurahan }}
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Masa Pajak</div>
                            <?php $bulan = [1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juni', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember'] ?>
                            <div class="col s12 m6 l8"><p>{{ $bulan[(int) $spt->periode->bulan] }} {{ $spt->periode->tahun }}</p></div>
                        </div>
                        @if ($spt->status === 1)
                            <div class="row">
                                <div class="col s12 m6 l4 font-bold m-right-align">Status</div>
                                <div class="col s12 m6 l8"><p>Lunas</p></div>
                            </div>
                        @endif
                        <hr>
                        <h5 class="card-title activator">Detail Pembayaran</h5>
                        @if ($objekPajak->jenis_pajak->kode == '4.1.1.02')
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Pendapatan dari Makanan dan Minuman</div>
                            <div class="col s12 m6 l8"><p>Rp{{ number_format($dataPendapatan->makanan, 2, ',', '.') }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Pendapatan dari Service Charge</div>
                            <div class="col s12 m6 l8"><p>Rp{{ number_format($dataPendapatan->service, 2, ',', '.') }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Pendapatan lainnya</div>
                            <div class="col s12 m6 l8"><p>Rp{{ number_format($dataPendapatan->lainnya, 2, ',', '.') }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Dasar Pengenaan Pajak (DPP)</div>
                            <div class="col s12 m6 l8"><p>Rp{{ number_format($spt->jumlah_dpp, 2, ',', '.') }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Pajak Terutang (10% x DPP)</div>
                            <div class="col s12 m6 l8"><p>Rp{{ number_format($spt->jumlah_pajak, 2, ',', '.') }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Pajak yang telah dibayar</div>
                            <div class="col s12 m6 l8"><p>Rp{{ $spt->status ? number_format($spt->data_pembayaran->pajak_dibayar ?? 0, 2, ',', '.') : '0,00' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Pajak kurang atau lebih bayar</div>
                            <div class="col s12 m6 l8"><p>Rp{{ $spt->status ? number_format($spt->data_pembayaran->pajak_kurang_atau_lebih_bayar ?? 0, 2, ',', '.') : '0,00' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Denda</div>
                            <div class="col s12 m6 l8"><p>Rp{{ number_format($spt->denda_pajak, 2, ',', '.') }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l4 font-bold m-right-align">Jumlah yang harus dibayar</div>
                            <div class="col s12 m6 l8"><p>Rp{{ number_format($spt->jumlah_pajak, 2, ',', '.') }}</p></div>
                        </div>
                        @endif
                        <div class="row">
                            <div class="input-field col s12 m6 l3 push-l6">
                                <a class="btn blue waves-effect waves-light" style="width:100%" href="{{ url('/pembayaran/' . $objekPajak->_id . '/detail/' . $spt->_id . '/cetak-sspd') }}"><i class="material-icons left">print</i>Cetak SSPD</a>
                            </div>
                            @if ($spt->status !== 1)
                                <div class="input-field col s12 m6 l3 push-l6">
                                    <a class="btn green waves-effect waves-light" style="width:100%" href="{{ url('/pembayaran/' . $objekPajak->_id) . '/bayar/' . $spt->_id }}"><i class="material-icons left">check</i>Konfirmasi Pembayaran</a>
                                </div>
                            @else
                                <div class="input-field col s12 m6 l3 push-l6">
                                    <a class="btn green waves-effect waves-light" style="width:100%" href="{{ url('/pembayaran/' . $objekPajak->_id) . '/detail/' . $spt->_id . '/cetak-invoice' }}"><i class="material-icons left">print</i>Cetak Invoice</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
