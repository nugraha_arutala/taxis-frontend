@extends('skeleton')

@section('title', ($user->username ?? '?') . ' - Data User')
@section('wrapper-title', 'Data User')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title activator">Data User</h5>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Nama Lengkap</div>
                            <div class="col s12 m8 l10"><p>{{ $user->nama }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Username</div>
                            <div class="col s12 m8 l10"><p>{{ $user->username }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Email</div>
                            <div class="col s12 m8 l10"><p>{{ $user->email ?? '-' }}</p></div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l2 font-bold">Role</div>
                            <div class="col s12 m8 l10"><p>{{ implode(", ", $user->getRoleNames()) ?? '-' }}</p></div>
                        </div>
                    </div>
                </div>
                @if ($user->subjek_pajak)
                    <div class="card">
                        <div class="card-content">
                            <h5 class="card-title activator"><a href="{{ url('/lihat-data/wajib-pajak/'.$user->subjek_pajak->_id) }}">Data Wajib Pajak</a></h5>
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Nama Lengkap</div>
                                <div class="col s12 m8 l10"><p>{{ $user->subjek_pajak->nama ?? '-' }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">NPWPD</div>
                                <div class="col s12 m8 l10"><p>{{ $user->subjek_pajak->npwpd ?? '-' }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">NPWP</div>
                                <div class="col s12 m8 l10"><p>{{ $user->subjek_pajak->npwp ?? '-' }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">NIK/paspor</div>
                                <div class="col s12 m8 l10">
                                    <p>
                                        @if(!empty($user->subjek_pajak->nik) && !empty($user->subjek_pajak->paspor))
                                        {{ $user->subjek_pajak->nik }} / {{ $user->subjek_pajak->paspor }}
                                        @elseif(!empty($user->subjek_pajak->nik) || !empty($user->subjek_pajak->paspor))
                                        {{ $user->subjek_pajak->nik }}{{ $user->subjek_pajak->paspor }}
                                        @else
                                        -
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Kewarganegaraan</div>
                                <div class="col s12 m8 l10"><p>{{ $user->subjek_pajak->kewarganegaraan ?? '-' }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Tempat, Tanggal Lahir</div>
                                <div class="col s12 m8 l10"><p>{{ isset($user->subjek_pajak->tempat_lahir) ? $user->subjek_pajak->tempat_lahir . ', ' : '' }}{{ $user->subjek_pajak->tanggal_lahir ?? '-' }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Alamat</div>
                                <div class="col s12 m8 l10">
                                    <p>
                                        @if(!empty($user->subjek_pajak->alamat->jalan))
                                            {{ $user->subjek_pajak->alamat->jalan }}<br>
                                        @endif

                                        @if(!empty($user->subjek_pajak->alamat->rt))
                                        RT: {{ $user->subjek_pajak->alamat->rt }}
                                        @endif
                                        @if(!empty($user->subjek_pajak->alamat->rw))
                                        RW: {{ $user->subjek_pajak->alamat->rw }}
                                        @endif
                                        @if(!empty($user->subjek_pajak->alamat->kode_pos))
                                        Kode Pos: {{ $user->subjek_pajak->alamat->kode_pos }}
                                        @endif

                                        @if(!empty($user->subjek_pajak->alamat->rt) || !empty($user->subjek_pajak->alamat->rw) || !empty($user->subjek_pajak->alamat->kode_pos))
                                        <br>
                                        @endif

                                        @if(!empty($user->subjek_pajak->alamat->provinsi))
                                        Provinsi: {{ $user->subjek_pajak->alamat->provinsi }}
                                        @endif
                                        @if(!empty($user->subjek_pajak->alamat->kota))
                                        Kabupaten/Kota: {{ $user->subjek_pajak->alamat->kota }}
                                        @endif

                                        @if(!empty($user->subjek_pajak->alamat->provinsi) || !empty($user->subjek_pajak->alamat->kota))
                                        <br>
                                        @endif


                                        @if(!empty($user->subjek_pajak->alamat->kecamatan))
                                        Kecamatan: {{ $user->subjek_pajak->alamat->kecamatan }}
                                        @endif
                                        @if(!empty($user->subjek_pajak->alamat->kelurahan))
                                        Kelurahan: {{ $user->subjek_pajak->alamat->kelurahan }}
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Nomor Telepon</div>
                                <div class="col s12 m8 l10">
                                    <p>
                                        @if(!empty($user->subjek_pajak->nomor_telepon) && !empty($user->subjek_pajak->nomor_hp))
                                        {{ $user->subjek_pajak->nomor_telepon }} / {{ $user->subjek_pajak->nomor_hp }}
                                        @elseif(!empty($user->subjek_pajak->nomor_telepon) || !empty($user->subjek_pajak->nomor_hp))
                                        {{ $user->subjek_pajak->nomor_telepon }}{{ $user->subjek_pajak->nomor_hp }}
                                        @else
                                        -
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Email</div>
                                <div class="col s12 m8 l10"><p>{{ $user->subjek_pajak->email ?? '-' }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col s12 m4 l2 font-bold">Jenis Wajib Pajak</div>
                                <div class="col s12 m8 l10"><p>{{ $user->subjek_pajak->jenis ?? '-' }}</p></div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
