@extends('skeleton')

@section('title', 'Data User')

@section('content')
    <div class="container-fluid">
        @if ($errors->any())
        <div class="card red darken-4">
            <div class="card-content white-text">
                <span class="card-title">Submit Gagal</span>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        </div>
        @endif
        <form method="POST" action="{{ url(!isset($profile) ? '/pendaftaran/user' : '/profil/edit-user') }}">
            @csrf
            @if (!empty($user))
                <input type="hidden" name="_id" value="{{ $user->_id }}">
            @endif
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="nama" type="text" name="nama" value="{{ old('nama') ?? $user->nama ?? '' }}">
                                    <label for="nama">Nama</label>
                                </div>
                            </div>
                            @if (!isset($profile))
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="username" type="text" name="username" value="{{ old('username') ?? $user->username ?? '' }}">
                                        <label for="username">Username</label>
                                    </div>
                                </div>
                            @endif
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="password" type="password" name="password" value="{{ old('password') ?? '' }}">
                                    <label for="password">Password</label>
                                    @if(isset($profile) || !empty($user))
                                        <span class="helper-text">Kosongkan saja bila tidak ingin diubah.</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="email" type="text" name="email" value="{{ old('email') ?? $user->email ?? '' }}">
                                    <label for="email">Email</label>
                                </div>
                            </div>
                            @if (!isset($profile))
                                <div class="row">
                                    <div class="input-field col s12">
                                        <select id="roles" name="roles[]" multiple>
                                            <option value="1" {{ in_array(1, (old('roles') ?? $user->roles ?? array())) ? 'selected' : '' }}>Wajib Pajak</option>
                                            <option value="2" {{ in_array(2, (old('roles') ?? $user->roles ?? array())) ? 'selected' : '' }}>Fiskus</option>
                                            <option value="365" {{ in_array(365, (old('roles') ?? $user->roles ?? array())) ? 'selected' : '' }}>Super Admin</option>
                                        </select>
                                        <label for="roles">Role</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="npwpd" type="text" name="npwpd" value="{{ old('npwpd') ?? $user->subjek_pajak->npwpd ?? '' }}">
                                        <label for="npwpd">NPWPD</label>
                                        <span class="helper-text">Jika diisi, user akan dihubungkan dengan data Wajib Pajak.</span>
                                    </div>
                                </div>
                            @endif
                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="btn cyan waves-effect waves-light right" type="submit">Simpan Data</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('javascript')
<script>
    $(function () {
        $("select").not(".disabled").formSelect();
    });
</script>
@endsection
