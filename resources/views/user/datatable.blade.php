@extends('skeleton')

@section('title', 'Daftar User')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                @if (Session::has('message'))
                    @if(Session::get('message') === 'save-successful')
                    <div class="card green darken-2">
                        <div class="card-content white-text">
                            Data baru berhasil disimpan.
                        </div>
                    </div>
                    @endif
                @endif
                <div class="card">
                    <div class="card-content">
                        <div class="right-align">
                            <a class="btn-floating btn-large waves-effect waves-light green" href="{{ url('/pendaftaran/user') }}"><i class="material-icons">add</i></a>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <select id="search_role">
                                    <option value="">Tampilkan semua</option>
                                    <option value="1">Wajib Pajak</option>
                                    <option value="2">Fiskus</option>
                                    <option value="365">Administrator</option>
                                </select>
                                <label for="search_role">Role</label>
                            </div>
                        </div>
                        <table id="datatable" class="responsive-table display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Username</th>
                                    <th>Nama</th>
                                    <th>Role</th>
                                    <th>Subjek Pajak</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="99" style="text-align: center">Loading...</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hide">
        <!-- action button column for datatable -->
        <div id="action-button-column-sample">
            <a class="waves-effect waves-light btn-small view" href="{{ url('/lihat-data/user/[id]') }}"><i class="material-icons">search</i></a>
            <a class="waves-effect waves-light btn-small edit" href="{{ url('/lihat-data/user/[id]/edit') }}"><i class="material-icons">edit</i></a>
        </div>
    </div>
@endsection

@section('stylesheet')
    <link href="{{ asset('css/datatable-fix.css') }}" rel="stylesheet">
@endsection
@section('javascript')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script>
        // Element init
        $(function () {
            $("select").not(".disabled").formSelect();
        });

        $('#datatable').DataTable({
            serverSide: true,
            lengthChange: false,
            ordering: false,
            order: [],
            searching: true,
            ajax: {
                url: '{{ url('/lihat-data/user/datatable') }}',
                type: 'GET'
            },
            columns: [
                {
                    data: function(row, type, set, meta) {
                        var pginfo = $(meta.settings.nTable).DataTable().page.info();
                        return (pginfo.page * pginfo.length) + (meta.row + 1);
                    },
                    className: 'center-align'
                },
                {
                    data: function(row, type, set, meta) {
                        return row.username || '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.nama || '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.roles ? row.roles.join(', ') : '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        return row.subjek_pajak ? row.subjek_pajak.nama : '-';
                    }
                },
                {
                    data: function(row, type, set, meta) {
                        var div = $('#action-button-column-sample').clone();
                        var buttons = div.find('a');
                        for(i of buttons) {
                            $(i).prop('href', $(i).prop('href').replace('[id]', row._id));
                        }
                        return div.html();
                    },
                    orderable: false
                }
            ]
        });

        // manual search
        $('#datatable_filter').remove();

        $('#search_role').val('');

        $('#search_role').on('change', function (e) {
            $('#datatable').DataTable().columns(3).search($('#search_role').val()).draw();
        })
    </script>
@endsection
