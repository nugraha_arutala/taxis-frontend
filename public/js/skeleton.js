// jQuery

$(function() {

    "use strict";
    $(function() {
        $(".preloader").fadeOut();
    });

    $('.sidenav').sidenav();
    $(".dropdown-trigger").dropdown({
        alignment: 'left',
        coverTrigger: false,
        hover: false,
        closeOnClick: false
    });
    $('.collapsible').collapsible();
    $("body").trigger("resize");

    // ==============================================================
    // This is for the top header part and sidebar part
    // ==============================================================
    var set = function() {
        var width = (window.innerWidth > 0) ? window.innerWidth : this.screen.width;
        var topOffset = 75;
        if (width < 1170) {
            //$("#main-wrapper").addClass("mini-sidebar");
            $('#topsubnav').sidenav({
                onOpenStart: function() {
                    $('body').addClass('overlay');
                },
                onCloseStart: function() {
                    $('body').removeClass('overlay');
                }
            });
        } else {
            //$("#main-wrapper").removeClass("mini-sidebar");
        }
    };
    $(window).ready(set);
    $(window).on("resize", set);

    // ==============================================================
    // active menu js
    // ==============================================================
    $(function() {
        var url = window.location;
        if(typeof sidebarUrl != 'undefined') {
            url = sidebarUrl;
        }
        var element = $('ul.collapsible a').filter(function() {
            return this.href == url;
        }).addClass('active').parent().addClass('active');
        while (true) {
            if (element.is('li')) {
                element = element.parent().parent().css({
                    "display": "block"
                }).parent().addClass('active');
            } else {
                break;
            }
        }
    });
    $(".sidebar-toggle").on('click', function() {
        $("#main-wrapper").toggleClass("show-sidebar");
    });
    // ==============================================================
    // sidebar-hover
    // ==============================================================
    $(".left-sidebar").hover(
        function() {
            $(".brand-logo").addClass("full-logo");
        },
        function() {
            $(".brand-logo").removeClass("full-logo");
        }
    );
    // ==============================================================
    // Perfect Scrollbar
    // ==============================================================
    $('#main-wrapper[data-layout="vertical"] #slide-out, #right-slide-out, .message-center, .scrollable, .pre-scroll').perfectScrollbar();
    // ==============================================================
    // FAB Buttons
    // ==============================================================
    $('.fixed-action-btn').floatingActionButton();
    $('.fixed-action-btn.horizontal').floatingActionButton({
        direction: 'left'
    });
    $('.fixed-action-btn.click-to-toggle').floatingActionButton({
        direction: 'left',
        hoverEnabled: false
    });
    // ==============================================================
    // modal
    // ==============================================================
    $('.modal').modal();
    // ==============================================================
    // tooltip
    // ==============================================================
    $('.tooltipped').tooltip();
    // ==============================================================
    // parallax
    // ==============================================================
    $('.parallax').parallax();
    // ==============================================================
    // dynamic color
    // ==============================================================
    // convert rgb to hex value string
    function rgb2hex(rgb) {
        if (/^#[0-9A-F]{6}$/i.test(rgb)) {
            return rgb;
        }

        rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);

        if (rgb === null) {
            return "N/A";
        }

        function hex(x) {
            return ("0" + parseInt(x).toString(16)).slice(-2);
        }

        return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    }

    $('.dynamic-color .col').each(function() {
        $(this).children().each(function() {
            var color = $(this).css('background-color'),
                classes = $(this).attr('class');
            $(this).html('<span>' + rgb2hex(color) + " " + classes + '</span>');
            if (classes.indexOf("darken") >= 0 || $(this).hasClass('black')) {
                $(this).css('color', 'rgba(255,255,255,.9');
            }
        });
    });
    // ==============================================================
    // Toggle Containers on page
    // ==============================================================
    var toggleContainersButton = $('#container-toggle-button');
    toggleContainersButton.click(function() {
        $('body .browser-window .container, .had-container').each(function() {
            $(this).toggleClass('had-container');
            $(this).toggleClass('container');
            if ($(this).hasClass('container')) {
                toggleContainersButton.text("Turn off Containers");
            } else {
                toggleContainersButton.text("Turn on Containers");
            }
        });
    });
    // ==============================================================
    // Toggle Flow Text
    // ==============================================================
    var toggleFlowTextButton = $('#flow-toggle');
    toggleFlowTextButton.click(function() {
        $('#flow-text-demo').children('p').each(function() {
            $(this).toggleClass('flow-text');
        });
    });

    $(".search-box a, .search-box .app-search .srh-btn").on('click', function() {
        $(".app-search").toggle(200);
        $(".app-search input").focus();
    });
    // ==============================================================
    // This is for the innerleft sidebar
    // ==============================================================
    $(".show-left-part").on('click', function() {
        $('.left-part').toggleClass('show-panel');
        $('.show-left-part').toggleClass('ti-menu');
    });
});
