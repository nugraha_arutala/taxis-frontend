<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// No welcome page; redirect to login
Route::get('/', function () { return redirect('/login'); });

Route::get('/login', 'AuthController@form')->name('login')->middleware('guest');
Route::post('/login', 'AuthController@authenticate')->middleware('guest');
Route::get('/logout', 'AuthController@logout')->name('logout');

Route::middleware(['auth'])->group(function () {
    // Dashboard
    Route::get('/dashboard', 'DashboardController@view');

    // Profil User
    Route::get('/profil', 'UserController@viewProfile');
    Route::get('/profil/edit-user', 'UserController@editProfileUser');
    Route::get('/profil/edit-wp', 'UserController@editProfileWp');
    Route::post('/profil/edit-user', 'UserController@saveProfileUser');
    Route::post('/profil/edit-wp', 'UserController@saveProfileWp');

    // Pseudo-API (for AJAX calls)
    Route::get('/ajax/wajib-pajak', 'WajibPajakController@find');
    Route::get('/ajax/objek-pajak/list', 'ObjekPajakController@listing');
    Route::get('/ajax/jenis-pajak', 'KlasifikasiPajakController@findJenisPajak');

    // Lihat Data
    Route::get('/lihat-data/wajib-pajak', 'WajibPajakController@viewTable');
    Route::get('/lihat-data/wajib-pajak/datatable', 'WajibPajakController@datatableJson');
    Route::get('/lihat-data/wajib-pajak/{id}', 'WajibPajakController@viewDetail');
    Route::get('/lihat-data/wajib-pajak/{id}/edit', 'WajibPajakController@formEdit');

    Route::get('/lihat-data/wajib-pajak/{subjekPajakId}/objek-pajak/datatable', 'ObjekPajakController@datatableJson');

    Route::get('/lihat-data/objek-pajak', 'ObjekPajakController@viewTable');
    Route::get('/lihat-data/objek-pajak/datatable', 'ObjekPajakController@datatableJson');
    Route::get('/lihat-data/objek-pajak/{id}', 'ObjekPajakController@viewDetail');
    Route::get('/lihat-data/objek-pajak/{id}/edit', 'ObjekPajakController@formEdit');
    Route::post('/lihat-data/objek-pajak/{id}/verifikasi', 'ObjekPajakController@verify');

    Route::get('/lihat-data/user', 'UserController@viewTable');
    Route::get('/lihat-data/user/datatable', 'UserController@datatableJson');
    Route::get('/lihat-data/user/{id}', 'UserController@viewDetail');
    Route::get('/lihat-data/user/{id}/edit', 'UserController@formEdit');

    // Pendaftaran
    Route::get('/pendaftaran/wajib-pajak', 'WajibPajakController@formNew');
    Route::post('/pendaftaran/wajib-pajak', 'WajibPajakController@post');

    Route::get('/pendaftaran/objek-pajak', 'ObjekPajakController@formNew');
    Route::get('/pendaftaran/objek-pajak/{idWajibPajak}', 'ObjekPajakController@formNew');
    Route::post('/pendaftaran/objek-pajak', 'ObjekPajakController@post');

    Route::get('/pendaftaran/user', 'UserController@formNew');
    Route::post('/pendaftaran/user', 'UserController@post');

    Route::get('/pendaftaran/spt/{objekPajakId}', 'PelaporanController@createForm');
    Route::post('/pendaftaran/spt/{objekPajakId}', 'PelaporanController@createProcess');
    Route::get('/pendaftaran/skpd/{objekPajakId}', 'SKPDController@createForm');
    Route::post('/pendaftaran/skpd/{objekPajakId}', 'SKPDController@createProcess');

    Route::get('/pendaftaran/surat-teguran', 'SuratTeguranController@formNew');
    Route::post('/pendaftaran/surat-teguran', 'SuratTeguranController@save');

    // Pembayaran
    Route::get('/pembayaran', 'PembayaranController@viewTable');

    Route::get('/pembayaran/{id}', 'PembayaranController@viewObjek');
    Route::get('/pembayaran/{id}/datatable', 'PembayaranController@objekDatatableJson');

    Route::get('/pembayaran/{objekPajakId}/bayar/{id}', 'PembayaranController@paymentPage');
    Route::post('/pembayaran/{objekPajakId}/bayar/{id}', 'PembayaranController@pay');
    Route::get('/pembayaran/{objekPajakId}/detail-pembayaran/{id}', 'PembayaranController@viewPaymentDetail');
    Route::get('/pembayaran/{objekPajakId}/detail/{id}', 'PembayaranController@viewSPDDetail');
    Route::get('/pembayaran/{objekPajakId}/detail/{id}/cetak-invoice', 'PembayaranController@printInvoice');
    Route::get('/pembayaran/{objekPajakId}/detail/{id}/cetak-sspd', 'PembayaranController@printSspd');

    // Laporan
    Route::get('/reporting', 'LaporanController@formView');
    Route::post('/reporting', 'LaporanController@export');

    // Pelaporan
    Route::get('/pelaporan', 'PelaporanController@viewTable');
    Route::get('/pelaporan/datatable', 'PelaporanController@datatableJson');

    Route::get('/pelaporan/{id}', 'PelaporanController@viewObjek');
    Route::get('/pelaporan/{id}/datatable', 'PelaporanController@objekDatatableJson');

    Route::get('/pelaporan/{objekPajakId}/form/{id}', 'PelaporanController@form');
    Route::post('/pelaporan/{objekPajakId}/form/{id}', 'PelaporanController@save');

    Route::get('/pelaporan/{objekPajakId}/detail/{id}', 'PelaporanController@viewDetail');
    Route::get('/pelaporan/{objekPajakId}/form/{id}/print', 'PelaporanController@printFormDetail');

    // Pemeriksaan
    // SKPD
    Route::get('/skpd', 'SKPDController@viewTable');
    Route::get('/skpd/datatable', 'SKPDController@datatableJson');

    Route::get('/skpd/{id}', 'SKPDController@viewObjek');
    Route::get('/skpd/{id}/datatable', 'SKPDController@objekDatatableJson');

    Route::get('/skpd/{objekPajakId}/form/{id}', 'SKPDController@form');
    Route::post('/skpd/{objekPajakId}/form/{id}', 'SKPDController@save');

    Route::get('/skpd/{objekPajakId}/detail/{id}', 'SKPDController@viewDetail');
    Route::get('/skpd/{objekPajakId}/form/{id}/print', 'SKPDController@printFormDetail');

    // Surat Teguran
    Route::get('/surat-teguran', 'SuratTeguranController@viewTable');
    Route::get('/surat-teguran/datatable', 'SuratTeguranController@datatableJson');

    Route::get('/surat-teguran/{id}/view', 'SuratTeguranController@viewFile');
    Route::get('/surat-teguran/{id}/edit', 'SuratTeguranController@formEdit');
    Route::get('/surat-teguran/{id}/file', 'SuratTeguranController@viewFile');
    Route::get('/surat-teguran/tambah', 'SuratTeguranController@formNew');
    Route::post('/surat-teguran', 'SuratTeguranController@save');

    // Master Data
    Route::get('/master-data/jenis-klasifikasi', 'KlasifikasiPajakController@viewTable');
    Route::get('/master-data/jenis-klasifikasi/datatable', 'KlasifikasiPajakController@datatableJson');

    Route::get('/master-data/jenis-klasifikasi/tambah', 'KlasifikasiPajakController@form');
    Route::get('/master-data/jenis-klasifikasi/{id}/edit', 'KlasifikasiPajakController@form');
    Route::post('/master-data/jenis-klasifikasi', 'KlasifikasiPajakController@post');
    Route::post('/master-data/jenis-klasifikasi/{id}', 'KlasifikasiPajakController@post');

    Route::get('/master-data/jenis-klasifikasi/{id}', 'KlasifikasiPajakController@viewDetail');
});
